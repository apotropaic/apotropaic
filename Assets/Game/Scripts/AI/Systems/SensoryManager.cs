using UnityEngine;

public class SensoryManager : MonoBehaviour
{
    [SerializeField] private PlayerData playerData = null;
    [SerializeField] private SensorySystem[] sensorySystems = null;

    #region UNITY
    private void Awake()
    {
        InitializeSensorySystems();
    }
    #endregion

    #region PUBLIC
    private void InitializeSensorySystems()
    {
        for (int i = 0; i < sensorySystems.Length; i++)
        {
            sensorySystems[i].Initialize(playerData);
        }
    }
    #endregion

    #region PRIVATE
    #endregion
}
