using UnityEngine;

public class SmellNode : MonoBehaviour
{
    [SerializeField] private Color startColor = new Color(0, 1, 0, 1);
    [SerializeField] private Color endColor = new Color(1, 0, 0, 1);

    private SpriteRenderer sprite = null;
    private float lifeSpan;
    private float timeElapsed = 0f;

    private float strength;

    private SmellNode nextNode = null;
    private SmellNode previousNode = null;

    private bool enableGizmos = false;

    #region UNITY

    private void Start()
    {
        sprite = GetComponentInChildren<SpriteRenderer>();
    }

    private void Update()
    {
        timeElapsed += Time.deltaTime;
        InterpolateColor();

        // When expires, turn it off and remove next node link
        if (timeElapsed > lifeSpan)
        {
            if (nextNode != null)
                nextNode.previousNode = null;

            ResetNode();
            gameObject.SetActive(false);
        }
    }

    private void OnDrawGizmos()
    {
        if (!enableGizmos) return;
        Color gizmosColor = sprite.color;
        gizmosColor.a = 1f;
        Gizmos.color = gizmosColor;
        Gizmos.DrawWireSphere(transform.position, transform.localScale.x * 2f);
    }
    #endregion

    #region PUBLIC
    public void Initialize(float lifeSpanTime, bool enableGizmo)
    {
        lifeSpan = lifeSpanTime;
        enableGizmos = enableGizmo;
    }

    public void ResetNode()
    {
        nextNode = null;
        previousNode = null;
        ResetColor();
    }

    public float Strength { get { return strength; } }

    public SmellNode NextNode { get { return nextNode; } set { nextNode = value; } }
    public SmellNode PreviousNode { get { return previousNode; } set { previousNode = value; } }
    #endregion

    #region PRIVATE
    private void ResetColor()
    {
        timeElapsed = 0.0f;
    }

    private void InterpolateColor()
    {
        sprite.color = Color.Lerp(startColor, endColor, timeElapsed / lifeSpan);
        strength = 100 - (timeElapsed / lifeSpan) * 100;
    }
    #endregion
}
