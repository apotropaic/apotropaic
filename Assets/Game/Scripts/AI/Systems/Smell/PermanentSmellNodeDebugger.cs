using UnityEngine;

public class PermanentSmellNodeDebugger : MonoBehaviour
{
    private bool drawGizmos = false;
    private Color gizmosColor = Color.white;

    #region UNITY
    private void OnDrawGizmos()
    {
        if (!drawGizmos) return;

        Gizmos.color = gizmosColor;
        Gizmos.DrawWireSphere(transform.position, transform.localScale.x * 2f);
        Gizmos.color = Color.white;
    }
    #endregion

    #region PUBLIC
    public void Initialize(bool enableGizmos)
    {
        drawGizmos = enableGizmos;
        gizmosColor = Color.green;
    }
    #endregion

    #region PRIVATE
    #endregion
}
