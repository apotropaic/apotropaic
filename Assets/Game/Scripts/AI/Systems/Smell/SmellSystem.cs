using UnityEngine;
using System.Collections.Generic;
using Apotropaic.Data;

public class SmellSystem : SensorySystem
{
    [Header("Settings")]
    [SerializeField] private SmellSystemSettings settings = null;

    // Settings
    private float nodesLifespan;
    private float nodeDistance;
    private float regularNodeScale = 1f;
    private float permenantNodeScale = 1f;
    private PoolingMode poolingMode = PoolingMode.Recycle;

    [Space(20)]
    [SerializeField] private int poolSize = 6;
    [SerializeField] private Transform nodeRoot;
    [SerializeField] private GameObject prefab;
    [SerializeField] private GameObject permenantNodePrefab;

    private List<GameObject> smellNodes = new List<GameObject>();

    private GameObject playerNode;
    private GameObject currentNode = null;

    // Cache
    private Vector3 playerPosition = Vector3.zero;

    public enum PoolingMode
    {
        Recycle = 0,
        InstantiateOnNeed = 1
    }

    #region UNITY
    protected override void Start()
    {
        base.Start();

        RefreshPlayerPositionCache();
        InitializePlayerNode();
        PopulateSmellNodesPool();
        InitializeFirstNode();
    }

    private void FixedUpdate()
    {
        RefreshPlayerPositionCache();
        DropNode();
        RefreshPermenantNodePosition();
    }

    #endregion
    //===============================
    //===============================
    //===============================
    //===============================
    //===============================
    #region PUBLIC


    #endregion
    //===============================
    //===============================
    //===============================
    //===============================
    //===============================
    #region PROTECTED
    protected override void LoadSettings()
    {
        nodesLifespan = settings.NodesLifespan;
        nodeDistance = settings.NodeDistance;
        regularNodeScale = settings.RegularNodeScale;
        permenantNodeScale = settings.PermenantNodeScale;
        poolingMode = settings.PoolingMode;
    }
    #endregion
    //===============================
    //===============================
    //===============================
    //===============================
    //===============================
    #region PRIVATE
    private void InitializePlayerNode()
    {
        playerNode = Instantiate(permenantNodePrefab, nodeRoot);
        playerNode.transform.localScale = Vector3.one * permenantNodeScale;
        playerNode.GetComponent<PermanentSmellNodeDebugger>().Initialize(settings.EnableSmellNodeGizmos);
    }

    private void PopulateSmellNodesPool()
    {
        while (smellNodes.Count < poolSize)
        {
            AddNewNodeToPool();
        }
    }

    private GameObject AddNewNodeToPool()
    {
        GameObject node = Instantiate(prefab, nodeRoot);
        node.transform.localScale = Vector3.one * regularNodeScale;
        node.SetActive(false);
        node.GetComponent<SmellNode>().Initialize(nodesLifespan, settings.EnableSmellNodeGizmos);
        smellNodes.Add(node);
        node.name += " " + smellNodes.Count;
        return node;
    }

    private void InitializeFirstNode()
    {
        currentNode = GetNodeFromPool();
        currentNode.transform.position = new Vector3(playerPosition.x, 0, playerPosition.z);
        currentNode.SetActive(true);
    }

    private void RefreshPlayerPositionCache()
    {
        playerPosition = playerTransform.position;
    }

    private void DropNode()
    {
        if (Vector3.SqrMagnitude(playerPosition - currentNode.transform.position) > nodeDistance * nodeDistance)
        {
            if (!currentNode.activeInHierarchy)
            {
                currentNode.transform.position = new Vector3(playerPosition.x, 0, playerPosition.z);
                currentNode.SetActive(true);
            }
            else
            {
                SmellNode currentSmellNode = currentNode.GetComponent<SmellNode>();

                GameObject newNode = GetNodeFromPool();

                if (newNode == null)
                {
                    SmellNode node = currentSmellNode;

                    while (node.PreviousNode != null)
                    {
                        node = node.PreviousNode;
                    }

                    newNode = node.gameObject;
                    newNode.GetComponent<SmellNode>().NextNode.PreviousNode = null;
                }

                SmellNode newSmellNode = newNode.GetComponent<SmellNode>();
                newSmellNode.ResetNode();

                currentSmellNode.NextNode = newSmellNode;
                newSmellNode.PreviousNode = currentSmellNode;

                currentNode = newNode;
                currentNode.transform.position = new Vector3(playerPosition.x, 0, playerPosition.z);
                currentNode.SetActive(true);
            }
        }
    }

    private void RefreshPermenantNodePosition()
    {
        playerNode.transform.position = new Vector3(playerPosition.x, 0, playerPosition.z);
    }

    private GameObject GetNodeFromPool()
    {
        for (int i = 0; i < smellNodes.Count; i++)
        {
            if (!smellNodes[i].activeInHierarchy)
            {
                return smellNodes[i];
            }
            else if (i == smellNodes.Count - 1 && poolingMode == PoolingMode.InstantiateOnNeed)
            {
                return AddNewNodeToPool();
            }
        }

        return null;
    }
    #endregion
}
