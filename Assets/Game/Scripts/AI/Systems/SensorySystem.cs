using UnityEngine;

public abstract class SensorySystem : MonoBehaviour
{
    protected PlayerData playerData = null;
    protected Transform playerTransform = null;

    #region UNITY
    protected virtual void Awake()
    {
        LoadSettings();
    }

    protected virtual void Start()
    {
        playerTransform = playerData.transform;
    }
    #endregion

    #region PUBLIC
    public void Initialize(PlayerData data)
    {
        playerData = data;
    }
    #endregion

    #region PROTECTED
    protected abstract void LoadSettings();
    #endregion

    #region PRIVATE
    #endregion
}
