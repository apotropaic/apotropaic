using UnityEngine;
using System;

public class SoundSystem : SensorySystem, ISoundSystem
{
    public delegate void SoundSystemDelegate(Vector3 position, float strength, float radius, SoundType soundType, BasicTrap basicTrap);
    public SoundSystemDelegate OnSoundTriggered = null;

    // TODO : Implement
    // [SerializeField] private SoundSystemSettings soundSystemSettings = null;

    #region UNITY
    protected override void Awake()
    {
        base.Awake();

        // Initialize
        SoundPlayer[] soundPlayers = FindObjectsOfType<SoundPlayer>(true);

        for (int i = 0; i < soundPlayers.Length; i++)
        {
            soundPlayers[i].Initialize(this);
        }
    }
    #endregion

    #region PUBLIC
    public void TriggerSound(Vector3 position, float strength, float radius, SoundType soundType, BasicTrap basicTrap)
    {
        if (OnSoundTriggered != null)
        {
            OnSoundTriggered(position, strength, radius, soundType, basicTrap);
        }
    }
    #endregion

    #region PROTECTED
    protected override void LoadSettings()
    {
    }
    #endregion

    #region PRIVATE
    #endregion
}
