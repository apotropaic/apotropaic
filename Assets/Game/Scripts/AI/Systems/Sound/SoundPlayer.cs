using Apotropaic.Data;
using EasyButtons;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundPlayer : MonoBehaviour
{
    [SerializeField] private SoundSettings soundSettings = null;

    // Settings
    private float strength = 0f;
    private float radius = 0f;
    private float notificationTime = 0f;
    private float delayBeforeFirstPlayback = 0f;
    private SoundType soundType = SoundType.PlayerSound;
    private BasicTrap basicTrap = null;
    private bool repeatEveryNotificationTime = false;

    float? delayUntilPlayback = null;

    private AudioSource audioSource = null;
    private ISoundSystem soundSystem = null;

    private float timePassed = 0f;
    private bool disableOnPlaybackEnded = false;

    private bool playedOnce = false;

    private bool isActive = false;

    private bool initialized = false;

    #region UNITY

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (soundSettings == null) return;
        if (!soundSettings.EnableDebugGizmos) return;

        Gizmos.color = soundSettings.DebugGizmosColor;
        Gizmos.DrawWireSphere(transform.position, soundSettings.Radius);
    }
#endif

    private void OnEnable()
    {
        playedOnce = false;
        disableOnPlaybackEnded = false;
    }

    private void Start()
    {
        // If we are not already initialized, initialize.
        // Needed for when we are not in the gameplay scene (therefore there is no sound system).
        Initialize();
    }

    private void Update()
    {
        if (delayUntilPlayback != null)
            ProcessScheduledPlayback();

        ProcessSoundEvents();
        ProcessTrapSoundPlayback();
    }

    #endregion

    #region PUBLIC
    public void Initialize(ISoundSystem _soundSystem)
    {
        soundSystem = _soundSystem;
        Initialize();
    }

    public void AttemptPlaySound()
    {
        if (delayBeforeFirstPlayback == 0f || playedOnce)
        {
            PlaySound();
        }
        else
        {
            // Schedule sound for playback
            delayUntilPlayback = delayBeforeFirstPlayback;
        }
    }

    public void StopSound()
    {
        audioSource.Stop();
        timePassed = 0f;
        isActive = false;
    }
    #endregion

    #region PRIVATE
    private void LoadSettings()
    {
        strength = soundSettings.Strength;
        radius = soundSettings.Radius;
        notificationTime = soundSettings.NotificationTime;
        soundType = soundSettings.SoundType;
        repeatEveryNotificationTime = soundSettings.RepeatEveryNotificationTime;
        delayBeforeFirstPlayback = soundSettings.DelayBeforeFirstPlayback;

        audioSource.clip = soundSettings.AudioClip;
        audioSource.loop = soundSettings.Loop;
        audioSource.pitch = soundSettings.Pitch;
        audioSource.volume = soundSettings.Volume;
    }

    private void Initialize()
    {
        if (initialized) return;

        audioSource = GetComponent<AudioSource>();
        audioSource.playOnAwake = false;
        LoadSettings();

        if (soundType == SoundType.TrapSound)
        {
            basicTrap = GetComponent<BasicTrap>();
        }

        initialized = true;
    }

    private void PlaySound()
    {
        if (soundSystem != null)
            soundSystem.TriggerSound(transform.position, strength, radius, soundType, basicTrap);

        if (soundType == SoundType.TrapSound)
        {
            if (basicTrap != null)
            {
                if (basicTrap.Lifespan == TrapLifespan.MultipleUses && basicTrap.TrapDetection == TrapDetection.Sound)
                {
                    basicTrap.UseTrap();

                    // Mark the trap to be destroyed when the current playback ends.
                    if (basicTrap.UsesLeft <= 0)
                    {
                        disableOnPlaybackEnded = true;
                    }
                }
            }
        }

        audioSource.Play();
        playedOnce = true;
        isActive = true;
    }

    private void ProcessScheduledPlayback()
    {
        delayUntilPlayback -= Time.deltaTime;

        if (delayUntilPlayback <= 0)
        {
            PlaySound();
            delayUntilPlayback = null;
        }
    }

    private void ProcessSoundEvents()
    {
        if (soundSystem == null) return;
        if (notificationTime == 0f) return;
        if (!isActive) return;

        if (soundType == SoundType.TrapSound)
        {
            if (basicTrap.UsesLeft <= 0)
            {
                // Trap can't trigger more sounds, return
                return;
            }
        }

        timePassed += Time.deltaTime;

        if (timePassed > notificationTime)
        {
            timePassed = 0f;

            if (repeatEveryNotificationTime)
            {
                AttemptPlaySound();
            }
            else
            {
                if (soundSystem != null)
                    soundSystem.TriggerSound(transform.position, strength, radius, soundType, basicTrap);
            }
        }
    }

    private void ProcessTrapSoundPlayback()
    {
        if (disableOnPlaybackEnded && !audioSource.isPlaying)
        {
            basicTrap.gameObject.SetActive(false);
        }
    }
    #endregion
}