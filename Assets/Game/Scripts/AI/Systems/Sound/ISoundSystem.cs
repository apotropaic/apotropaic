using UnityEngine;

public interface ISoundSystem
{
    public void TriggerSound(Vector3 position, float strength, float radius, SoundType soundType, BasicTrap basicTrap);
}
