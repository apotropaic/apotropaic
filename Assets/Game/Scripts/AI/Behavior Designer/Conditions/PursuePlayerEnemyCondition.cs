using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

public class PursuePlayerEnemyCondition : EnemyCondition
{
    #region PUBLIC
    public override TaskStatus OnUpdate()
    {
        if (_enemyController.PursuingPlayer && !_enemyController.Frozen)
            return TaskStatus.Success;
        else if (_enemyController.PursuingPlayer)
            _enemyController.PursuingPlayer = false;

        return TaskStatus.Failure;
    }
    #endregion

    #region PRIVATE
    #endregion
}