using Apotropaic.Data;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections.Generic;
using UnityEngine;

public class ResolveBasicTrapEnemyAction : EnemyAction
{
    [SerializeField] private SharedFloat freezeMovementDelay = 0f;

    #region PUBLIC
    public override TaskStatus OnUpdate()
    {
        return ResolveBasicTrap();
    }
    #endregion

    #region PROTECTED
    #endregion

    #region PRIVATE
    private TaskStatus ResolveBasicTrap()
    {
        BasicTrap trap = _enemyController.DetectedBasicTrap;

        if (trap != null)
        {
            // If it is a smell trap and the enemy has no smell strength, return.
            if (trap.TrapDetection == TrapDetection.Smell && _enemyController.SmellMultiplier == 0f)
            {
                _enemyController.DetectedBasicTrap = null;
                return TaskStatus.Success;
            }

            ResolveTrap(trap.EnemyDelay, trap.Damage, trap.RepulsionOdds);

            if (trap.Lifespan == TrapLifespan.SingleUse)
            {
                trap.DestroyTrap();
            }
            else if (trap.Lifespan == TrapLifespan.MultipleUses && trap.TrapDetection != TrapDetection.Sound)
            {
                trap.UseTrap();

                if (trap.UsesLeft == 0)
                {
                    trap.DestroyTrap();
                }
            }

            _enemyController.DetectedBasicTrap = null;
        }

        return TaskStatus.Success;
    }

    private void ResolveTrap(float enemyDelay, int damage, List<RepulsionOdds> repulsionOdds)
    {
        if (damage > 0)
        {
            _enemyController.TakeDamage(damage);
        }

        if (repulsionOdds.Count > 0)
        {
            for (int i = 0; i < repulsionOdds.Count; i++)
            {
                if (repulsionOdds[i].EnemyName == _enemySettings.EnemyName)
                {
                    bool ignoreRepulsionIfPursuingPlayer = repulsionOdds[i].IgnoreRepulsionIfPursuingPlayer;

                    if (!ignoreRepulsionIfPursuingPlayer || (ignoreRepulsionIfPursuingPlayer && !_enemyController.PursuingPlayer))
                    {
                        float odds = Random.Range(0f, 1f);

                        if (odds <= repulsionOdds[i].RepulsionOdd)
                        {
                            _enemyController.FetchFurthestWaypoint();
                            _enemyController.Wander();
                            _enemyController.StartRepulsionPursueCooldown();
                            // Return, enemies shouldn't be delayed.
                            return;
                        }
                    }

                    break;
                }
            }
        }

        // If enemy wasn't affected by repulsion and there is delay on the trap.
        if (enemyDelay > 0)
        {
            freezeMovementDelay.Value = enemyDelay;
            _enemyController.SetAgentDestination(_enemyController.DetectedBasicTrap.transform.position);
            _enemyController.Frozen = true;
        }
    }
    #endregion
}