using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

public class ResolveBasicTrapEnemyCondition : EnemyCondition
{
    #region PUBLIC
    public override TaskStatus OnUpdate()
    {
        if (_enemyController.DetectedBasicTrap != null)
        {
            return TaskStatus.Success;
        }

        return TaskStatus.Failure;
    }
    #endregion

    #region PRIVATE
    #endregion
}