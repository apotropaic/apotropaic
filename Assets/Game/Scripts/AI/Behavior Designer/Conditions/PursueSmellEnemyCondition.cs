using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

public class PursueSmellEnemyCondition : EnemyCondition
{
    private bool newSmellDetected = false;
    private float smellMultiplier = 0f;
    private float pursueSmellSpeedMultiplier = 0f;

    #region PUBLIC
    public override void OnAwake()
    {
        base.OnAwake();
        smellMultiplier = _enemyController.SmellMultiplier;
        pursueSmellSpeedMultiplier = _enemyController.PursueSmellSpeedMultiplier;
    }

    public override TaskStatus OnUpdate()
    {
        // Enemy detected a smell
        if (_enemyController.SmellDetected)
        {
            // Resolve the smell
            ResolveSmell(_enemyController.DetectedSmellNode);

            // Reset the detection variables.
            _enemyController.SmellDetected = false;
            _enemyController.DetectedSmellNode = null;
        }

        if (newSmellDetected || _enemyController.TargetSmellNode != null)
        {
            newSmellDetected = false;
            return TaskStatus.Success;
        }

        return TaskStatus.Failure;
    }
    #endregion

    #region PROTECTED
    private void ResolveSmell(SmellNode smellNode)
    {
        // Enemy cannot smell.
        if (smellMultiplier == 0f) return;
        // If there is a cooldown to pursue, ignore smell.
        if (_enemyController.CooldownToPursue > 0f) return;

        // Smell detected
        if (smellNode == null)
        {
            // PursuePlayer
            _enemyController.TargetSmellNode = null;
            _enemyController.PursuePlayer();
            return;
        }

        if (_enemyController.TargetSmellNode != null && smellNode.Strength < _enemyController.TargetSmellNode.Strength)
        {
            // Weaker smell node than cyrrent target smell node. Ignore.
            return;
        }

        float smellStrength = smellMultiplier * smellNode.Strength;
        float failureRate = Random.Range(0f, 100f);

        if (failureRate > smellStrength)
        {
            // Smell is not obvious. 
            _enemyController.TargetSmellNode = null;
            _enemyController.Wander();
            return;
        }

        // If there is a next smell node, pursue it.
        if (smellNode.NextNode != null)
        {
            // Pursue smell
            _enemyController.RefreshAgentSpeed(_enemySettings.MovementSpeed * pursueSmellSpeedMultiplier);
            _enemyController.SetAgentDestination(smellNode.transform.position);
            _enemyController.TargetSmellNode = smellNode;
            _enemyController.PlaySFX(true);
            _enemyController.PlaySniffParticles(true);
            newSmellDetected = true;
        }
        // If the smell node has no next node, pursue the player instead.
        else
        {
            // PursuePlayer
            _enemyController.TargetSmellNode = null;
            _enemyController.PursuePlayer();
        }

    }
    #endregion
}