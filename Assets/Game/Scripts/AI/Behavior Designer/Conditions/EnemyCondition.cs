using Apotropaic.Data;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

public abstract class EnemyCondition : Conditional
{
    [SerializeField] private SharedEnemyController enemyController = null;
    [SerializeField] private SharedEnemyCollision enemyCollision = null;
    [SerializeField] private SharedEnemySettings enemySettings = null;

    protected EnemyController _enemyController = null;
    protected EnemyCollision _enemyCollision = null;
    protected EnemySettings _enemySettings = null;

    #region PUBLIC
    public override void OnAwake()
    {
        base.OnAwake();

        _enemyController = enemyController.Value;
        _enemyCollision = enemyCollision.Value;
        _enemySettings = enemySettings.Value;
    }
    #endregion

    #region PROTECTED
    #endregion
}