using Apotropaic.Data;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

public class PursueSoundEnemyCondition : EnemyCondition
{
    private float pursueSoundSpeedMultiplier = 0f;
    private float hearingStrength = 0f;
    [SerializeField] private bool soundDetected = false;
    [SerializeField] private SharedSoundType pursuedSoundType = null;
    [SerializeField] private SharedBasicTrap detectedSoundTrap = null;

    #region UNITY
    public override void OnAwake()
    {
        base.OnAwake();
        _enemyController.OnSoundTriggered += OnSoundTriggered;
        pursueSoundSpeedMultiplier = _enemySettings.PursueSoundSpeedMultiplier;
        hearingStrength = _enemySettings.HearingStrength;
    }

    public override void OnBehaviorComplete()
    {
        base.OnBehaviorComplete();
        _enemyController.OnSoundTriggered -= OnSoundTriggered;
    }
    #endregion

    #region PUBLIC
    public override TaskStatus OnUpdate()
    {
        if (soundDetected || _enemyController.TargetSoundPosition != null)
        {
            soundDetected = false;
            return TaskStatus.Success;
        }

        return TaskStatus.Failure;
    }
    #endregion

    #region PRIVATE
    private void OnSoundTriggered(Vector3 position, float strength, float radius, SoundType soundType, BasicTrap basicTrap)
    {
        // If it is the player sound and the enemy is already pursuing the player, ignore the sound.
        if (_enemyController.PursuingPlayer && soundType == SoundType.PlayerSound) return;
        // If the enemy is pursuing a trap sound, ignore player sounds.
        if (_enemyController.PursuingTrapSound && soundType == SoundType.PlayerSound) return;
        // Ignore the sound if there is a smell detected.
        if (_enemyController.SmellDetected) return;
        // If enemy is frozen, ignore the sound. (So we wouldn't call set destination)
        if (_enemyController.Frozen) return;
        // If there is a cooldown to pursue, ignore sound.
        if (_enemyController.CooldownToPursue > 0f) return;

        // Sound is too weak
        if (strength == 0f || radius == 0f) return;

        // Enemy is deaf
        if (hearingStrength == 0f) return;

        float sqrDistance = Vector3.SqrMagnitude(transform.position - position);
        float sqrRadius = radius * radius;

        // Sound is too far
        if (sqrDistance > sqrRadius) return;

        // Calculate the sound strength relative to the enemy location
        float relativeSoundStrength = 1f - ((sqrDistance / sqrRadius) * strength);
        // Add the enemy hearing strength to the relative sound strength to calculate the accuracy
        float accuracy = Mathf.Clamp(hearingStrength + relativeSoundStrength, 0f, 1f);
        // Random radius based on accuracy
        float randRadius = Random.Range(0f, radius - (radius * accuracy));
        // Random Angle from 0 to 180 based on accuracy

        float randAngle = Random.Range((90f * accuracy), 90f);
        // Random right/left offset
        randAngle += (Random.Range(0, 2) == 0) ? 0 : (90f * (1f - accuracy));
        randAngle = 90f - randAngle;

        Vector3 direction = Vector3.Normalize(transform.position - position);
        float enemyOrientation = Vector3.SignedAngle(transform.forward, direction, Vector3.up);
        randAngle += 180 + enemyOrientation;

        // Convert to radians
        randAngle *= Mathf.Deg2Rad;
        float y = Mathf.Cos(randAngle);
        float x = Mathf.Sin(randAngle);
        Vector3 newPos = new Vector3(x, 0, y);
        newPos *= randRadius;
        // Transpose new position to sound position
        Vector3 targetPosition = position + newPos;
        // Validate target position
        targetPosition = _enemyController.ValidateNavigationPosition(targetPosition);

        // If the enemy isn't already pursuing a sound, start pursuing the sound
        if (_enemyController.TargetSoundPosition == null)
        {
            soundDetected = true;

            _enemyController.PlaySFX(true);
        }

        pursuedSoundType.Value = soundType;

        if (soundType == SoundType.TrapSound)
        {
            _enemyController.PursuingTrapSound = true;
            detectedSoundTrap.Value = basicTrap;
        }

        _enemyController.TargetSoundPosition = targetPosition;
        _enemyController.RefreshAgentSpeed(_enemySettings.MovementSpeed * pursueSoundSpeedMultiplier);
        _enemyController.SetAgentDestination(targetPosition);
    }
    #endregion
}
