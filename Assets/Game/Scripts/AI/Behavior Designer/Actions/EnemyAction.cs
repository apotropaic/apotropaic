using Apotropaic.Data;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

public abstract class EnemyAction : Action
{
    [SerializeField] private SharedEnemyController enemyController = null;
    [SerializeField] private SharedEnemySettings enemySettings = null;
    [SerializeField] private SharedPlayerData playerData = null;
    [SerializeField] private SharedFloat minWaypointSqrMagnitude = 0.15f;

    protected EnemyController _enemyController = null;
    protected EnemySettings _enemySettings = null;
    protected PlayerData _playerData = null;
    protected float _minWaypointSqrMagnitude = 0f;

    #region PUBLIC
    public override void OnAwake()
    {
        base.OnStart();

        _enemyController = enemyController.Value;
        _playerData = playerData.Value;
        _minWaypointSqrMagnitude = minWaypointSqrMagnitude.Value;
        _enemySettings = enemySettings.Value;
    }
    #endregion

    #region PRIVATE
    #endregion
}