using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

public class WanderEnemyAction : EnemyAction
{
    #region PUBLIC
    public override TaskStatus OnUpdate()
    {
        return RefreshTargetWaypoint();
    }
    #endregion

    #region PRIVATE
    private TaskStatus RefreshTargetWaypoint()
    {
        Waypoint targetWaypoint = _enemyController.TargetWaypoint;

        float sqrMagnitude = Vector3.SqrMagnitude(transform.position - targetWaypoint.Position);

        if (sqrMagnitude <= _minWaypointSqrMagnitude || _enemyController.IsStuck)
        {
            _enemyController.ResetStuck();

            _enemyController.RefreshTargetWaypoint();
        }

        return TaskStatus.Success;
    }
    #endregion
}