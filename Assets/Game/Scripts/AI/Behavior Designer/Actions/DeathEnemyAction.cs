using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;

public class DeathEnemyAction : EnemyAction
{
    #region PUBLIC
    public override void OnStart()
    {
        base.OnStart();
        _enemyController.ResetAgentPath();
        _enemyController.EnemyAnimator.PlayDeath();
        _enemyController.EnemyAnimator.OnAnimationPlaybackComplete += OnAnimationCompleted;
    }

    public override void OnBehaviorComplete()
    {
        base.OnBehaviorComplete();

        if (_enemyController != null && _enemyController.EnemyAnimator != null)
            _enemyController.EnemyAnimator.OnAnimationPlaybackComplete -= OnAnimationCompleted;
    }

    public override TaskStatus OnUpdate()
    {
        return TaskStatus.Running;
    }
    #endregion

    #region PRIVATE
    private void OnAnimationCompleted()
    {
        _enemyController.DestroyEnemy();
    }
    #endregion
}