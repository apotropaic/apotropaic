using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

public class PursuePlayerEnemyAction : EnemyAction
{
    private bool stayInPatrolArea = false;
    private const float minPlayerDist = 0.5f;

    #region PUBLIC
    public override void OnAwake()
    {
        base.OnAwake();

        stayInPatrolArea = _enemySettings.StayInPatrolArea;
    }

    public override TaskStatus OnUpdate()
    {
        return RefreshPlayerTarget();
    }
    #endregion

    #region PRIVATE
    private TaskStatus RefreshPlayerTarget()
    {
        Vector3 playerPos = _playerData.transform.position;
        float playerDistSqrMagnitude = Vector3.SqrMagnitude(transform.position - playerPos);
        float sqrMaxPlayerDistance = (_enemyController.MaxPursueDistance * _enemyController.MaxPursueDistance);

        // If the enemey should stay in patrol area and the player is outside of the patrol area, ignore player.
        if (stayInPatrolArea)
        {
            float areaRadius = _enemyController.CurrentPatrolArea.AreaRadius;

            float sqrDist = Vector3.SqrMagnitude(playerPos - _enemyController.CurrentPatrolArea.AreaCenter);

            if (sqrDist > areaRadius * areaRadius)
            {
                // Lost player.
                _enemyController.Wander();
                return TaskStatus.Failure;
            }
        }

        if ((playerDistSqrMagnitude > sqrMaxPlayerDistance || _enemyController.CooldownToPursue > 0))
        {
            // Lost player.
            _enemyController.Wander();
            return TaskStatus.Failure;
        }
        // If we are too close to the player, stop
        else if (playerDistSqrMagnitude < minPlayerDist * minPlayerDist)
        {
            _enemyController.CurrentDestination = transform.position;
            _enemyController.SetAgentDestination(_enemyController.CurrentDestination);

            return TaskStatus.Success;
        }
        else
        {
            float currentDestinationSqrMagnitude = Vector3.SqrMagnitude(transform.position - _enemyController.CurrentDestination);

            if (currentDestinationSqrMagnitude <= _minWaypointSqrMagnitude)
            {
                // Refresh target destination.
                _enemyController.CurrentDestination = playerPos;
                _enemyController.SetAgentDestination(_enemyController.CurrentDestination);
            }

            return TaskStatus.Success;
        }
    }
    #endregion
}
