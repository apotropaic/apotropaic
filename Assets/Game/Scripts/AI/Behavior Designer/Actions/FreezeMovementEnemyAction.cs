using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

public class FreezeMovementEnemyAction : EnemyAction
{
    [SerializeField] private SharedFloat freezeMovementDelay = 0f;

    #region PUBLIC
    public override void OnAwake()
    {
        base.OnAwake();
    }

    public override TaskStatus OnUpdate()
    {
        freezeMovementDelay.Value = Mathf.Clamp(freezeMovementDelay.Value - Time.deltaTime, 0f, float.MaxValue);

        if (freezeMovementDelay.Value == 0f)
        {
            _enemyController.Frozen = false;
            _enemyController.Wander();
            _enemyController.StartRepulsionPursueCooldown();
            return TaskStatus.Failure;
        }

        return TaskStatus.Running;
    }
    #endregion

    #region PRIVATE
    #endregion
}
