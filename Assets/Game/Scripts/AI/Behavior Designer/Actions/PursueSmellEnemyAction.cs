using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

public class PursueSmellEnemyAction : EnemyAction
{
    #region PUBLIC
    public override TaskStatus OnUpdate()
    {
        return RefreshSmellNodeTarget();
    }
    #endregion

    #region PRIVATE
    private TaskStatus RefreshSmellNodeTarget()
    {
        SmellNode targetSmellNode = _enemyController.TargetSmellNode;

        // If the target smell node expired, wander.
        // If the enemy has a pursue cooldown, wander.
        if (!targetSmellNode.gameObject.activeInHierarchy || _enemyController.CooldownToPursue > 0f)
        {
            // Lost track of player smell.
            _enemyController.TargetSmellNode = null;
            _enemyController.Wander();
            return TaskStatus.Failure;
        }

        // If enemy shouldn't leave patrol area, don't pursue smell outside of said area.
        if (_enemySettings.StayInPatrolArea)
        {
            float areaRadius = _enemyController.CurrentPatrolArea.AreaRadius;

            float sqrDist = Vector3.SqrMagnitude(targetSmellNode.transform.position - _enemyController.CurrentPatrolArea.AreaCenter);

            if (sqrDist > areaRadius * areaRadius)
            {
                _enemyController.TargetSmellNode = null;
                _enemyController.Wander();
                return TaskStatus.Failure;
            }
        }

        // Square distance between enemy and target smell node.
        float sqrMagnitude = Vector3.SqrMagnitude(transform.position - targetSmellNode.transform.position);

        // If less than min square distance, the enemy arrived at the node.
        if (sqrMagnitude <= _minWaypointSqrMagnitude || _enemyController.IsStuck)
        {
            _enemyController.ResetStuck();

            // If the current smell node has a next smell node, update the target smell node.
            if (targetSmellNode.NextNode != null && _enemyController.CooldownToPursue <= 0f)
            {
                _enemyController.SetAgentDestination(targetSmellNode.NextNode.transform.position);
                _enemyController.TargetSmellNode = targetSmellNode.NextNode;
            }
            else
            {
                // Found player.
                _enemyController.TargetSmellNode = null;
                _enemyController.PursuePlayer();
                return TaskStatus.Failure;
            }

            return TaskStatus.Running;
        }

        return TaskStatus.Running;
    }
    #endregion
}
