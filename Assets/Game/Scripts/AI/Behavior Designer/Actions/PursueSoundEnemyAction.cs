using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

public class PursueSoundEnemyAction : EnemyAction
{
    private float minSoundPlayerPursueDistance = 0f;

    [SerializeField] private SharedSoundType pursuedSoundType = null;
    [SerializeField] private SharedBasicTrap detectedSoundTrap = null;

    private float maxSoundTimeToPursuePlayer = 0f;
    private bool stayInPatrolArea = false;
    private float timePassed = 0f;

    #region PUBLIC
    public override void OnAwake()
    {
        base.OnAwake();

        minSoundPlayerPursueDistance = _enemySettings.MinSoundPlayerPursueDistance;
        maxSoundTimeToPursuePlayer = _enemySettings.MaxSoundTimeToPursuePlayer;
        stayInPatrolArea = _enemySettings.StayInPatrolArea;
    }

    // TODO : Is this working every time the action is triggered?
    public override void OnStart()
    {
        base.OnStart();

        timePassed = 0f;
    }

    public override TaskStatus OnUpdate()
    {
        return PursueSoundPosition();
    }
    #endregion

    #region PRIVATE
    private TaskStatus PursueSoundPosition()
    {
        if (_enemyController.TargetSoundPosition == null)
        {
            return Wander();
        }

        // If enemy shouldn't leave patrol area, don't pursue sound outside of said area.
        if (stayInPatrolArea)
        {
            float areaRadius = _enemyController.CurrentPatrolArea.AreaRadius;

            float sqrDist = Vector3.SqrMagnitude(_enemyController.TargetSoundPosition.Value - _enemyController.CurrentPatrolArea.AreaCenter);

            if (sqrDist > areaRadius * areaRadius || _enemyController.IsStuck)
            {
                _enemyController.ResetStuck();
                _enemyController.TargetSoundPosition = null;
                return Wander();
            }
        }

        SoundType pursuedSound = pursuedSoundType.Value;
        float sqrMagnitude = Vector3.SqrMagnitude(transform.position - _enemyController.TargetSoundPosition.Value);

        // If arrived at target sound position.
        if (sqrMagnitude <= _minWaypointSqrMagnitude || _enemyController.IsStuck)
        {
            _enemyController.ResetStuck();

            if (pursuedSound == SoundType.PlayerSound)
            {
                float playerDistSqrMagnitude = Vector3.SqrMagnitude(transform.position - _playerData.transform.position);
                float sqrMaxPlayerDistance = (minSoundPlayerPursueDistance * minSoundPlayerPursueDistance);

                timePassed += Time.deltaTime;
                // If player is not close, or if time passed since the enemy heard the sound is over the max, back to wander.
                if (playerDistSqrMagnitude > sqrMaxPlayerDistance || (timePassed > maxSoundTimeToPursuePlayer && maxSoundTimeToPursuePlayer > 0))
                {
                    return Wander();
                }
                // Else, pursue player
                else
                {
                    _enemyController.TargetSoundPosition = null;
                    _enemyController.PursuePlayer();
                    return TaskStatus.Failure;
                }
            }
            else if (pursuedSound == SoundType.TrapSound)
            {
                _enemyController.TargetSoundPosition = null;

                BasicTrap basicTrap = detectedSoundTrap.Value;
                _enemyController.DetectedBasicTrap = basicTrap;
                _enemyController.PursuingTrapSound = false;
                return TaskStatus.Failure;
            }
        }

        return TaskStatus.Running;
    }

    private TaskStatus Wander()
    {
        _enemyController.Wander();
        return TaskStatus.Failure;
    }
    #endregion
}