using BehaviorDesigner.Runtime;

[System.Serializable]
public class SharedBasicTrap : SharedVariable<BasicTrap>
{
    public static implicit operator SharedBasicTrap(BasicTrap value) { return new SharedBasicTrap { Value = value }; }
}