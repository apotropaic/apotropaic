using BehaviorDesigner.Runtime;

[System.Serializable]
public class SharedPlayerData : SharedVariable<PlayerData>
{
    public static implicit operator SharedPlayerData(PlayerData value) { return new SharedPlayerData { Value = value }; }
}