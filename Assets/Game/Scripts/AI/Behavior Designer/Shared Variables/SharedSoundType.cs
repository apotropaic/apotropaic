using BehaviorDesigner.Runtime;

[System.Serializable]
public class SharedSoundType : SharedVariable<SoundType>
{
    public static implicit operator SharedSoundType(SoundType value) { return new SharedSoundType { Value = value }; }
}