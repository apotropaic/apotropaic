using BehaviorDesigner.Runtime;

[System.Serializable]
public class SharedEnemyCollision : SharedVariable<EnemyCollision>
{
    public static implicit operator SharedEnemyCollision(EnemyCollision value) { return new SharedEnemyCollision { Value = value }; }
}