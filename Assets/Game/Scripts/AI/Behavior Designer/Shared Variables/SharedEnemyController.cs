using BehaviorDesigner.Runtime;

[System.Serializable]
public class SharedEnemyController : SharedVariable<EnemyController>
{
    public static implicit operator SharedEnemyController(EnemyController value) { return new SharedEnemyController { Value = value }; }
}