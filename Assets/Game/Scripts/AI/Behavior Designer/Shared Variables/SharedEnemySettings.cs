using Apotropaic.Data;
using BehaviorDesigner.Runtime;

[System.Serializable]
public class SharedEnemySettings : SharedVariable<EnemySettings>
{
    public static implicit operator SharedEnemySettings(EnemySettings value) { return new SharedEnemySettings { Value = value }; }
}