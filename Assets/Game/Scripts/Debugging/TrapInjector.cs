using Apotropaic.Data;
using EasyButtons;
using UnityEngine;

public class TrapInjector : MonoBehaviour
{
    [SerializeField] private InventoryManager inventoryManager = null;
    [SerializeField] private BasicTrapSettings bellTrap = null;

    private IInventoryTrapAccessor trapAccessor = null;

    #region UNITY
    private void Start()
    {
        trapAccessor = inventoryManager;
    }
    #endregion

    #region PUBLIC
    [Button]
    public void InjectSoundTrap()
    {
        trapAccessor.AddTrap(bellTrap.UID, 0, 1);
    }
    #endregion

    #region PRIVATE
    #endregion
}
