using UnityEngine;
using Apotropaic.Utility;
using Apotropaic.Data;

public class TransparencyInterpolator : MonoBehaviour
{
    [SerializeField] private TransparencyInterpolationSettings settings = null;
    [SerializeField] private MeshRenderer[] meshRenderers = null;

    private float targetAlpha = 0f;
    private float speed = 0f;

    private int overlappingCount = 0;
    private bool transparent = false;
    private float currentAlpha = 1f;

    #region UNITY
    private void Start()
    {
        targetAlpha = settings.TargetAlpha;
        speed = settings.Speed;

        StandardShaderUtils.Initialize();

        for (int i = 0; i < meshRenderers.Length; i++)
        {
            StandardShaderUtils.ChangeRenderMode(meshRenderers[i].material, StandardShaderUtils.BlendMode.Opaque);
        }
    }

    private void Update()
    {
        MonitorTransparency();
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (overlappingCount == 0)
        {
            for (int i = 0; i < meshRenderers.Length; i++)
            {
                StandardShaderUtils.ChangeRenderMode(meshRenderers[i].material, StandardShaderUtils.BlendMode.Fade);
            }
        }

        overlappingCount++;
        RefreshTransparency();
    }

    private void OnTriggerExit(Collider collision)
    {
        overlappingCount = Mathf.Clamp(overlappingCount - 1, 0, int.MaxValue);
        RefreshTransparency();
    }
    #endregion

    #region PRIVATE
    private void RefreshTransparency()
    {
        transparent = (overlappingCount > 0);
    }

    private void MonitorTransparency()
    {
        if (transparent && currentAlpha != targetAlpha)
        {
            ApplyTransparency(-speed);
        }
        else if (!transparent && currentAlpha != 1f)
        {
            ApplyTransparency(speed);


            if (currentAlpha == 1f)
            {
                for (int i = 0; i < meshRenderers.Length; i++)
                {
                    StandardShaderUtils.ChangeRenderMode(meshRenderers[i].material, StandardShaderUtils.BlendMode.Opaque);
                }
            }
        }
    }

    private void ApplyTransparency(float val)
    {
        for (int i = 0; i < meshRenderers.Length; i++)
        {
            Color currentColor = meshRenderers[i].material.color;
            currentColor.a = Mathf.Clamp(currentColor.a + val, targetAlpha, 1f);
            currentAlpha = currentColor.a;
            meshRenderers[i].material.color = currentColor;
        }
    }
    #endregion
}
