using UnityEngine;

public class SceneLightController : MonoBehaviour
{
    [SerializeField] private Light sceneLight = null;
    [SerializeField] private GameplayData gameplayData = null;

    [SerializeField] private Gradient eveningColors = null;
    [SerializeField] private float initialEveningIntensity = 1f;
    [SerializeField] private float lateEveningIntensity = 1f;
    [SerializeField] private AnimationCurve eveningInterpolationCurve = null;

    [SerializeField] private Gradient nightColors = null;
    [SerializeField] private float initialNightIntensity = 1f;
    [SerializeField] private float lateNightIntensity = 1f;
    [SerializeField] private AnimationCurve nightInterpolationCurve = null;

    #region UNITY
    private void Update()
    {
        switch (gameplayData.TimeOfDay)
        {
            case TimeOfDay.Evening:
                InterpolateEvening(gameplayData.CurrentEveningProgress);
                break;
            case TimeOfDay.Night:
                InterpolateNight(gameplayData.CurrentNightProgress);
                break;
        }
    }
    #endregion

    #region PUBLIC
    #endregion

    #region PRIVATE
    private void InterpolateEvening(float delta)
    {
        InterpolateLight(eveningColors, initialEveningIntensity, lateEveningIntensity, delta, eveningInterpolationCurve);
    }

    private void InterpolateNight(float delta)
    {
        InterpolateLight(nightColors, initialNightIntensity, lateNightIntensity, delta, nightInterpolationCurve);
    }

    private void InterpolateLight(Gradient colorGrad, float initialIntensity, float lateIntensity, float delta, AnimationCurve animationCurve)
    {
        float curveDelta = animationCurve.Evaluate(delta);

        Color newColor = colorGrad.Evaluate(curveDelta);
        float newIntensity = Mathf.Lerp(initialIntensity, lateIntensity, curveDelta);

        sceneLight.color = newColor;
        sceneLight.intensity = newIntensity;
    }
    #endregion
}
