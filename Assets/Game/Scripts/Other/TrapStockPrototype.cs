using Apotropaic.Data;
using Apotropaic.Events;
using System.Linq;
using UnityEngine;


//
// This script updates the traps on the player HUD and their switching, this is not a prototype.
// 
public class TrapStockPrototype : MonoBehaviour
{
    [SerializeField] private GameObject[] TrapSlots;
    [SerializeField] private TrapSpawnerData trapSpawnerData = null;
    [SerializeField] private PlayerInventory playerInventory = null;
    [SerializeField] private TrapSettingsList trapSettings = null;

    #region UNITY

    private void Start()
    {
        InitializeTrapStockPrototype();
    }
    private void OnEnable()
    {
        GameplayEvents.OnPlacedTrap += RefreshStock;
        GameplayEvents.OnToggleTrap += RefreshHighlightedTrap;
        UIEvents.OnConversationEnded += RefreshStock;
        
    }
    private void Update()
    {
    }

    private void OnDisable()
    {
        GameplayEvents.OnPlacedTrap -= RefreshStock;
        GameplayEvents.OnToggleTrap -= RefreshHighlightedTrap;
        UIEvents.OnConversationEnded -= RefreshStock;

    }

    #endregion

    #region PUBLIC
    public void InitializeTrapStockPrototype()
    {
        if(trapSpawnerData.CurrentSelectedTrap == null)
        {
            foreach(GameObject gameObject in TrapSlots)
            {
                gameObject.GetComponentInChildren<TrapSlot>().SetSlot(null, 0);   
            }
        }
        else
        {
            for (int i = 0; i < playerInventory.TrapList.Count; i++)
            {
                if(playerInventory.TrapList.ElementAt(i).Key == trapSpawnerData.CurrentSelectedTrap.UID)
                {
                    TrapSlots[1].SetActive(true);
                    TrapSlots[1].GetComponentInChildren<TrapSlot>().SetSlot(trapSpawnerData.CurrentSelectedTrap, playerInventory.TrapList.ElementAt(i).Value);

                    if(i == 0)
                    {
                        TrapSlots[0].SetActive(true);
                        TrapSlots[0].GetComponentInChildren<TrapSlot>().SetSlot(FetchSettingsFromTrapList(playerInventory.TrapList.ElementAt(playerInventory.TrapList.Count - 1).Key),
                                                                                playerInventory.TrapList.ElementAt(playerInventory.TrapList.Count - 1).Value);
                    }
                    else
                    {
                        TrapSlots[0].SetActive(true);
                        TrapSlots[0].GetComponentInChildren<TrapSlot>().SetSlot(FetchSettingsFromTrapList(playerInventory.TrapList.ElementAt(i - 1).Key),
                                                                                playerInventory.TrapList.ElementAt(i - 1).Value);
                    }
                    
                    if(i == playerInventory.TrapList.Count - 1)
                    {
                        TrapSlots[2].SetActive(true);
                        TrapSlots[2].GetComponentInChildren<TrapSlot>().SetSlot(FetchSettingsFromTrapList(playerInventory.TrapList.ElementAt(0).Key),
                                                                                playerInventory.TrapList.ElementAt(0).Value);
                    }
                    else
                    {
                        TrapSlots[2].SetActive(true);
                        TrapSlots[2].GetComponentInChildren<TrapSlot>().SetSlot(FetchSettingsFromTrapList(playerInventory.TrapList.ElementAt(i + 1).Key),
                                                                                playerInventory.TrapList.ElementAt(i + 1).Value);
                    }
                    
                }
            }
        }
    }

    
    #endregion

    #region PRIVATE
    // Fetches trap information from the  list of global traps using the ID
    private BasicTrapSettings FetchSettingsFromTrapList(int _uID)
    {
        BasicTrapSettings _trapSettings = null;
        for (int i = 0; i < trapSettings.Traps.Length; i++)
        {
            if (trapSettings.Traps[i].UID == _uID)
                _trapSettings = trapSettings.Traps[i];
        }
        return _trapSettings;
    }
    private void RefreshStock()
    {
        InitializeTrapStockPrototype();
    }

    private void RefreshHighlightedTrap()
    {
        InitializeTrapStockPrototype();
    }
    #endregion
}