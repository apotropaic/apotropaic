using EasyButtons;
using UnityEngine;
using Apotropaic.Data;

// This script rescales the NPC sprites to look original scale as if viewed from the front. 
// Uses the camera angle and some trigonometry to scale up the sprite vertically to look the original size despite the camera angle
public class ObjectsOrthographicRescaler : MonoBehaviour
{
    [SerializeField] private CameraSettings settings = null;

    private float cameraAngle;
    private float cameraAngleRad = 0f;

    private bool settingsLoaded = false;

    // Cache
    private float? objectScale = null;

    #region UNITY

    private void Awake()
    {
        LoadSettings();
    }
    private void Start()
    {
        RescaleObject();
    }
    #endregion

    #region PUBLIC
    [Button]
    public void RescaleObject()
    {
        if (!settingsLoaded)
            LoadSettings();

        if (objectScale == null)
        {
            cameraAngleRad = Mathf.Deg2Rad * cameraAngle;
            objectScale = transform.localScale.y;
            transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y * (1 / Mathf.Cos(cameraAngleRad)), transform.localScale.z);
        }
    }

    [Button]
    public void ResetScale()
    {
        if (objectScale != null)
        {
            transform.localScale = new Vector3(transform.localScale.x, objectScale.Value, transform.localScale.z);
            objectScale = null;
        }
    }
    #endregion

    #region PRIVATE

    private void LoadSettings()
    {
        cameraAngle = settings.Angle;
        settingsLoaded = true;
    }

    #endregion
}
