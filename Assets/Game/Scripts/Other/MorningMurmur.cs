using Apotropaic.Data;
using UnityEngine;
using Apotropaic.Events;

public class MorningMurmur : MonoBehaviour
{
    [SerializeField] private SceneLoaderTemp sceneLoader = null;
    [SerializeField] private SceneSettings sceneSettings = null;

    #region UNITY
    private void Start()
    {
        CoreEvents.OnOfficeTransition += LoadOfficeScene;
    }

    private void OnDestroy()
    {
        CoreEvents.OnOfficeTransition -= LoadOfficeScene;
    }
    #endregion

    #region PUBLIC
    public void LoadOfficeScene()
    {
        sceneLoader.LoadScene(sceneSettings.OfficesScene);
    }
    #endregion

    #region PRIVATE
    #endregion
}
