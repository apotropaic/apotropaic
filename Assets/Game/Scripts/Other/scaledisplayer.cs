using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scaledisplayer : MonoBehaviour
{
    Transform trans;
    // Start is called before the first frame update
    void Start()
    {
        trans = transform.parent.transform;
        GetComponent<TextMesh>().text = trans.name+": "+ trans.localScale.x+"*"+ trans.localScale.y + "*"+ trans.localScale.z;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
