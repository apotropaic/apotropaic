using UnityEngine;
public class PositionInterpolator : MonoBehaviour
{
    [SerializeField] private AnimationCurve interpolationCurve = null;
    [SerializeField] private float interpolationTime = 0f;
    [SerializeField] private Vector3 interpolationOffset = Vector3.zero;

    private Vector3 originalPos = Vector3.zero;
    private Vector3 targetPos = Vector3.zero;
    private int direction = 1;
    private float timePassed = 0f;

    #region UNITY
    private void OnEnable()
    {
        Initialize();
    }

    private void Update()
    {
        Interpolate();
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position, transform.position + interpolationOffset);
    }
    #endregion

    #region PUBLIC
    #endregion

    #region PRIVATE
    private void Initialize()
    {
        originalPos = transform.position;
        targetPos = originalPos + interpolationOffset;
    }

    private void Interpolate()
    {
        Vector3 newPos = Vector3.zero;
        timePassed = Mathf.Clamp(timePassed + Time.deltaTime, 0f, interpolationTime);
        float delta = interpolationCurve.Evaluate(timePassed / interpolationTime);

        if (direction == 1)
        {
            newPos = Vector3.Lerp(originalPos, targetPos, delta);
        }
        else
        {
            newPos = Vector3.Lerp(targetPos, originalPos, delta);
        }

        transform.position = newPos;

        if (timePassed == interpolationTime)
        {
            timePassed = 0f;
            direction *= -1;
        }
    }
    #endregion
}