using Apotropaic.Data;
using UnityEngine;

[CreateAssetMenu(fileName = "Trap Spawner Data", menuName = "Scriptable Object/Trap Spawner Data")]
public class TrapSpawnerData : ScriptableObject
{
    private int currentTrapSelectionIndex = 0;
    //private int trapsCount = 0;
    //private int[] trapsCurrentStock = null;


    private BasicTrapSettings currentSelectedTrap = null;
    
    #region PUBLIC
    
    public int CurrentTrapSelectionIndex { get { return currentTrapSelectionIndex; } set { currentTrapSelectionIndex = value; } }
    //public int TrapsCount { get { return trapsCount; } set { trapsCount = value; } }
    //public int[] TrapsStock { get { return trapsCurrentStock; } set { trapsCurrentStock = value; } }

    public BasicTrapSettings CurrentSelectedTrap { get { return currentSelectedTrap; } set { currentSelectedTrap = value; } }
    
    #endregion
}