using UnityEngine;
using Apotropaic.Core;
using UnityEngine.SceneManagement;


public class SceneLoaderTemp : MonoBehaviour
{
    #region PUBLIC
    public void LoadScene(SceneReference sceneReference)
    {
        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            if (SceneManager.GetSceneAt(i).name == sceneReference)
            {
                return;
            }
        }

        Scene scene = SceneManager.GetSceneByPath(sceneReference);

        if (scene.isLoaded == false)
        {
            SceneLoader.Instance.UnloadMainScene();
            SceneLoader.Instance.LoadScene(sceneReference, true);
        }
    }
    #endregion
}
