using UnityEngine;
using Apotropaic.Events;

public class NightTrigger : MonoBehaviour
{
    private OfficesManager officesManager;
    [SerializeField] private GameplayData gameplayData;

    #region UNITY

    private void Start()
    {
        CoreEvents.OnNightTransition += NightTransition;
    }

    // Transition to night from the offices.
    private void NightTransition()
    {
        officesManager.StartNightPhase();
    }


    private void OnDestroy()
    {
        CoreEvents.OnNightTransition -= NightTransition;
    }
    #endregion

    #region PUBLIC
    public void SetOfficesManager(OfficesManager _officesManager)
    {
        officesManager = _officesManager;
    }
    #endregion

    #region PRIVATE
    #endregion
}
