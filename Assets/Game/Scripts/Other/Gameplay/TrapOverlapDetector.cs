using UnityEngine;

public class TrapOverlapDetector : MonoBehaviour
{
    [SerializeField] private MeshRenderer meshRenderer;
    [SerializeField] private Color overlapShade;
    [SerializeField] private Color noOverlapShade;

    private int overlapCount = 0;

    #region UNITY
    private void OnEnable()
    {
        meshRenderer.material.color = noOverlapShade;
    }

    private void OnDisable()
    {
        meshRenderer.material.color = Color.white;
    }

    private void OnTriggerEnter(Collider collision)
    {
        overlapCount++;
        meshRenderer.material.color = overlapShade;
    }

    private void OnTriggerExit(Collider collision)
    {
        overlapCount = Mathf.Clamp(overlapCount - 1, 0, int.MaxValue);

        if (overlapCount == 0)
        {
            meshRenderer.material.color = noOverlapShade;
        }
    }
    #endregion

    #region PUBLIC
    public bool IsOverlapping
    {
        get { return (overlapCount > 0); }
    }
    #endregion

    #region PRIVATE
    #endregion
}