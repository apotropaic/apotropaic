using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;

public class AIClickToMove : MonoBehaviour
{
    [SerializeField] private NavMeshAgent navMeshAgent = null;

    #region UNITY
    #endregion

    #region PUBLIC
    public void OnLeftMouseButton(InputAction.CallbackContext context)
    {
        if (!context.started) return;

        Vector2 mousePos = Mouse.current.position.ReadValue();

        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(mousePos), out hit, 100))
        {
            SetTargetDestination(hit.point);
        }
    }
    #endregion

    #region PRIVATE
    private void SetTargetDestination(Vector3 destination)
    {
        navMeshAgent.destination = destination;
    }
    #endregion
}
