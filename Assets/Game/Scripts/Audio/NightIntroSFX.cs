using UnityEngine;
using Apotropaic.Events;

public class NightIntroSFX : MonoBehaviour
{
    private AudioSource sfxSource = null;

    #region UNITY
    private void Start()
    {
        sfxSource = GetComponent<AudioSource>();
        GameplayEvents.OnNightPhase += OnNight;
    }

    private void OnDestroy()
    {
        GameplayEvents.OnNightPhase -= OnNight;
    }
    #endregion

    #region PUBLIC
    #endregion

    #region PRIVATE
    private void OnNight()
    {
        sfxSource.Play();
    }
    #endregion
}
