namespace Apotropaic.Utility
{
    public enum Operator
    {
        AND = 0,
        OR = 1,
        NOT = 2
    }
}