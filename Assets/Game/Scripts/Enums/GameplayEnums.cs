// Traps

public enum TrapType
{
    Physical = 0,
    Spiritual = 1,
    Both = 2
}

public enum TrapDetection
{
    Collision = 0,
    Smell = 1,
    Sound = 2
}

public enum TrapLifespan
{
    MultipleUses = 0,
    SingleUse = 1,
    Permanent = 2,
    Time = 3
}

public enum TrapState
{
    Placement = 0,
    Active = 1
}

public enum SoundType
{
    PlayerSound = 0,
    TrapSound = 1
}

public enum Speaker
{
    Eloise = 0,
    Dan = 1,
    Murmur = 2,
    Smurl = 3,
    Bonnie = 4,
    Jenna = 5,
    Mom = 6
}

public enum TimeOfDay
{
    Morning = 0,
    Evening = 1,
    Night = 2
}