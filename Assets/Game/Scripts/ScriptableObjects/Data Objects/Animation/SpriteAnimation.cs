using Apotropaic.Utility;
using UnityEngine;

[CreateAssetMenu(fileName = "Sprite Animation", menuName = "Scriptable Object/Animations/Sprite Animation", order = 0)]
public class SpriteAnimation : ScriptableObject
{
    [SerializeField] private Sprite[] frames = null;
    [SerializeField] private bool useDefaultFrameRate = true;
    [SerializeField] private float animationSpeed = 1f;
    [DrawIf("useDefaultFrameRate", Operator.AND, DrawIfAttribute.DisablingType.DontDraw, false)] [SerializeField] private int frameRate = 24;
    [SerializeField] private bool loop = false;

    private int? animationNameHashCode = null;

    #region PUBLIC
    public Sprite[] Frames { get { return frames; } }
    public bool UseDefaultFrameRate { get { return useDefaultFrameRate; } }
    public int FrameRate { get { return frameRate; } }
    public float AnimationSpeed { get { return animationSpeed; } }
    public bool Loop { get { return loop; } }

    public int NameHashCode
    {
        get
        {
            if (animationNameHashCode == null)
            {
                animationNameHashCode = name.GetHashCode();
            }

            return animationNameHashCode.Value;
        }
    }
    #endregion
}