using UnityEngine;

[CreateAssetMenu(fileName = "Character Animations Package", menuName = "Scriptable Object/Animations/Packages/Character Animations Package", order = 0)]
public class CharacterAnimationsPackage : ScriptableObject
{
    [SerializeField] private SpriteAnimation idleAnimation = null;

    #region PUBLIC
    public SpriteAnimation IdleAnimation { get { return idleAnimation; } }
    #endregion
}