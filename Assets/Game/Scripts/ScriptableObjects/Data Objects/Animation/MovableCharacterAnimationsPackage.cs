using UnityEngine;

[CreateAssetMenu(fileName = "Movable Character Animations Package", menuName = "Scriptable Object/Animations/Packages/Movable Character Animations Package", order = 0)]
public class MovableCharacterAnimationsPackage : CharacterAnimationsPackage
{
    [SerializeField] private SpriteAnimation idleBackAnimation = null;
    [SerializeField] private SpriteAnimation walkRightAnimation = null;
    [SerializeField] private SpriteAnimation walkFrontAnimation = null;
    [SerializeField] private SpriteAnimation walkBackAnimation = null;

    #region PUBLIC
    public SpriteAnimation IdleBackAnimation { get { return idleBackAnimation; } }
    public SpriteAnimation WalkRightAnimation { get { return walkRightAnimation; } }
    public SpriteAnimation WalkFrontAnimation { get { return walkFrontAnimation; } }
    public SpriteAnimation WalkBackAnimation { get { return walkBackAnimation; } }
    #endregion
}