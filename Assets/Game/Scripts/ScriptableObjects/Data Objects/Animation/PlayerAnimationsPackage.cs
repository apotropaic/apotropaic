using UnityEngine;

[CreateAssetMenu(fileName = "Player Animations Package", menuName = "Scriptable Object/Animations/Packages/Player Animations Package", order = 0)]
public class PlayerAnimationsPackage : MovableCharacterAnimationsPackage
{
    [SerializeField] private SpriteAnimation tiptoeAnimation = null;
    [SerializeField] private SpriteAnimation placingTrapAnimation = null;

    #region PUBLIC
    public SpriteAnimation TiptoeAnimation { get { return tiptoeAnimation; } }
    public SpriteAnimation PlacingTrapAnimation { get { return placingTrapAnimation; } }
    #endregion
}