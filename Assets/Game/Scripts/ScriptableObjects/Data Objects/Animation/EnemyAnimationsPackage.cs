using UnityEngine;

[CreateAssetMenu(fileName = "Enemy Animations Package", menuName = "Scriptable Object/Animations/Packages/Enemy Animations Package", order = 0)]
public class EnemyAnimationsPackage : MovableCharacterAnimationsPackage
{
    [SerializeField] private SpriteAnimation attackAnimation = null;
    [SerializeField] private SpriteAnimation deathAnimation = null;

    #region PUBLIC
    public SpriteAnimation AttackAnimation { get { return attackAnimation; } }
    public SpriteAnimation DeathAnimation { get { return deathAnimation; } }
    #endregion
}
