using System;
using UnityEngine;

[CreateAssetMenu(fileName = "Input Data", menuName = "Scriptable Object/Data Objects/Input Data", order = 0)]
public class InputData : ScriptableObject
{
    [NonSerialized] private InputType currentInputType = InputType.KeyboardMouse;

    #region PUBLIC
    public InputType CurrentInputType { get { return currentInputType; } set { currentInputType = value; } }
    #endregion
}