using Apotropaic.Data;
using UnityEngine;


[CreateAssetMenu(fileName = "Traps List", menuName = "Scriptable Object/Data Objects/Traps List", order = 0)]
public class TrapSettingsList : ScriptableObject
{
    [SerializeField] private BasicTrapSettings[] traps = null;

    #region PUBLIC

    public BasicTrapSettings[] Traps { get { return traps; } set { traps = value; } }

    #endregion

}
