using System;
using UnityEngine;

[CreateAssetMenu(fileName = "Gameplay Data", menuName = "Scriptable Object/Data Objects/Gameplay Data", order = 0)]
public class GameplayData : ScriptableObject
{
    // TODO remove testing serialization
    [NonSerialized] private int nightsCleared = 0;
    [NonSerialized] private bool playedNightTutorial = false;
    [NonSerialized] private int conversationsThisDay = 0;

    //TODO: use settings instead
    [SerializeField] private int conversationsPerDay = 3;

    [NonSerialized] private bool invincibility = false;
    [NonSerialized] private bool infiniteTraps = false;
    [NonSerialized] private bool enableOfficeMurmur = false;

    private float currentEveningProgress = 0f;
    private float currentNightProgress = 0f;
    private TimeOfDay timeOfDay = TimeOfDay.Morning;

    #region UNITY
    #endregion

    #region PUBLIC
    public float CurrentEveningProgress { get { return currentEveningProgress; } set { currentEveningProgress = value; } }
    public float CurrentNightProgress { get { return currentNightProgress; } set { currentNightProgress = value; } }
    public int NightsCleared { get { return nightsCleared; } set { nightsCleared = value; } }
    public bool FinishedNightTutorial { get { return playedNightTutorial; } set { playedNightTutorial = value; } }
    public int ConversationsThisDay { get { return conversationsThisDay; } set { conversationsThisDay = value; } }
    public int ConversationsPerDay { get { return conversationsPerDay; } }
    public bool Invincibility { get { return invincibility; } set { invincibility = value; } }
    public bool InfiniteTraps { get { return infiniteTraps; } set { infiniteTraps = value; } }
    public bool EnableOfficeMurmur { get { return enableOfficeMurmur; } set { enableOfficeMurmur = value; } }
    public TimeOfDay TimeOfDay { get { return timeOfDay; } set { timeOfDay = value; } }
    #endregion

    #region PRIVATE
    #endregion
}
