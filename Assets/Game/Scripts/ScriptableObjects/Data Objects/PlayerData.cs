using UnityEngine;
using Apotropaic.Core;

[CreateAssetMenu(fileName = "Player Data", menuName = "Scriptable Object/Data Objects/Player Data", order = 0)]
public class PlayerData : ScriptableObject
{
    private int currentHealth = 0;
    private int maxHealth = 0;
    private int[] currentTrapsStock = null;
    private Transform playerTransform = null;
    private Transform cameraTargetTransform = null;
    private IInventoryTrapAccessor inventoryTrapAccessor = null;

    #region PUBLIC
    public int CurrentHealth { get { return currentHealth; } set { currentHealth = value; } }
    public int MaxHealth { get { return maxHealth; } set { maxHealth = value; } }
    public int[] CurrentTrapsStock { get { return currentTrapsStock; } set { currentTrapsStock = value; } }
    public Transform transform { get { return playerTransform; } set { playerTransform = value; } }
    public Transform CameraTargetTransform { get { return cameraTargetTransform; } set { cameraTargetTransform = value; } }
    public IInventoryTrapAccessor InventoryTrapAccessor { get { return inventoryTrapAccessor; } set { inventoryTrapAccessor = value; } }
    #endregion
}