namespace Apotropaic.Core
{
    public interface IPlayerDataSetter
    {
        public int Currency { set; }
    }
}