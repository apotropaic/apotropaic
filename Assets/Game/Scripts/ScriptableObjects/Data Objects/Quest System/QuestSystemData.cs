using System;
using UnityEngine;

[CreateAssetMenu(fileName = "QuestSystemData", menuName = "Scriptable Object/Data Objects/Quest System Data", order = 2)]
public class QuestSystemData : ScriptableObject
{
    #region PUBLIC
    public QuestData[] QuestDatas { get { return questDatas; } }
    public int CurrentActiveQuest { get { return currentActiveQuest; } set { currentActiveQuest = value; } }

    public ConversationData EndQuestSuccessConversation { get { return endQuestSuccessConversation; } }

    #endregion

    #region PRIVATE
    [SerializeField] private QuestData[] questDatas;

    [SerializeField] private ConversationData endQuestSuccessConversation;

    private int currentActiveQuest = 0;

    #endregion
}

// Data for each individual quest for the list
[Serializable]
public class QuestData
{
    private Vector3 decidedLocation;
    private Quaternion rotation;
    [NonSerialized] private bool objectiveCompleted = false;
    [NonSerialized] private bool isActive = false;
    [SerializeField] private string questTitle = "";
    [SerializeField] private ConversationData startConversation;
    [SerializeField] private ConversationData[] failureConversations;
    [SerializeField] [Range(0.0f, 1.0f)] private float spawnTime;
    [SerializeField] [Range(0.0f, 1.0f)] private float endTime;

    public string QuestTitle { get { return questTitle; } }
    public bool ObjectiveComplete { get { return objectiveCompleted; } set { objectiveCompleted = value; } }
    public bool IsActive { get { return isActive; } set { isActive = value; } }
    public Vector3 DecidedLocation { get { return decidedLocation; } set { decidedLocation = value; } }
    public Quaternion Rotation { get { return rotation; } set { rotation = value; } }
    public ConversationData StartConversation { get { return startConversation; } }
    public ConversationData[] FailureConversations { get { return failureConversations; } }
    public float SpawnTime { get { return spawnTime; } }
    public float EndTime { get { return endTime; } }
}
