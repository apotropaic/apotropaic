using UnityEngine;

// Holds conversation lists for the various lists.

[CreateAssetMenu(fileName = "NPCData", menuName = "Scriptable Object/Data Objects/NPC Data", order = 2)]
public class NPCData : ScriptableObject
{
    #region PUBLIC
    public ConversationData[] TrapConversationList { get { return trapConversationList; } }

    public ConversationData[] PersonalConversationList { get { return personalConversationLst; } }

    public ConversationData[] NoConversationLoop { get { return noConversationLoop; } }
    public Sprite GetSprite { get { return sprite; } }

    #endregion

    #region PRIVATE
    [SerializeField] private ConversationData[] trapConversationList;
    [SerializeField] private ConversationData[] personalConversationLst;
    [SerializeField] private ConversationData[] noConversationLoop;
    [SerializeField] private Sprite sprite;

    #endregion
}
