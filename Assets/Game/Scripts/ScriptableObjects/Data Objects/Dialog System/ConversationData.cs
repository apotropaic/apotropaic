using Apotropaic.Data;
using System;
using UnityEngine;

// Holds data for each conversation

[CreateAssetMenu(fileName = "Dialog Data", menuName = "Scriptable Object/Data Objects/Conversation Data", order = 0)]
public class ConversationData : ScriptableObject
{
    #region private
    [SerializeField] private string optionPreface;
    [SerializeField] private SentenceData[] sentences;
    [SerializeField] private BasicTrapSettings trapToUnlock;

    [SerializeField] private Color textColor = Color.black;
    [SerializeField] private int unlockDay = 0;

    [NonSerialized] private bool unlocked = false;
    [NonSerialized] private bool talked = false;


    #endregion

    #region public

    public SentenceData[] Sentences { get { return sentences; } }

    public BasicTrapSettings TrapToUnlock {  get { return trapToUnlock; } }
    public Color TextColor {  get { return textColor; } }

    public int UnlockDay { get { return unlockDay; } }

    public bool Unlcoked { get { return unlocked; } set { unlocked = value; } }
    public bool Talked { get { return talked; } set { talked = value; } }

    public string OptionPreface { get { return optionPreface; } }

    virtual public bool TransitionToNight { get { return false; } }
    virtual public bool TransitionToOffice { get { return false; } }
    #endregion
}


// data for each sentence in the conversation
[Serializable]
public class SentenceData
{
    #region private

    [SerializeField] string sentence;
    [SerializeField] Speaker speaker;
    [SerializeField] Sprite reactionSprite;
    [SerializeField] bool showEloise;
    [SerializeField] bool showSpeaker;

    #endregion

    #region public

    public string Sentence { get { return sentence; } }

    public Speaker SpeakerName { get { return speaker; } }
    public Sprite ReactionSprite { get { return reactionSprite; } }
    public bool ShowEloise { get { return showEloise; } }
    public bool ShowSpeaker { get { return showSpeaker; } }
    #endregion
}
