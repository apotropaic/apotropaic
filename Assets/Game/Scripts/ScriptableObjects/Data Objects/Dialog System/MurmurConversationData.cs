using UnityEngine;

// Addition flags to enable murmur conversations to be able to enable  transitions in scenes.
[CreateAssetMenu(fileName = "Dialog Data", menuName = "Scriptable Object/Data Objects/Murmur Conversation Data", order = 0)]
public class MurmurConversationData : ConversationData
{
    [SerializeField] private bool transitionToNight = false;
    [SerializeField] private bool transitionToOffice = false;

    public override bool TransitionToNight { get { return transitionToNight; } }
    public override bool TransitionToOffice { get { return transitionToOffice; } }

}
