using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Inventory Data", menuName = "Scriptable Object/Data Objects/Inventory Data")]
public class PlayerInventory : ScriptableObject
{
    [SerializeField] private int slots;
    [SerializeField] private int maximumQuantity;
    [NonSerialized] private int currency = 0;
    [NonSerialized] private Dictionary<int, int> trapList = new Dictionary<int, int>();

    #region PUBLIC

    public int Slots { get { return slots; } set { slots = value; } }
    public int Currency { get { return currency; } set { currency = value; } }
    public int MaximumQuantity { get { return maximumQuantity; } set { maximumQuantity = value; } }
    public Dictionary<int, int> TrapList { get { return trapList; } set { trapList = value; } }

    #endregion

    #region PRIVATE
    #endregion
}
