using System;
using UnityEngine;

namespace Apotropaic.Data
{
    [CreateAssetMenu(fileName = "Playtest Settings", menuName = "Scriptable Object/Settings/Playtest Settings", order = 0)]
    public class PlaytestSettings : GameSettings<PlaytestPreset>
    {
        #region PUBLIC
        public float LoadingTime { get { return CurrentPreset.LoadingTime; } }
        public bool OverrideData { get { return CurrentPreset.OverrideData; } }
        public bool FinishedNightTutorial { get { return CurrentPreset.FinishedNightTutorial; } }
        public bool InfiniteTraps { get { return CurrentPreset.InfiniteTraps; } }
        public bool EnableOfficeMurmur { get { return CurrentPreset.EnableOfficeMurmur; } }
        public bool InvinciblePlayer { get { return CurrentPreset.InvinciblePlayer; } }
        public int NightsCleared { get { return CurrentPreset.NightsCleared; } }
        public int Currency { get { return CurrentPreset.Currency; } }
        #endregion
    }

    [Serializable]
    public class PlaytestPreset : Preset
    {
        [SerializeField] [Range(0.3f, 2f)] private float loadingTime = 2f;
        [Header("Override Options")]
        [SerializeField] private bool overrideData = false;
        [SerializeField] private bool finishedNightTutorial = false;
        [SerializeField] private bool infiniteTraps = false;
        [SerializeField] private bool enableOfficeMurmur = false;
        [SerializeField] private bool invinciblePlayer = false;
        [SerializeField] private int nightsCleared = 0;
        [SerializeField] private int currency = 200;

        #region PUBLIC
        public float LoadingTime { get { return loadingTime; } }
        public bool OverrideData { get { return overrideData; } }
        public bool FinishedNightTutorial { get { return finishedNightTutorial; } }
        public bool InfiniteTraps { get { return infiniteTraps; } }
        public bool EnableOfficeMurmur { get { return enableOfficeMurmur; } }
        public bool InvinciblePlayer { get { return invinciblePlayer; } }
        public int NightsCleared { get { return nightsCleared; } }
        public int Currency { get { return currency; } }
        #endregion
    }
}