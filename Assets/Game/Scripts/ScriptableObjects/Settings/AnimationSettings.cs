using System;
using UnityEngine;

namespace Apotropaic.Data
{
    [CreateAssetMenu(fileName = "Animation Settings", menuName = "Scriptable Object/Settings/Animation Settings", order = 0)]
    public class AnimationSettings : GameSettings<AnimationPreset>
    {
        #region PUBLIC
        public int DefaultFrameRate { get { return CurrentPreset.DefaultFrameRate; } }
        #endregion
    }

    [Serializable]
    public class AnimationPreset : Preset
    {
        [SerializeField] [Range(1, 120)] private int defaultFrameRate = 24;

        public int DefaultFrameRate { get { return defaultFrameRate; } }
    }
}