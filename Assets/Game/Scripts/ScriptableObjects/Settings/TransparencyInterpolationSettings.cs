using System;
using UnityEngine;

namespace Apotropaic.Data
{
    [CreateAssetMenu(fileName = "Transparency Interpolation Settings", menuName = "Scriptable Object/Settings/Transparency Interpolation Settings", order = 0)]
    public class TransparencyInterpolationSettings : GameSettings<TransparencyInterpolationPreset>
    {
        #region PUBLIC
        public float TargetAlpha { get { return CurrentPreset.TargetAlpha; } }
        public float Speed { get { return CurrentPreset.Speed; } }
        #endregion
    }

    [Serializable]
    public class TransparencyInterpolationPreset : Preset
    {
        [SerializeField] private float targetAlpha = 0.2f;
        [SerializeField] private float speed = 0.02f;

        #region PUBLIC
        public float TargetAlpha { get { return targetAlpha; } }
        public float Speed { get { return speed; } }
        #endregion
    }
}