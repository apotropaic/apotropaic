using System;
using UnityEngine;

namespace Apotropaic.Data
{
    [CreateAssetMenu(fileName = "Inventory Settings", menuName = "Scriptable Object/Settings/Inventory Settings", order = 0)]
    public class InventorySettings : GameSettings<InventoryPreset>
    {
        #region PUBLIC
        public int StartCurrency { get { return CurrentPreset.StartCurrency; } }
        public BasicTrapSettings[] TrapsUnlockedByDefault { get { return CurrentPreset.TrapsUnlockedByDefault; } }

        #endregion
    }

    [Serializable]
    public class InventoryPreset : Preset
    {
        [SerializeField] private int startCurrency = 200;
        [SerializeField] private BasicTrapSettings[] trapsUnlockedByDefault = null;

        public int StartCurrency { get { return startCurrency; } }
        public BasicTrapSettings[] TrapsUnlockedByDefault { get { return trapsUnlockedByDefault; } }
    }
}