using System;
using UnityEngine;

namespace Apotropaic.Data
{
    [CreateAssetMenu(fileName = "Basic Trap Settings", menuName = "Scriptable Object/Settings/Traps/Basic Trap Settings", order = 0)]
    public class BasicTrapSettings : TrapSettings<BasicTrapPreset>
    {
    }

    [Serializable]
    public class BasicTrapPreset : TrapPreset
    {
    }
}