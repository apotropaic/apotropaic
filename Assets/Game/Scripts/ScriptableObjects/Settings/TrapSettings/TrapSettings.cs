using Apotropaic.Utility;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Apotropaic.Data
{
    public abstract class TrapSettings<T> : GameSettings<T> where T : TrapPreset
    {
        #region PUBLIC
        public string Name { get { return CurrentPreset.Name; } }
        public string Description { get { return CurrentPreset.Description; } }
        public TrapType Type { get { return CurrentPreset.Type; } }
        public TrapDetection TrapDetection { get { return CurrentPreset.TrapDetection; } }
        public TrapLifespan Lifespan { get { return CurrentPreset.Lifespan; } }
        public int UsesCount { get { return CurrentPreset.UsesCount; } }
        public float LifespanTime { get { return CurrentPreset.LifespanTime; } }
        public List<RepulsionOdds> RepulsionOdds { get { return CurrentPreset.RepulsionOdds; } }
        public bool LockTrapOrientation { get { return CurrentPreset.LockTrapOrientation; } }

        public int Damage { get { return CurrentPreset.Damage; } }
        public float EnemyDelay { get { return CurrentPreset.EnemyDelay; } }
        public float PlacementTime { get { return CurrentPreset.PlacementTime; } }
        public int UID { get { return CurrentPreset.UID; } }

        public GameObject Prefab { get { return CurrentPreset.Prefab; } }
        public int Cost { get { return CurrentPreset.Cost; } }
        public Sprite SpriteFile { get { return CurrentPreset.SpriteFile; } }
        public Sprite JournalDetailedSprite { get { return CurrentPreset.JournalDetailedSprite; } }

        public bool Unlocked { get { return CurrentPreset.Unlocked; } set { CurrentPreset.Unlocked = value; } }
        #endregion
    }

    public abstract class TrapPreset : Preset
    {
        [SerializeField] private string trapName = "";
        [SerializeField] private int uID;
        [SerializeField] private GameObject prefab;
        [SerializeField] private Sprite sprite;
        [SerializeField] private Sprite journalDetailedSprite;
        [SerializeField] private string description;

        [NonSerialized] private bool unlocked = false;

        [Space(20)]

        [SerializeField] private TrapType trapType = TrapType.Physical;
        [SerializeField] private TrapDetection trapDetection = TrapDetection.Collision;
        [SerializeField] private TrapLifespan trapLifespan = TrapLifespan.Permanent;

        [DrawIf("trapLifespan", Operator.AND, DrawIfAttribute.DisablingType.DontDraw, TrapLifespan.MultipleUses)]
        [SerializeField] private int usesCount = 2;

        [DrawIf("trapLifespan", Operator.AND, DrawIfAttribute.DisablingType.DontDraw, TrapLifespan.Time)]
        [SerializeField] private float lifespanTime = 120f;

        [Space(20)]
        [Header("Properties")]
        [SerializeField] private int cost = 0;
        [SerializeField] [Range(0f, 20)] private int damage = 0;
        [Tooltip("Enemy will freeze for this number of seconds")]
        [SerializeField] [Range(0f, 20f)] private float enemyDelay = 0f;
        [Tooltip("Number of seconds for placing the trap")]
        [SerializeField] [Range(0f, 5f)] private float placementTime = 0f;
        [SerializeField] private List<RepulsionOdds> repulsionOdds = null;
        [Tooltip("If true, the trap won't rotate based on the player view direction.")]
        [SerializeField] private bool lockTrapOrientation = true;

        #region PUBLIC
        public string Name { get { return trapName; } }
        public string Description { get { return description; } }
        public TrapType Type { get { return trapType; } }
        public TrapDetection TrapDetection { get { return trapDetection; } }
        public TrapLifespan Lifespan { get { return trapLifespan; } }
        public int UsesCount { get { return usesCount; } }
        public float LifespanTime { get { return lifespanTime; } }

        public int Damage { get { return damage; } }
        public float EnemyDelay { get { return enemyDelay; } }
        public float PlacementTime { get { return placementTime; } }
        public List<RepulsionOdds> RepulsionOdds { get { return repulsionOdds; } }
        public bool LockTrapOrientation { get { return lockTrapOrientation; } }

        public int UID { get { return uID; } }
        public GameObject Prefab { get { return prefab; } }
        public int Cost { get { return cost; } }
        public Sprite SpriteFile { get { return sprite; } }
        public Sprite JournalDetailedSprite { get { return journalDetailedSprite; } }

        public bool Unlocked { get { return unlocked; } set { unlocked = value; } }

        #endregion
    }

    [Serializable]
    public class RepulsionOdds
    {
        [SerializeField] private EnemySettings enemySettings = null;
        [Tooltip("If set to 0, repulsion won't work. If set to 1, repulsion will 100% work.")]
        [SerializeField] [Range(0f, 1f)] private float repulsionOdd = 0f;
        [SerializeField] private bool ignoreRepulsionIfPursuingPlayer = false;

        public string EnemyName { get { return enemySettings.EnemyName; } }
        public float RepulsionOdd { get { return repulsionOdd; } }
        public bool IgnoreRepulsionIfPursuingPlayer { get { return ignoreRepulsionIfPursuingPlayer; } }
    }
}
