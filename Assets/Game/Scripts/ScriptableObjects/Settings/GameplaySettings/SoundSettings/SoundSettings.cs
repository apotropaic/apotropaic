using Apotropaic.Utility;
using System;
using UnityEngine;

namespace Apotropaic.Data
{
    [CreateAssetMenu(fileName = "Sound Settings", menuName = "Scriptable Object/Settings/Sound Settings", order = 0)]
    public class SoundSettings : GameSettings<SoundPreset>
    {
        #region PUBLIC
        public SoundType SoundType { get { return CurrentPreset.SoundType; } }
        public AudioClip AudioClip { get { return CurrentPreset.AudioClip; } }
        public bool Loop { get { return CurrentPreset.Loop; } }
        public bool RepeatEveryNotificationTime { get { return CurrentPreset.RepeatEveryNotificationTime; } }
        public float DelayBeforeFirstPlayback { get { return CurrentPreset.DelayBeforeFirstPlayback; } }
        public float Pitch { get { return CurrentPreset.Pitch; } }
        public float Volume { get { return CurrentPreset.Volume; } }

        public float Strength { get { return CurrentPreset.Strength; } }
        public float Radius { get { return CurrentPreset.Radius; } }
        public float NotificationTime { get { return CurrentPreset.NotificationTime; } }
        public bool EnableDebugGizmos { get { return CurrentPreset.EnableDebugGizmos; } }
        public Color DebugGizmosColor { get { return CurrentPreset.DebugGizmosColor; } }
        #endregion
    }

    [Serializable]
    public class SoundPreset : Preset
    {
        [SerializeField] private SoundType soundType = SoundType.PlayerSound;
        [DrawIf("soundType", Operator.AND, DrawIfAttribute.DisablingType.DontDraw, SoundType.TrapSound)]
        [SerializeField] private BasicTrapSettings trapSettings = null;
        [SerializeField] private AudioClip audioClip = null;
        [SerializeField] private bool loop = false;
        [Tooltip("Instead of looping the sound, the sound will be re-played every n seconds of the notificationTime variable (See below).")]
        [SerializeField] private bool repeatEveryNotificationTime = false;
        [SerializeField] [Range(0f, 3f)] private float pitch = 1f;
        [SerializeField] [Range(0f, 1f)] private float volume = 1f;
        [SerializeField] [Range(0f, 10f)] private float delayBeforeFirstPlayback = 0f;

        [Tooltip("The higher the strength the more accurate the enemy can detect the sound.\nIf set to 0, sound will be ignored completely.")]
        [SerializeField] [Range(0f, 1f)] private float strength = 1f;
        [Tooltip("If the distance between the enemy and the sound source is larger than the radius, the sound will be ignored (regardless of the enemy hearing strength).")]
        [SerializeField] [Range(0f, 100f)] private float radius = 10f;

        [Tooltip("If set to 0, enemies will be notified of the sound once. Else, it will notify the enemy every n seconds (Needed for looping sounds).")]
        [SerializeField] [Range(0f, 5f)] private float notificationTime = 0f;
        [SerializeField] private bool enableDebugGizmos = false;
        [SerializeField] private Color debugGizmosColor = Color.white;

        #region PUBLIC
        public SoundType SoundType { get { return soundType; } }
        public BasicTrapSettings TrapSettings { get { return trapSettings; } }
        public AudioClip AudioClip { get { return audioClip; } }
        public bool Loop { get { return loop; } }
        public bool RepeatEveryNotificationTime { get { return repeatEveryNotificationTime; } }
        public float DelayBeforeFirstPlayback { get { return delayBeforeFirstPlayback; } }
        public float Pitch { get { return pitch; } }
        public float Volume { get { return volume; } }
        public float Strength { get { return strength; } }
        public float Radius { get { return radius; } }
        public float NotificationTime { get { return notificationTime; } }
        public bool EnableDebugGizmos { get { return enableDebugGizmos; } }
        public Color DebugGizmosColor { get { return debugGizmosColor; } }
        #endregion
    }
}