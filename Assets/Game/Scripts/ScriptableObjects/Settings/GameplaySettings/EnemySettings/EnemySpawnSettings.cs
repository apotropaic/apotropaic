using System;
using System.Collections.Generic;
using UnityEngine;

namespace Apotropaic.Data
{
    [CreateAssetMenu(fileName = "Enemy Spawn Settings", menuName = "Scriptable Object/Settings/Enemies/Enemy Spawn Settings", order = 0)]
    public class EnemySpawnSettings : ScriptableObject
    {
        [SerializeField] private List<NightSpawnSettings> nightSpawnSettings = null;

        #region PUBLIC
        public List<NightSpawnSettings> NightSpawnSettings { get { return nightSpawnSettings; } }
        #endregion
    }

    [Serializable]
    public class NightSpawnSettings
    {
        [Tooltip("Point of time in the night phase at which the enemy will spawn.\n E.g. 0 = the enemy will spawn the instant the night starts.\n 0.5 = the enemy will spawn exactly in the middle of the night.")]
        [SerializeField] [Range(0f, 1f)] private float spawnTime = 0f;
        [Tooltip("Number of enemies to spawn at the specified spawn time.")]
        [SerializeField] [Range(1, 10)] private int spawnQuantity = 1;

        public float SpawnTime { get { return spawnTime; } }
        public int SpawnQuantity { get { return spawnQuantity; } }
    }
}