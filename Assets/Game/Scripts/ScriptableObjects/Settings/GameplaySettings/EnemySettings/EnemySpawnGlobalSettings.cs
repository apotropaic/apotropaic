using UnityEngine;
using System;
using Apotropaic.Utility;

namespace Apotropaic.Data
{
    [CreateAssetMenu(fileName = "Enemy Spawn Global Settings", menuName = "Scriptable Object/Settings/Enemies/Enemy Spawn Global Settings", order = 0)]
    public class EnemySpawnGlobalSettings : GameSettings<EnemySpawnGlobalPreset>
    {
        #region PUBLIC
        public float MinSpawnPointDist { get { return CurrentPreset.MinSpawnPointDist; } }
        public int? MaxEnemiesCount { get { return CurrentPreset.MaxEnemiesCount; } }
        public bool EnableSpawnPointGizmos { get { return CurrentPreset.EnableSpawnPointGizmos; } }
        public Color GizmosColor { get { return CurrentPreset.GizmosColor; } }
        #endregion
    }

    [Serializable]
    public class EnemySpawnGlobalPreset : Preset
    {
        [Tooltip("If the distance between a spawn point and the player is more than this value, an enemy will spawn.")]
        [SerializeField] [Range(0f, 50f)] private float minSpawnPointDist = 10f;
        [Tooltip("If limit enemies count is enabled, it will override the spawn settings of the enemies.")]
        [SerializeField] private bool limitEnemiesCount = false;
        [Tooltip("The max number of enemies active at any given time.")]
        [DrawIf("limitEnemiesCount", Operator.AND, DrawIfAttribute.DisablingType.DontDraw, true)] [SerializeField] private int maxEnemiesCount = 1;
        [SerializeField] private bool enableSpawnPointGizmos = false;
        [SerializeField] private Color gizmosColor = Color.white;

        #region PUBLIC
        public float MinSpawnPointDist { get { return minSpawnPointDist; } }
        public int? MaxEnemiesCount
        {
            get
            {
                if (limitEnemiesCount)
                    return maxEnemiesCount;
                else
                    return null;
            }
        }

        public bool EnableSpawnPointGizmos { get { return enableSpawnPointGizmos; } }
        public Color GizmosColor { get { return gizmosColor; } }
        #endregion
    }
}