using UnityEngine;
using System;
using System.Collections.Generic;

namespace Apotropaic.Data
{
    [CreateAssetMenu(fileName = "Enemy Settings", menuName = "Scriptable Object/Settings/Enemies/Enemy Settings", order = 0)]
    public class EnemySettings : GameSettings<EnemyPreset>
    {
        #region PUBLIC
        public string EnemyName { get { return CurrentPreset.EnemyName; } }
        public Sprite EnemySprite { get { return CurrentPreset.EnemySprite; } }
        public string EnemyDescription { get { return CurrentPreset.EnemyDescription; } }
        public int MaxHealth { get { return CurrentPreset.MaxHealth; } }
        public float MovementSpeed { get { return CurrentPreset.MovementSpeed; } }
        public float TurnSlowdownMultiplier { get { return CurrentPreset.TurnSlowdownMultiplier; } }
        public float PursueSmellSpeedMultiplier { get { return CurrentPreset.PursueSmellSpeedMultiplier; } }
        public float PursuePlayerSpeedMultiplier { get { return CurrentPreset.PursuePlayerSpeedMultiplier; } }
        public float PostAttackSpeedMultiplier { get { return CurrentPreset.PostAttackSpeedMultiplier; } }
        public List<EnemySpawnSettings> EnemySpawnSettings { get { return CurrentPreset.EnemySpawnSettings; } }
        public float SmellMultiplier { get { return CurrentPreset.SmellMultiplier; } }
        public int AttackDamage { get { return CurrentPreset.AttackDamage; } }
        public float MaxPursueDistance { get { return CurrentPreset.MaxPursueDistance; } }
        public bool EnableMaxPursueDistanceGizmo { get { return CurrentPreset.EnableMaxPursueDistanceGizmo; } }
        public Color MaxPursueDistanceGizmoColor { get { return CurrentPreset.MaxPursueDistanceGizmoColor; } }
        public int CurrencyDrop { get { return CurrentPreset.CurrencyDrop; } }
        public float HearingStrength { get { return CurrentPreset.HearingStrength; } }
        public float MaxSoundTimeToPursuePlayer { get { return CurrentPreset.MaxSoundTimeToPursuePlayer; } }
        public float PursueSoundSpeedMultiplier { get { return CurrentPreset.PursueSoundSpeedMultiplier; } }
        public float MinSoundPlayerPursueDistance { get { return CurrentPreset.MinSoundPlayerPursueDistance; } }
        public bool EnableMinSoundPlayerPursueDistanceGizmo { get { return CurrentPreset.EnableMinSoundPlayerPursueDistanceGizmo; } }
        public Color MinSoundPlayerPursueDistanceGizmoColor { get { return CurrentPreset.MinSoundPlayerPursueDistanceGizmoColor; } }
        public float RepulsionPursueCooldown { get { return CurrentPreset.RepulsionPursueCooldown; } }
        public bool StayInPatrolArea { get { return CurrentPreset.StayInPatrolArea; } }
        public bool EnableFootstepsParticles { get { return CurrentPreset.EnableFootstepsParticles; } }
        public bool EnableSniffingParticles { get { return CurrentPreset.EnableSniffingParticles; } }
        public bool EnableTransparency { get { return CurrentPreset.EnableTransparency; } }
        public bool EnableDebugGizmos { get { return CurrentPreset.EnableDebugGizmos; } }
        public Color GizmosColor { get { return CurrentPreset.GizmosColor; } }
        #endregion
    }

    [Serializable]
    public class EnemyPreset : Preset
    {
        [SerializeField] private string enemyName = "";
        [SerializeField] private Sprite enemySprite = null;
        [SerializeField] [TextArea] private string enemyDescription = "";
        [Space(20)]

        [Header("Enemy speed")]
        [SerializeField] [Range(0f, 10f)] private float movementSpeed = 5.5f;
        [Tooltip("If set to 0, the enemy won't slowdown in turns.")]
        [SerializeField] [Range(0f, 10f)] private float turnSlowdownMultiplier = 4f;
        [Tooltip("This value is multiplied by the base movement speed.\nThis is the speed of the enemy when targeting the player.")]
        [SerializeField] [Range(1f, 5f)] private float pursuePlayerSpeedMultiplier = 1.35f;
        [Tooltip("This value is multiplied by the base movement speed.\nThis is the speed of the enemy after colliding with the player. \n\nMaking this value lower allows the player to escape, the value then reset when the enemy is no longer pursuing the player.")]
        [SerializeField] [Range(0f, 1f)] private float postAttackSpeedMultiplier = 0.65f;

        [Space(20)]
        [SerializeField] [Range(1, 100)] private int maxHealth = 10;
        [SerializeField] [Range(0, 20)] private int attackDamage = 1;
        [Tooltip("During player pursue: If the distance between the player and the enemy is more than the max pursue distance, the enemy will stop pursuing the player.")]
        [SerializeField] [Range(0f, 20f)] private float maxPursueDistance = 6f;
        [SerializeField] private bool enableMaxPursueDistanceGizmo = false;
        [SerializeField] private Color maxPursueDistanceGizmoColor = Color.white;

        [SerializeField] [Range(0, 100)] private int currencyDrop = 0;
        [SerializeField] private List<EnemySpawnSettings> enemySpawnSettings = null;
        [Space(20)]

        [Header("Smell detection")]
        [Tooltip("This value is multiplied by the current smell strength of a smell node.\nThe value of the SMELL NODE (not this smell multiplier) strength is a percentage (0 to 100%).")]
        [SerializeField] [Range(0f, 10f)] private float smellMultiplier = 0f;
        [Tooltip("This value is multiplied by the base movement speed.\nThis is the speed of the enemy pursuing smell nodes.")]
        [SerializeField] [Range(1f, 5f)] private float pursueSmellSpeedMultiplier = 1f;
        [Space(20)]

        [Header("Sound detection")]
        [Tooltip("If hearingStrength is 1, the enemy accuracy to find the player location is 100% (If the enemy is in range of the sound) regardless how far the enemy is from the sound source.\nIf it is set to 0, the sound will be ignored.\nIt is recommended to set this to a very low value to enable the sound detection. (e.g. 0.05)")]
        [SerializeField] [Range(0f, 1f)] private float hearingStrength = 0f;
        [Tooltip("NOTE: 0 = unlimited time.\nIf the time between hearing the sound to reaching the target destination is over this number, it won't pursue the player even if the player is close to the sound source.")]
        [SerializeField] [Range(0f, 50f)] private float maxSoundTimeToPursuePlayer = 0f;
        [Tooltip("This value is multiplied by the base movement speed.\nThis is the speed of the enemy when pursuing sound targets.")]
        [SerializeField] [Range(1f, 5f)] private float pursueSoundSpeedMultiplier = 1f;
        [Tooltip("After the enemy reaches the current sound target destination: If the distance between the player and the enemy is less than the min pursue distance, the enemy will pursue the player.")]
        [SerializeField] [Range(0f, 20f)] private float minSoundPlayerPursueDistance = 6f;
        [SerializeField] private bool enableMinSoundPlayerPursueDistanceGizmo = false;
        [SerializeField] private Color minSoundPlayerPursueDistanceGizmoColor = Color.white;
        [Space(20)]

        [Tooltip("Seconds to resume pursuing smell/sound/player after being repelled by the trap.")]
        [SerializeField] [Range(0f, 10f)] private float repulsionPursueCooldown = 0f;
        [Tooltip("If set to true, the enemy won't pursue anything outside the patrol area.")]
        [SerializeField] private bool stayInPatrolArea = false;
        [Space(20)]

        [Header("Particle Effects")]
        [SerializeField] private bool enableFootstepsParticles = false;
        [SerializeField] private bool enableSniffingParticles = false;

        [Header("Debugging")]
        [Tooltip("If enabled, the enemy is only visible when the player vision cone is reaching the enemy. Else, the enemy will be visible at all times.")]
        [SerializeField] private bool enableTransparency = true;
        [SerializeField] private bool enableDebugGizmos = false;
        [SerializeField] private Color gizmosColor = Color.white;

        #region PUBLIC
        public string EnemyName { get { return enemyName; } }
        public Sprite EnemySprite { get { return enemySprite; } }
        public string EnemyDescription { get { return enemyDescription; } }
        public int MaxHealth { get { return maxHealth; } }
        public float MovementSpeed { get { return movementSpeed; } }
        public float TurnSlowdownMultiplier { get { return turnSlowdownMultiplier; } }
        public float PursueSmellSpeedMultiplier { get { return pursueSmellSpeedMultiplier; } }
        public float PursuePlayerSpeedMultiplier { get { return pursuePlayerSpeedMultiplier; } }
        public float PostAttackSpeedMultiplier { get { return postAttackSpeedMultiplier; } }
        public List<EnemySpawnSettings> EnemySpawnSettings { get { return enemySpawnSettings; } }
        public float SmellMultiplier { get { return smellMultiplier; } }
        public int AttackDamage { get { return attackDamage; } }
        public float MaxPursueDistance { get { return maxPursueDistance; } }
        public bool EnableMaxPursueDistanceGizmo { get { return enableMaxPursueDistanceGizmo; } }
        public Color MaxPursueDistanceGizmoColor { get { return maxPursueDistanceGizmoColor; } }
        public int CurrencyDrop { get { return currencyDrop; } }
        public float HearingStrength { get { return hearingStrength; } }
        public float MaxSoundTimeToPursuePlayer { get { return maxSoundTimeToPursuePlayer; } }
        public float PursueSoundSpeedMultiplier { get { return pursueSoundSpeedMultiplier; } }
        public float MinSoundPlayerPursueDistance { get { return minSoundPlayerPursueDistance; } }
        public bool EnableMinSoundPlayerPursueDistanceGizmo { get { return enableMinSoundPlayerPursueDistanceGizmo; } }
        public Color MinSoundPlayerPursueDistanceGizmoColor { get { return minSoundPlayerPursueDistanceGizmoColor; } }
        public float RepulsionPursueCooldown { get { return repulsionPursueCooldown; } }
        public bool StayInPatrolArea { get { return stayInPatrolArea; } }
        public bool EnableFootstepsParticles { get { return enableFootstepsParticles; } }
        public bool EnableSniffingParticles { get { return enableSniffingParticles; } }
        public bool EnableTransparency { get { return enableTransparency; } }
        public bool EnableDebugGizmos { get { return enableDebugGizmos; } }
        public Color GizmosColor { get { return gizmosColor; } }
        #endregion
    }
}