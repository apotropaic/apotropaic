using System;
using UnityEngine;

namespace Apotropaic.Data
{
    [CreateAssetMenu(fileName = "Player Settings", menuName = "Scriptable Object/Settings/Player Settings", order = 0)]
    public class PlayerSettings : GameSettings<PlayerPreset>
    {
        #region PUBLIC
        public float WalkingSpeed { get { return CurrentPreset.WalkingSpeed; } }
        public float TiptoeSpeed { get { return CurrentPreset.TiptoeSpeed; } }
        public float RotationSpeed { get { return CurrentPreset.RotationSpeed; } }
        public int MaxHealth { get { return CurrentPreset.MaxPlayerHealth; } }
        public float TakenDamageCooldown { get { return CurrentPreset.TakenDamageCooldown; } }
        public bool EnableFootstepsParticles { get { return CurrentPreset.EnableFootstepsParticles; } }
        public bool EnableSmellParticles { get { return CurrentPreset.EnableSmellParticles; } }

        public int[] TrapsStock { get { return CurrentPreset.TrapsStock; } }
        #endregion
    }

    [Serializable]
    public class PlayerPreset : Preset
    {
        [SerializeField] [Range(0f, 10f)] private float walkingSpeed = 3f;
        [SerializeField] [Range(0f, 10f)] private float tiptoeSpeed = 1f;
        [SerializeField] [Range(0f, 20f)] private float rotationSpeed = 10f;
        [SerializeField] [Range(1, 100)] private int maxPlayerHealth = 10;
        [Tooltip("The cooldown in seconds for the player to register damage.")]
        [SerializeField] [Range(0f, 10f)] private float takenDamageCooldown = 0f;
        [SerializeField] private bool enableFootstepsParticles = true;
        [SerializeField] private bool enableSmellParticles = true;
        // TODO : For testing
        [SerializeField] private int[] trapsStock = null;

        #region PUBLIC
        public float WalkingSpeed { get { return walkingSpeed; } }
        public float TiptoeSpeed { get { return tiptoeSpeed; } }
        public float RotationSpeed { get { return rotationSpeed; } }
        public int MaxPlayerHealth { get { return maxPlayerHealth; } }
        public float TakenDamageCooldown { get { return takenDamageCooldown; } }
        public bool EnableFootstepsParticles { get { return enableFootstepsParticles; } }
        public bool EnableSmellParticles { get { return enableSmellParticles; } }

        public int[] TrapsStock { get { return trapsStock; } }
        #endregion
    }
}