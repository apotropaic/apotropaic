using System;
using UnityEngine;

namespace Apotropaic.Data
{
    [CreateAssetMenu(fileName = "Sound System Settings", menuName = "Scriptable Object/Settings/Sound System Settings", order = 0)]
    public class SoundSystemSettings : GameSettings<SoundSystemPreset>
    {
        #region PUBLIC
        #endregion
    }

    [Serializable]
    public class SoundSystemPreset : Preset
    {
        #region PUBLIC
        #endregion
    }
}