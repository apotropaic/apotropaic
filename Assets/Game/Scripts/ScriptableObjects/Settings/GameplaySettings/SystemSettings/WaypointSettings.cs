using System;
using UnityEngine;

namespace Apotropaic.Data
{
    [CreateAssetMenu(fileName = "Waypoint Settings", menuName = "Scriptable Object/Settings/Waypoint Settings", order = 0)]
    public class WaypointSettings : GameSettings<WaypointPreset>
    {
        #region PUBLIC
        public bool EnableGizmos { get { return CurrentPreset.EnableGizmos; } }
        public Color GizmosColor { get { return CurrentPreset.GizmosColor; } }
        #endregion
    }

    [Serializable]
    public class WaypointPreset : Preset
    {
        [SerializeField] private bool enableGizmos = false;
        [SerializeField] private Color gizmosColor = Color.white;

        #region PUBLIC
        public bool EnableGizmos { get { return enableGizmos; } }
        public Color GizmosColor { get { return gizmosColor; } }
        #endregion
    }
}