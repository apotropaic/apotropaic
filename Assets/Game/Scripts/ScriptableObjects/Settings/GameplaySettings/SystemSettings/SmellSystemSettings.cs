using System;
using UnityEngine;

namespace Apotropaic.Data
{
    [CreateAssetMenu(fileName = "Smell System Settings", menuName = "Scriptable Object/Settings/Smell System Settings", order = 0)]
    public class SmellSystemSettings : GameSettings<SmellSystemPreset>
    {
        #region PUBLIC
        public float NodesLifespan { get { return CurrentPreset.NodesLifespan; } }
        public float NodeDistance { get { return CurrentPreset.NodeDistance; } }
        public float RegularNodeScale { get { return CurrentPreset.RegularNodeScale; } }
        public float PermenantNodeScale { get { return CurrentPreset.PermenantNodeScale; } }
        public bool EnableSmellNodeGizmos { get { return CurrentPreset.EnableSmellNodeGizmos; } }
        public SmellSystem.PoolingMode PoolingMode { get { return CurrentPreset.PoolingMode; } }
        #endregion
    }

    [Serializable]
    public class SmellSystemPreset : Preset
    {
        [Tooltip("Seconds for the smell node to expire")]
        [SerializeField] private float nodesLifespan = 8f;
        [Tooltip("If the distance between the player & the previous smell node is greater than this number, a new smell node will be spawned.")]
        [SerializeField] private float nodeDistance = 4f;
        [Tooltip("Scale of the smell nodes dropped by the player")]
        [SerializeField] private float regularNodeScale = 1f;
        [Tooltip("Scale of the smell node attached to the player")]
        [SerializeField] private float permenantNodeScale = 1f;
        [SerializeField] private bool enableSmellNodeGizmos = false;
        [SerializeField] private SmellSystem.PoolingMode poolingMode = SmellSystem.PoolingMode.Recycle;

        #region PUBLIC
        public float NodesLifespan { get { return nodesLifespan; } }
        public float NodeDistance { get { return nodeDistance; } }
        public float RegularNodeScale { get { return regularNodeScale; } }
        public float PermenantNodeScale { get { return permenantNodeScale; } }
        public bool EnableSmellNodeGizmos { get { return enableSmellNodeGizmos; } }
        public SmellSystem.PoolingMode PoolingMode { get { return poolingMode; } }
        #endregion
    }
}