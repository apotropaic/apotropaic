using System;
using UnityEngine;

namespace Apotropaic.Data
{
    [CreateAssetMenu(fileName = "Gameplay Settings", menuName = "Scriptable Object/Settings/Gameplay Settings", order = 0)]
    public class GameplaySettings : GameSettings<GameplayPreset>
    {
        #region PUBLIC
        public float EveningLength { get { return CurrentPreset.EveningLength; } }
        public float NightLength { get { return CurrentPreset.NightLength; } }
        public float NightInitialProgress { get { return CurrentPreset.NightInitialProgress; } }
        public int ClearedNightReward { get { return CurrentPreset.ClearedNightReward; } }
        #endregion
    }

    [Serializable]
    public class GameplayPreset : Preset
    {
        [SerializeField] private float eveningLength = 0f;
        [SerializeField] private float nightLength = 0f;
        [Tooltip("If set to 0.5f, the gameplay will start in the middle of the night phase")]
        [SerializeField] [Range(0f, 1f)] private float nightInitialProgress = 0f;
        [SerializeField] private int clearedNightReward = 0;

        public float EveningLength { get { return eveningLength; } }
        public float NightLength { get { return nightLength; } }
        public float NightInitialProgress { get { return nightInitialProgress; } }
        public int ClearedNightReward { get { return clearedNightReward; } }
    }
}
