using System;
using UnityEngine;

namespace Apotropaic.Data
{
    [CreateAssetMenu(fileName = "Scene Settings", menuName = "Scriptable Object/Settings/Scene Settings", order = 0)]
    public class SceneSettings : GameSettings<ScenePreset>
    {
        #region PUBLIC
        public SceneReference StartupScene { get { return CurrentPreset.StartupScene; } }
        public SceneReference MainMenuScene { get { return CurrentPreset.MainMenuScene; } }
        public SceneReference UIScene { get { return CurrentPreset.UIScene; } }
        public SceneReference OfficesScene { get { return CurrentPreset.OfficesScene; } }
        public SceneReference LibraryScene { get { return CurrentPreset.LibraryScene; } }
        #endregion
    }

    [Serializable]
    public class ScenePreset : Preset
    {
        public enum StartupScenes
        {
            MainMenu = 0,
            Offices = 1,
            Library = 2
        }

        [SerializeField] private StartupScenes startupScene = StartupScenes.MainMenu;
        [SerializeField] private SceneReference mainMenuScene = null;
        [SerializeField] private SceneReference uiScene = null;
        [SerializeField] private SceneReference officesScene = null;
        [SerializeField] private SceneReference libraryScene = null;

        public SceneReference StartupScene
        {
            get
            {
                switch (startupScene)
                {
                    case StartupScenes.MainMenu:
                        return mainMenuScene;
                    case StartupScenes.Offices:
                        return officesScene;
                    case StartupScenes.Library:
                        return libraryScene;
                    default:
                        return mainMenuScene;
                }
            }
        }

        public SceneReference MainMenuScene { get { return mainMenuScene; } }
        public SceneReference UIScene { get { return uiScene; } }
        public SceneReference OfficesScene { get { return officesScene; } }
        public SceneReference LibraryScene { get { return libraryScene; } }
    }
}