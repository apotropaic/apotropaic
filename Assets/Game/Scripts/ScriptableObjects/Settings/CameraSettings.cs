using System;
using UnityEngine;
 
namespace Apotropaic.Data
{
    [CreateAssetMenu(fileName = "Camera Settings", menuName = "Scriptable Object/Settings/Camera Settings", order = 0)]
    public class CameraSettings : GameSettings<CameraPreset>
    {
        public float Height { get { return CurrentPreset.Height; } }
        public float Angle { get { return CurrentPreset.Angle; } }
        public float Distance { get { return CurrentPreset.Distance; } }
        public float SnapSpeed { get { return CurrentPreset.SnapSpeed; } }
        public float Size { get { return CurrentPreset.Size; } }
    }


    [Serializable]
    public class CameraPreset : Preset
    {
        [SerializeField] private float height;
        [SerializeField] private float angle;
        [SerializeField] private float distance;
        [SerializeField] private float snapSpeed;
        [SerializeField] private float size;

        public float Height { get { return height; } }
        public float Angle { get { return angle; } }
        public float Distance { get { return distance; } }
        public float SnapSpeed { get { return snapSpeed; } }
        public float Size { get { return size; } }
    }
}
