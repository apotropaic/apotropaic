using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MenuClassifier", menuName = "Scriptable Object/Menu Classifier")]
public class MenuClassifier : ScriptableObject
{
    [SerializeField] private string menuName;

    public string MenuName { get { return menuName; } }
}
