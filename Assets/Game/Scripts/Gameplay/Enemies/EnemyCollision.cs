using System;
using UnityEngine;

public class EnemyCollision : MonoBehaviour
{
    private const string trapTag = "Trap";
    private const string smellTag = "Smell";
    private const string playerTag = "Player";

    public Action<BasicTrap> OnTrapDetected = null;
    public Action<SmellNode> OnSmellDetected = null;
    public Action OnPlayerDetected = null;

    #region UNITY
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(trapTag))
        {
            BasicTrap trap = other.GetComponent<BasicTrap>();

            if (OnTrapDetected != null)
            {
                OnTrapDetected(trap);
            }
        }
        else if (other.CompareTag(smellTag))
        {
            SmellNode smellNode = other.GetComponent<SmellNode>();

            if (OnSmellDetected != null)
            {
                OnSmellDetected(smellNode);
            }
        }
        else if (other.CompareTag(playerTag))
        {
            if (OnPlayerDetected != null)
            {
                OnPlayerDetected();
            }
        }
    }
    #endregion
}