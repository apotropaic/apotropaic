using UnityEngine;

public class CurrencyDropper : MonoBehaviour
{
    [SerializeField] private GameObject currencyPrefab = null;
    private GameObject currencyDrop = null;

    #region UNITY
    #endregion

    #region PUBLIC
    public void Initialize(int value)
    {
        currencyDrop = Instantiate(currencyPrefab);
        currencyDrop.GetComponent<CurrencyDrop>().CurrencyValue = value;
        currencyDrop.SetActive(false);
    }

    public void DropCurrency()
    {
        currencyDrop.transform.position = transform.position;
        currencyDrop.SetActive(true);
    }
    #endregion

    #region PRIVATE
    #endregion
}