using UnityEngine;

public class EnemyHealthBar : MonoBehaviour
{
    private float maxHP;
    private float currentHP;
    private Vector3 startingScale;
    #region UNITY
    private void Start()
    { 
    }

    private void Update()
    {
        if(gameObject.activeInHierarchy)
        {
            transform.localScale = new Vector3(startingScale.x * (currentHP / maxHP), transform.localScale.y, transform.localScale.z);
        }
    }
    #endregion

    #region PUBLIC
    public void SetCurrentHP(int _currentHP)
    {
        currentHP = (float)_currentHP;
    }

    public void SetMaxHP(int _maxHP)
    {
        maxHP = (float)_maxHP;
        currentHP = (float)_maxHP;
        startingScale = transform.localScale;
    }
    #endregion

    #region PRIVATE
    #endregion
}
