using UnityEngine;

public class PatrolArea : MonoBehaviour
{
    [SerializeField] private Waypoint startingWaypoint = null;
    private Transform[] waypoints = null;
    private Vector3 areaCenter = Vector3.zero;
    private float radius = 0f;

    #region UNITY
    private void Start()
    {
        waypoints = GetComponentsInChildren<Transform>();

        Vector3 summation = Vector3.zero;

        for (int i = 0; i < waypoints.Length; i++)
        {
            summation += waypoints[i].position;
        }

        areaCenter = summation / waypoints.Length;

        float sqrDistanceFromFurthestPoint = Vector3.SqrMagnitude(areaCenter - waypoints[0].transform.position);

        for (int i = 0; i < waypoints.Length; i++)
        {
            float sqrDistance = Vector3.SqrMagnitude(areaCenter - waypoints[i].transform.position);
            if (sqrDistance > sqrDistanceFromFurthestPoint)
            {
                sqrDistanceFromFurthestPoint = sqrDistance;
            }
        }

        radius = Mathf.Sqrt(sqrDistanceFromFurthestPoint);
    }
    #endregion

    #region PUBLIC
    public Waypoint FirstWaypoint { get { return startingWaypoint; } }
    public Vector3 AreaCenter { get { return areaCenter; } }
    public float AreaRadius { get { return radius; } }
    #endregion

    #region PRIVATE
    #endregion
}