using Apotropaic.Data;
using Apotropaic.Utility;
using System.Collections;
using UnityEngine;

public class EnemyTransparencyController : MonoBehaviour
{
    [SerializeField] private MeshRenderer mesh;
    [SerializeField] private MeshRenderer damageBoard;
    [SerializeField] private float FadeOutCooldown;

    bool FadeInFlag = false;
    private float resetTimer;
    [SerializeField]private float fadeSpeed;

    #region UNITY
    private void Start()
    {
        Color matColor = mesh.material.color;
        matColor.a = 0f;
        mesh.material.color = matColor;
        damageBoard.material.color = matColor;
    }

    private void Update()
    {
        resetTimer += Time.deltaTime;
        AdjustTransparency();
    }
    #endregion

    #region PUBLIC
    public void FadeIn()
    {
        FadeInFlag = true;
        resetTimer = 0.0f;
    }
    #endregion

    #region PRIVATE
    private void AdjustTransparency()
    {
        if(FadeInFlag)
        {
            if(mesh.material.color.a == 0)
            {
                StandardShaderUtils.ChangeRenderMode(mesh.material, StandardShaderUtils.BlendMode.Fade);
                StandardShaderUtils.ChangeRenderMode(damageBoard.material, StandardShaderUtils.BlendMode.Fade);
            }

            if (mesh.material.color.a != 1)
            {
                ApplyTransparency(1.0f * fadeSpeed);
            }
            else
            {
                StandardShaderUtils.ChangeRenderMode(mesh.material, StandardShaderUtils.BlendMode.Cutout);
                StandardShaderUtils.ChangeRenderMode(damageBoard.material, StandardShaderUtils.BlendMode.Cutout);
                FadeInFlag = false;
            }
        }
        //else
        if(resetTimer > FadeOutCooldown)
        {
            StandardShaderUtils.ChangeRenderMode(mesh.material, StandardShaderUtils.BlendMode.Fade);
            StandardShaderUtils.ChangeRenderMode(damageBoard.material, StandardShaderUtils.BlendMode.Fade);
            ApplyTransparency(-1.0f * fadeSpeed);

        }
    }

    private void ApplyTransparency(float sign)
    {
        Color currentColor = mesh.material.color;
        currentColor.a = Mathf.Clamp(currentColor.a + sign, (sign > 0 ? 1 : 0), 1f);
        mesh.material.color = currentColor;
        damageBoard.material.color = currentColor;
    }
    #endregion
}
