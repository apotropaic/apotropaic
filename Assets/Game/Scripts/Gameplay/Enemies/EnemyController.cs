using System;
using UnityEngine;
using UnityEngine.AI;
using Apotropaic.Data;
using System.Collections.Generic;
using BehaviorDesigner.Runtime;
using Apotropaic.Events;

public class EnemyController : MonoBehaviour
{
    public Action<PatrolArea, EnemyController> OnClearedPatrolArea;
    public Action<EnemyController> OnDestroyed = null;

    public delegate void SoundSystemDelegate(Vector3 position, float strength, float radius, SoundType soundType, BasicTrap basicTrap);
    public SoundSystemDelegate OnSoundTriggered = null;

    [SerializeField] private EnemySettings settings = null;

    private int maxHealth;
    private float turnSlowdownMultiplier = 0f;
    private float pursueSmellSpeedMultiplier;
    private float pursuePlayerSpeedMultiplier;
    private float postAttackSpeedMultiplier;
    private float smellMultiplier;
    private int attackDamage;
    private float maxPursueDistance;
    private bool enableMaxPursueDistanceGizmo;
    private Color maxPursueDistanceGizmoColor;
    private float minSoundPlayerPursueDistance;
    private bool enableMinSoundPlayerPursueDistanceGizmo;
    private Color minSoundPlayerPursueDistanceGizmoColor;
    private float repulsionPursueCooldown = 0f;
    private bool stayInPatrolArea = false;
    private bool enableFootstepsParticles = false;
    private bool enableSniffingParticles = false;
    private int currencyDrop;
    private bool debugGizmos;
    private Color gizmosColor;

    [Space(20)]
    [SerializeField] private bool log = false;
    [SerializeField] private BehaviorTree behaviorTree = null;
    [SerializeField] private NavMeshAgent agent = null;
    [SerializeField] private EnemyCollision enemyCollision = null;
    [SerializeField] private EnemyAwarenessCollision awarenessCollision = null;
    [SerializeField] private EnemyHealthBar healthBar;
    [SerializeField] private AudioSource sfxAudioSource = null;
    [SerializeField] private EnemyTransparencyController transparencyController = null;
    [SerializeField] private Rigidbody myRigidbody = null;
    [SerializeField] private PlayerData playerData = null;
    [SerializeField] private CurrencyDropper currencyDropper = null;
    [SerializeField] private EnemyAnimator enemyAnimator = null;
    [SerializeField] private Transform scaleRoot = null;
    [SerializeField] private ParticleSystem footstepsParticles = null;
    [SerializeField] private ParticleSystem sniffParticles = null;
    [SerializeField] private Transform rotationRoot = null;

    private IPlayerDamageDealer playerDamageDealer = null;
    private int currentHealth = 0;
    private Waypoint targetWaypoint = null;
    private SmellNode targetSmellNode = null;
    private Vector3? targetSoundPosition = null;
    private PatrolArea currentPatrolArea = null;
    private Vector3 currentDestination = Vector3.zero;
    private int pursueNodesDirection = 1;

    private float cooldownToPursue = 0f;
    private float currentAgentMovementSpeed = 0f;
    // Rotation
    private float agentAngularVelocity = 0f;
    private Vector3 previousFacingDirection = Vector3.zero;
    // Animation
    private float walkAnimationCooldownRemaining = 0f;
    private const float walkAnimationCooldown = 1f;
    private EnemyAnimation currentAnimation = EnemyAnimation.Idle;

    private float idleAnimationCooldownTime = 0f;
    private const float idleAnimationCooldown = 0.3f;

    private float allAnimationsCooldownTime = 0f;
    private const float allAnimationsCooldown = 0.3f;

    // Monitor enemy movement
    // Position previous frame
    private Vector3 cachedPosition = Vector3.zero;
    // If the distance between current position and previous position is more than this value, the enemy is not stuck
    private const float minStuckDist = 0.01f;
    // The time passed since the enemy was stuck
    private float timePassedSinceStuck = 0f;
    // The time to declare the enemy stuck
    private const float enemyStuckTime = 0.3f;
    // Cooldown until monitoring the enemy position is resumed
    private const float stuckMonitorCooldown = 0.2f;
    // Cooldown left
    private float stuckMonitorCooldownTimeLeft = 0f;

    private enum EnemyAnimation
    {
        Front = 0,
        Right = 1,
        Left = 2,
        Back = 3,
        Idle = 4,
        Attack = 5
    }

    #region UNITY
    private void Awake()
    {
        LoadSettings();
        SubscribeToEvents();
    }

    private void OnEnable()
    {
        cachedPosition = transform.position;
        SetupHPBar();
    }

    private void Start()
    {
        ResetEnemy();
    }

    private void OnDestroy()
    {
        UnsubscribeFromEvents();
    }

    private void Update()
    {
        if (currentHealth == 0) return;

        ProcessStayingInPatrolArea();
        ProcessPursueCooldown();
        MonitorEnemyPosition();
        PlayFootstepsParticles();
        UpdateAnimator();
        UpdateRotationRoot();
    }

    private void FixedUpdate()
    {
        if (currentHealth == 0) return;

        MonitorAngularVelocity();
        ApplyAgentSpeed();
        MoveEnemy();
    }

    private void OnDrawGizmos()
    {
        if (debugGizmos)
        {
            Gizmos.color = gizmosColor;
            Gizmos.DrawWireSphere(transform.position, 2f);

            if (targetSoundPosition != null)
            {
                Gizmos.color = gizmosColor;
                Gizmos.DrawWireSphere(targetSoundPosition.Value, 2f);
            }

            Gizmos.color = gizmosColor;
            Gizmos.DrawLine(transform.position, agent.destination);

            if (enableMaxPursueDistanceGizmo)
            {
                Gizmos.color = maxPursueDistanceGizmoColor;
                Gizmos.DrawWireSphere(transform.position, maxPursueDistance);
            }

            if (enableMinSoundPlayerPursueDistanceGizmo)
            {
                Gizmos.color = minSoundPlayerPursueDistanceGizmoColor;
                Gizmos.DrawWireSphere(transform.position, minSoundPlayerPursueDistance);
            }

            if (stayInPatrolArea)
            {
                Gizmos.color = gizmosColor;
                Gizmos.DrawWireSphere(currentPatrolArea.AreaCenter, currentPatrolArea.AreaRadius);
            }

            Gizmos.color = Color.white;
        }
    }
    #endregion

    #region PUBLIC
    // TODO : Post-refactoring cleanup. Get rid of unnecessary properties, move misplaced properties.
    public float MaxPursueDistance { get { return maxPursueDistance; } }
    public float SmellMultiplier { get { return smellMultiplier; } }
    public float PursueSmellSpeedMultiplier { get { return pursueSmellSpeedMultiplier; } }
    public Waypoint TargetWaypoint { get { return targetWaypoint; } set { targetWaypoint = value; } }
    // If true, the enemy pursues the next nodes. If false, the enemy pursues the previous nodes.
    public int PursueNodesDirection { get { return pursueNodesDirection; } set { pursueNodesDirection = value; } }
    public SmellNode TargetSmellNode { get { return targetSmellNode; } set { targetSmellNode = value; } }
    public Vector3? TargetSoundPosition { get { return targetSoundPosition; } set { targetSoundPosition = value; } }
    public PatrolArea CurrentPatrolArea { get { return currentPatrolArea; } }
    public bool SmellDetected { get; set; }
    public SmellNode DetectedSmellNode { get; set; }
    public BasicTrap DetectedBasicTrap { get; set; }
    public bool PursuingPlayer { get; set; }
    public bool PursuingTrapSound { get; set; }
    public bool Frozen { get; set; }
    public Vector3 CurrentDestination { get { return currentDestination; } set { currentDestination = value; } }
    public EnemySettings Settings { get { return settings; } }
    public float CooldownToPursue { get { return cooldownToPursue; } }
    public EnemyAnimator EnemyAnimator { get { return enemyAnimator; } }
    public bool IsStuck { get; set; }
    public Vector3 AgentVelocity { get { return agent.velocity; } }
    public Vector3 AgentDesiredVelocity { get { return agent.desiredVelocity; } }

    public void Initialize(IPlayerDamageDealer damageDealer)
    {
        playerDamageDealer = damageDealer;
        agent.updatePosition = false;
        agent.updateRotation = true;
    }

    public void ResetEnemy()
    {
        RefreshAgentSpeed(settings.MovementSpeed);
        previousFacingDirection = transform.forward;
        currentHealth = maxHealth;
        SharedInt healthVal = currentHealth;
        behaviorTree.SetVariable("CurrentHealth", healthVal);
        currencyDropper.Initialize(currencyDrop);
    }

    public void InitializeNewPatrolArea(PatrolArea area)
    {
        currentPatrolArea = area;
        SetWaypointTarget(currentPatrolArea.FirstWaypoint);
    }

    public void RefreshTargetWaypoint()
    {
        Waypoint pursuedWaypoint = null;

        if (pursueNodesDirection == 1)
        {
            targetWaypoint.RefreshNextNode();
            pursuedWaypoint = targetWaypoint.NextNode;
        }
        else
        {
            pursuedWaypoint = targetWaypoint.PreviousNode;
        }

        if (pursuedWaypoint != null)
        {
            targetWaypoint = pursuedWaypoint;
            SetAgentDestination(pursuedWaypoint.Position);
        }
        else
        {
            pursueNodesDirection = 1;

            if (OnClearedPatrolArea != null)
                OnClearedPatrolArea(currentPatrolArea, this);
        }
    }

    public void FetchFurthestWaypoint()
    {
        Vector3 trapDirection = Vector3.Normalize(DetectedBasicTrap.transform.position - transform.position);
        targetWaypoint.RefreshNextNode();

        List<Waypoint> nearbyWaypoints = new List<Waypoint>();

        nearbyWaypoints.Add(targetWaypoint);
        nearbyWaypoints.Add(targetWaypoint.NextNode);
        nearbyWaypoints.Add(targetWaypoint.PreviousNode);

        float?[] dotProducts = new float?[nearbyWaypoints.Count];

        for (int i = 0; i < dotProducts.Length; i++)
        {
            if (nearbyWaypoints[i] != null)
            {
                Vector3 waypointDirection = Vector3.Normalize(nearbyWaypoints[i].Position - DetectedBasicTrap.transform.position);
                dotProducts[i] = Vector3.Dot(trapDirection, waypointDirection);
            }
            else
            {
                dotProducts[i] = null;
            }
        }

        int indexOfFurthestPoint = 0;
        float valueOfFurthestPoint = dotProducts[0].Value;

        for (int i = 0; i < dotProducts.Length; i++)
        {
            if (dotProducts[i] != null)
            {
                if (dotProducts[i].Value < valueOfFurthestPoint)
                {
                    valueOfFurthestPoint = dotProducts[i].Value;
                    indexOfFurthestPoint = i;
                }
            }
        }

        // If the furthest point is the previous waypoint, reverse direction.
        if (indexOfFurthestPoint == 2)
        {
            pursueNodesDirection = -1;
        }
        else
        {
            pursueNodesDirection = 1;
        }

        Waypoint newTarget = nearbyWaypoints[indexOfFurthestPoint];

        targetWaypoint = newTarget;
        SetAgentDestination(newTarget.Position);
    }

    public Vector3 ValidateNavigationPosition(Vector3 position)
    {
        NavMeshHit hit;

        // If the point doesn't exist on the NavMesh
        if (!NavMesh.SamplePosition(position, out hit, 0f, NavMesh.AllAreas))
        {
            // Find closest point on the NavMesh
            if (NavMesh.SamplePosition(position, out hit, float.MaxValue, NavMesh.AllAreas))
            {
                position = hit.position;
            }
        }

        return position;
    }

    public void SetAgentDestination(Vector3 destination)
    {
        if (Frozen || !gameObject.activeInHierarchy) return;
        agent.SetDestination(destination);
    }

    public void ResetAgentPath()
    {
        agent.ResetPath();
    }

    public void RefreshAgentSpeed(float newSpeed)
    {
        currentAgentMovementSpeed = newSpeed;
    }

    public void Wander()
    {
        if (!gameObject.activeInHierarchy) return;

        PlaySFX(false);
        PlaySniffParticles(false);
        targetSmellNode = null;
        targetSoundPosition = null;
        PursuingPlayer = false;

        RefreshAgentSpeed(settings.MovementSpeed);
        agent.SetDestination(targetWaypoint.Position);
    }

    public void PursuePlayer()
    {
        // If the enemey should stay in patrol area and the player is outside of the patrol area, ignore player.
        if (stayInPatrolArea)
        {
            float areaRadius = currentPatrolArea.AreaRadius;

            float sqrDist = Vector3.SqrMagnitude(playerData.transform.position - currentPatrolArea.AreaCenter);

            if (sqrDist > areaRadius * areaRadius)
            {
                return;
            }
        }

        PlaySFX(true);

        // Pursue player
        RefreshAgentSpeed(settings.MovementSpeed * pursuePlayerSpeedMultiplier);

        agent.ResetPath();
        currentDestination = playerData.transform.position;
        agent.SetDestination(currentDestination);
        PursuingPlayer = true;
    }

    public void TakeDamage(int damage)
    {
        currentHealth = Mathf.Clamp(currentHealth - damage, 0, int.MaxValue);
        healthBar.SetCurrentHP(currentHealth);
        SharedInt healthVal = currentHealth;
        behaviorTree.SetVariable("CurrentHealth", healthVal);
    }

    public void DestroyEnemy()
    {
        DropCurrency();
        agent.ResetPath();
        PlaySFX(false);
        PlaySniffParticles(false);
        gameObject.SetActive(false);

        if (OnDestroyed != null)
            OnDestroyed(this);

        GameplayEvents.OnEnemyDied?.Invoke();
    }

    public void PlaySniffParticles(bool play)
    {
        if (!enableSniffingParticles) return;

        if (play)
            sniffParticles.Play();
        else
            sniffParticles.Stop();
    }

    public void PlaySFX(bool play)
    {
        if (sfxAudioSource.isPlaying != play)
        {
            if (play)
                sfxAudioSource.Play();
            else
                sfxAudioSource.Stop();
        }
    }

    public void OnSoundDetected(Vector3 position, float strength, float radius, SoundType soundType, BasicTrap basicTrap)
    {
        if (OnSoundTriggered != null)
            OnSoundTriggered(position, strength, radius, soundType, basicTrap);
    }

    public void StartRepulsionPursueCooldown()
    {
        cooldownToPursue = repulsionPursueCooldown;
    }

    public void ResetStuck()
    {
        if (!IsStuck) return;

        stuckMonitorCooldownTimeLeft = stuckMonitorCooldown;
        IsStuck = false;
    }
    #endregion

    #region PRIVATE
    private void LoadSettings()
    {
        maxHealth = settings.MaxHealth;
        turnSlowdownMultiplier = settings.TurnSlowdownMultiplier;
        smellMultiplier = settings.SmellMultiplier;
        attackDamage = settings.AttackDamage;
        pursueSmellSpeedMultiplier = settings.PursueSmellSpeedMultiplier;
        pursuePlayerSpeedMultiplier = settings.PursuePlayerSpeedMultiplier;
        postAttackSpeedMultiplier = settings.PostAttackSpeedMultiplier;
        maxPursueDistance = settings.MaxPursueDistance;
        enableMaxPursueDistanceGizmo = settings.EnableMaxPursueDistanceGizmo;
        maxPursueDistanceGizmoColor = settings.MaxPursueDistanceGizmoColor;
        minSoundPlayerPursueDistance = settings.MinSoundPlayerPursueDistance;
        enableMinSoundPlayerPursueDistanceGizmo = settings.EnableMinSoundPlayerPursueDistanceGizmo;
        minSoundPlayerPursueDistanceGizmoColor = settings.MinSoundPlayerPursueDistanceGizmoColor;
        currencyDrop = settings.CurrencyDrop;
        transparencyController.enabled = settings.EnableTransparency;
        repulsionPursueCooldown = settings.RepulsionPursueCooldown;
        stayInPatrolArea = settings.StayInPatrolArea;
        enableFootstepsParticles = settings.EnableFootstepsParticles;
        enableSniffingParticles = settings.EnableSniffingParticles;
        debugGizmos = settings.EnableDebugGizmos;
        gizmosColor = settings.GizmosColor;

        // Not constant
        currentAgentMovementSpeed = settings.MovementSpeed;
    }

    private void SubscribeToEvents()
    {
        enemyCollision.OnTrapDetected += OnBasicTrapDetected;
        enemyCollision.OnSmellDetected += OnSmellDetected;
        enemyCollision.OnPlayerDetected += OnPlayerDetected;

        if (awarenessCollision != null)
        {
            awarenessCollision.OnPlayerDetected += OnAwarenessPlayerDetected;
        }
    }

    private void UnsubscribeFromEvents()
    {
        enemyCollision.OnTrapDetected -= OnBasicTrapDetected;
        enemyCollision.OnSmellDetected -= OnSmellDetected;
        enemyCollision.OnPlayerDetected -= OnPlayerDetected;

        if (awarenessCollision != null)
        {
            awarenessCollision.OnPlayerDetected -= OnAwarenessPlayerDetected;
        }
    }

    private void PlayFootstepsParticles()
    {
        if (!enableFootstepsParticles) return;

        if (PursuingPlayer)
        {
            if (footstepsParticles.isPlaying)
                footstepsParticles.Stop();

            return;
        }

        if (agent.desiredVelocity != Vector3.zero && !footstepsParticles.isPlaying)
        {
            footstepsParticles.Play();
        }
        else if (agent.desiredVelocity == Vector3.zero && footstepsParticles.isPlaying)
        {
            footstepsParticles.Stop();
        }
    }

    private void UpdateAnimator()
    {
        if (enemyAnimator == null) return;

        if (walkAnimationCooldownRemaining > 0f)
        {
            walkAnimationCooldownRemaining -= Time.deltaTime;
            return;
        }

        if (allAnimationsCooldownTime > 0f)
        {
            allAnimationsCooldownTime -= Time.deltaTime;
            return;
        }

        Vector3 currentVelocity = agent.velocity;

        if (currentVelocity == Vector3.zero)
        {
            idleAnimationCooldownTime += Time.time;
            if (idleAnimationCooldownTime > idleAnimationCooldown)
            {
                PlayEnemyAnimation(enemyAnimator.PlayIdle, EnemyAnimation.Idle);
                idleAnimationCooldownTime = 0f;
                return;
            }
        }
        else
        {
            idleAnimationCooldownTime = 0f;
        }

        float movementAngle = Vector3.SignedAngle(Vector3.forward, currentVelocity, Vector3.up);

        if (Mathf.Abs(movementAngle) < 45)
        {
            PlayEnemyAnimation(enemyAnimator.PlayWalkBack, EnemyAnimation.Back);
        }
        else if (Mathf.Abs(movementAngle) > 135)
        {
            PlayEnemyAnimation(enemyAnimator.PlayWalkFront, EnemyAnimation.Front);
        }
        else if (movementAngle > 0)
        {
            Vector3 newScale = scaleRoot.localScale;
            newScale.x = 1f;
            scaleRoot.localScale = newScale;
            PlayEnemyAnimation(enemyAnimator.PlayWalkRight, EnemyAnimation.Right);
        }
        else
        {
            Vector3 newScale = scaleRoot.localScale;
            newScale.x = -1f;
            scaleRoot.localScale = newScale;
            PlayEnemyAnimation(enemyAnimator.PlayWalkRight, EnemyAnimation.Left);
        }
    }

    private void PlayEnemyAnimation(Action<float> animationAction, EnemyAnimation animation)
    {
        if (currentAnimation == animation) return;

        animationAction(1f);
        currentAnimation = animation;
        allAnimationsCooldownTime = allAnimationsCooldown;
    }

    private void UpdateRotationRoot()
    {
        rotationRoot.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
    }

    // If the enemy should stay in the patrol area, return to wander state whenever the enemy is outside the patrol area.
    private void ProcessStayingInPatrolArea()
    {
        if (currentHealth == 0) return;
        if (!stayInPatrolArea) return;
        if (currentPatrolArea == null) return;
        if (repulsionPursueCooldown > 0f) return;

        float sqrDistance = Vector3.SqrMagnitude(transform.position - currentPatrolArea.AreaCenter);

        if (sqrDistance >= currentPatrolArea.AreaRadius * currentPatrolArea.AreaRadius)
        {
            StartRepulsionPursueCooldown();
            Wander();
        }
    }

    private void ProcessPursueCooldown()
    {
        if (cooldownToPursue > 0f)
            cooldownToPursue = Mathf.Clamp(cooldownToPursue - Time.deltaTime, 0f, float.MaxValue);
    }

    private void MonitorEnemyPosition()
    {
        // Return if there is a cooldown
        if (stuckMonitorCooldownTimeLeft > 0f)
        {
            stuckMonitorCooldownTimeLeft = Mathf.Clamp(stuckMonitorCooldownTimeLeft - Time.deltaTime, 0f, float.MaxValue);
            return;
        }

        if (Frozen || agentAngularVelocity > 0f) return;

        float sqrDistance = Vector3.SqrMagnitude(transform.position - cachedPosition);

        if (sqrDistance < minStuckDist * minStuckDist)
        {
            timePassedSinceStuck += Time.deltaTime;
        }
        else
        {
            timePassedSinceStuck = 0f;
        }

        if (timePassedSinceStuck > enemyStuckTime && !IsStuck)
        {
            IsStuck = true;
        }
        else if (IsStuck)
        {
            IsStuck = false;
        }

        cachedPosition = transform.position;
    }

    /// <summary>
    /// Calculate angular velocity
    /// Answer by Baste
    /// https://forum.unity.com/threads/detecting-if-a-navmeshagent-is-rotating.410197/
    /// </summary>
    private void MonitorAngularVelocity()
    {
        Vector3 facingDirection = transform.forward;
        agentAngularVelocity = Vector3.Angle(facingDirection, previousFacingDirection) / Time.deltaTime;
        previousFacingDirection = facingDirection;
    }

    private void MoveEnemy()
    {
        if (agent.desiredVelocity != Vector3.zero)
        {
            myRigidbody.velocity = Vector3.zero;
            myRigidbody.MovePosition(agent.nextPosition);
        }
    }

    private void OnBasicTrapDetected(BasicTrap basicTrap)
    {
        DetectedBasicTrap = basicTrap;
    }

    private void SetupHPBar()
    {
        healthBar.SetMaxHP(maxHealth);
    }

    private void DropCurrency()
    {
        if (currencyDrop <= 0) return;

        currencyDropper.DropCurrency();
    }

    private void OnSmellDetected(SmellNode smellNode)
    {
        SmellDetected = true;
        DetectedSmellNode = smellNode;
    }

    private void OnPlayerDetected()
    {
        if (!PursuingPlayer || Frozen) return;

        playerDamageDealer.DealDamage(attackDamage);

        if (enemyAnimator != null)
        {
            enemyAnimator.PlayAttack();
            walkAnimationCooldownRemaining = walkAnimationCooldown;
        }

        RefreshAgentSpeed(settings.MovementSpeed * postAttackSpeedMultiplier);
    }

    private void OnAwarenessPlayerDetected(Vector3 position)
    {
        if (PursuingPlayer) return;

        PursuePlayer();
    }

    private void SetWaypointTarget(Waypoint target)
    {
        targetWaypoint = target;
        agent.SetDestination(targetWaypoint.Position);
    }

    private void ApplyAgentSpeed()
    {
        float angularVelocityMultiplier = 1f - (Mathf.Clamp(agentAngularVelocity * turnSlowdownMultiplier, 0f, 360f) / 360f);
        float newSpeed = currentAgentMovementSpeed * angularVelocityMultiplier;
        agent.speed = newSpeed;
    }

    private void Log(string message)
    {
        if (log)
            Debug.Log(message);
    }
    #endregion
}