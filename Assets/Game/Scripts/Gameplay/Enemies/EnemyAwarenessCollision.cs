using System;
using UnityEngine;

public class EnemyAwarenessCollision : MonoBehaviour
{
    private const string playerTag = "Player";

    public Action<Vector3> OnPlayerDetected = null;

    #region UNITY
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(playerTag))
        {
            if (OnPlayerDetected != null)
            {
                OnPlayerDetected(other.transform.position);
            }
        }
    }
    #endregion
}