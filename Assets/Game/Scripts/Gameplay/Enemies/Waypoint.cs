using Apotropaic.Data;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour
{
    [SerializeField] private List<Waypoint> nextNodes = null;
    [SerializeField] private WaypointSettings waypointSettings = null;

    Waypoint previousNode = null;
    Waypoint nextNode = null;

    public Waypoint NextNode { get { return nextNode; } }
    public Waypoint PreviousNode { get { return previousNode; } }
    public Vector3 Position { get { return transform.position; } }

    private void Start()
    {
        if (nextNodes != null && nextNodes.Count > 0)
        {
            for (int i = 0; i < nextNodes.Count; i++)
            {
                nextNodes[i].SetPreviousNode(this);
            }
        }
    }

    public void RefreshNextNode()
    {
        nextNode = (nextNodes == null || nextNodes.Count == 0) ? null : nextNodes[Random.Range(0, nextNodes.Count)];
    }

    private void SetPreviousNode(Waypoint node)
    {
        previousNode = node;
    }

    private void OnDrawGizmos()
    {
        if (waypointSettings == null || !waypointSettings.EnableGizmos) return;

        Gizmos.color = waypointSettings.GizmosColor;

        for (int i = 0; i < nextNodes.Count; i++)
        {
            if (nextNodes[i] != null)
                Gizmos.DrawLine(transform.position, nextNodes[i].transform.position);
        }

        Gizmos.color = Color.white;
    }
}