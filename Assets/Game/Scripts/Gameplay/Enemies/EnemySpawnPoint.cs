using UnityEngine;

public class EnemySpawnPoint : MonoBehaviour
{
    [SerializeField] private PatrolArea patrolArea = null;

    private Color gizmoColor = Color.white;
    private bool drawGizmos = false;
    private float minSpawnDistance = 0f;

    #region UNITY
    private void OnDrawGizmos()
    {
        if (drawGizmos)
        {
            Gizmos.color = gizmoColor;
            Gizmos.DrawWireSphere(transform.position, minSpawnDistance);

            if (patrolArea != null && patrolArea.FirstWaypoint != null)
                Gizmos.DrawLine(transform.position, patrolArea.FirstWaypoint.transform.position);

            Gizmos.color = Color.white;
        }
    }
    #endregion

    #region PUBLIC
    public PatrolArea PatrolArea { get { return patrolArea; } }

    public void Initialize(bool enableGizmos, float minSpawnDist, Color gizmosColor)
    {
        drawGizmos = enableGizmos;
        minSpawnDistance = minSpawnDist;
        gizmoColor = gizmosColor;
    }
    #endregion

    #region PRIVATE
    #endregion
}
