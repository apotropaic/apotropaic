using UnityEngine;

public class CurrencyDrop : MonoBehaviour
{
    private int currencyValue = 0;

    #region PUBLIC
    public int CurrencyValue { get { return currencyValue; } set { currencyValue = value; } }

    public void DestroyCurrency()
    {
        Destroy(gameObject);
    }
    #endregion
}