using System;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

using Apotropaic.Data;
using Apotropaic.Utility;
using Apotropaic.Core;
using System.Collections.Generic;
using Apotropaic.Events;

public class PlayerController : MonoBehaviour, IPlayerDamageDealer
{
    // TODO : Needs refactoring.
    bool shopFlag = false;
    bool BagFlag = false;
    IInteractable interactable = null;


    public enum PlayerMode
    {
        GameplayMode = 0,
        RegularMode = 1
    }

    [Header("Settings")]
    [SerializeField] private PlayerSettings playerSettings = null;

    // Settings
    private float rotationSpeed = 0f;
    private int maxHealth = 0;
    private float takenDamageCooldown = 0f;
    private bool enableFootstepsParticles = false;
    private bool enableSmellParticles = false;

    [Space(20)]
    [SerializeField] private PlayerFSM fsm = null;
    [Header("Data")]
    [SerializeField] private PlayerData playerData = null;
    [SerializeField] private TrapSpawnerData trapSpawnerData = null;
    [SerializeField] private GameplayData gameplayData = null;
    [Space(20)]
    [SerializeField] private VisualBar trapPlacementProgressBar = null;
    [SerializeField] private Transform lightRotationRoot = null;
    [SerializeField] private Transform cameraTargetRotationRoot = null;
    [SerializeField] private Transform trapPlacementRotationRoot = null;
    [SerializeField] private Transform rendererTransform = null;
    [SerializeField] private Transform trapPlacementRoot = null;
    [SerializeField] private Transform cameraTargetTransform = null;
    [SerializeField] private GameObject spotLight = null;

    [SerializeField] private SceneSettings sceneSettings = null;

    [SerializeField] private AudioSource sfxAudioSource = null;
    [SerializeField] private AudioClip hitAudioClip = null;

    [SerializeField] private TrapPlacer trapPlacer = null;

    [SerializeField] private TrapSettingsList trapList = null;
    [SerializeField] private PlayerInventory playerInventory = null;

    [SerializeField] private ParticleSystem footstepsParitcles = null;
    [SerializeField] private ParticleSystem tiptoeParitcles = null;
    [SerializeField] private ParticleSystem smellParticles = null;

    private InventoryManager inventoryManager = null;

    [Header("Sounds")]
    [SerializeField] private SoundPlayer walkSound = null;
    [SerializeField] private SoundPlayer tiptoeSound = null;

    public Action<InputAction.CallbackContext> OnRightDirections = null;
    public Action<InputAction.CallbackContext> OnLeftDirections = null;
    public Action<InputAction.CallbackContext> OnActionS = null;
    public Action<InputAction.CallbackContext> OnActionE = null;
    public Action<InputAction.CallbackContext> OnActionW = null;
    public Action<InputAction.CallbackContext> OnActionN = null;
    public Action<InputAction.CallbackContext> OnActionR1 = null;
    public Action<InputAction.CallbackContext> OnAction_Start = null;
    public Action<InputAction.CallbackContext> OnAction_Select = null;
    public Action<InputAction.CallbackContext> OnAction_R2 = null;

    private List<Action<InputAction.CallbackContext>> inputActionsBuffer = null;
    private List<InputAction.CallbackContext> inputContextBuffer = null;

    private PlayerMode playerMode = PlayerMode.GameplayMode;
    private int currentHealth = 0;
    private Rigidbody myRigidbody = null;
    private Vector2 inputMovementDirection = Vector2.zero;
    private Vector2 currentMovementDirection = Vector2.zero;
    private Vector3 targetRotationDirection = Vector3.zero;
    private float currentMovementSpeed = 1.0f;
    private float trapPlacementTime = 0f;
    private float damageTakenCooldownRemaining = 0f;
    private bool playerRotationLocked = true;
    private bool trapPlacementRotationLocked = false;
    private bool lockTrapLocalOrientation = true;
    private bool lockTrapPlacement = true;
    private bool movementLocked = false;
    private bool movementInputLocked = true;

    #region UNITY
    private void Awake()
    {
        InitializePlayer();
    }

    private void OnDestroy()
    {
        if (SceneLoader.isValidSingleton())
            SceneLoader.Instance.onSceneLoadedEvent.RemoveListener(OnSceneLoaded);

        UnsubscribeFromEvents();
    }

    private void Update()
    {
        ProcessTakenDamageTime();
        if (playerInventory.TrapList.Count != 0)
            if (trapSpawnerData.CurrentSelectedTrap == null)
                ToggleTraps();
    }

    private void FixedUpdate()
    {
        MovePlayer();
        ApplyRotation();
        UpdateTrapPlacementPosition();
    }
    #endregion

    #region IPlayerDataProvider
    public Vector3 Position { get { return transform.position; } }
    #endregion

    #region IPlayerDamageDealer
    public void DealDamage(int damage)
    {
        if (gameplayData.Invincibility == true) return;

        if (damageTakenCooldownRemaining > 0) return;

        damageTakenCooldownRemaining = takenDamageCooldown;

        sfxAudioSource.PlayOneShot(hitAudioClip);

        currentHealth = Mathf.Clamp(currentHealth - damage, 0, maxHealth);

        if (!gameplayData.FinishedNightTutorial && currentHealth <= 0)
        {
            currentHealth = 1;
        }

        playerData.CurrentHealth = currentHealth;

        if (GameplayEvents.OnPlayerTakenDamage != null)
            GameplayEvents.OnPlayerTakenDamage(currentHealth);

        if (currentHealth <= 0)
        {
            if (GameplayEvents.OnPlayerDied != null)
                GameplayEvents.OnPlayerDied();
        }
    }
    #endregion

    #region PUBLIC
    public void InitializePlayer()
    {
        if (SceneLoader.isValidSingleton())
        {
            SceneLoader.Instance.onSceneLoadedEvent.AddListener(OnSceneLoaded);
        }

        myRigidbody = GetComponent<Rigidbody>();
        inventoryManager = GetComponentInChildren<InventoryManager>();
        targetRotationDirection = lightRotationRoot.forward;
        inputActionsBuffer = new List<Action<InputAction.CallbackContext>>();
        inputContextBuffer = new List<InputAction.CallbackContext>();

        InitializePlayerData();
        LoadSettings();
        SubscribeToevents();
    }

    public PlayerSettings Settings { get { return playerSettings; } }
    public bool MovementInputLocked { get { return movementInputLocked; } }
    public Vector2 InputMovement { get { return inputMovementDirection; } set { inputMovementDirection = value; if (value != Vector2.zero) currentMovementDirection = value; } }
    public float CurrentMovementSpeed { set { currentMovementSpeed = value; } }
    public PlayerMode CurrentPlayerMode { get { return playerMode; } }
    public float TrapPlacementTime { get { return trapPlacementTime; } }
    public float TargetRotationAngle { get { return Vector3.SignedAngle(Vector3.forward, targetRotationDirection, Vector3.up); } }
    public Vector2 CurrentMovementDirection { get { return currentMovementDirection; } }

    public void EnableTrapPlacementProgressBar()
    {
        trapPlacementProgressBar.EnableBar(true);
        trapPlacementProgressBar.SetMaxVal(trapPlacer.PlacementTime);
    }

    public void RefreshTrapPlacementProgressBar(float currentVal)
    {
        trapPlacementProgressBar.SetCurrentVal(currentVal);
    }

    public void DisableTrapPlacementProgressBar()
    {
        trapPlacementProgressBar.EnableBar(false);
    }

    public void LockPlayerRotation(bool lockPlacement)
    {
        playerRotationLocked = lockPlacement;
    }

    public void LockTrapPlacementRotation(bool lockRotation)
    {
        trapPlacementRotationLocked = lockRotation;
    }

    public void LockPlayerMovement(bool lockMovement)
    {
        movementLocked = lockMovement;
    }

    public void LockPlayerMovementInput(bool lockMovement)
    {
        movementInputLocked = lockMovement;
    }

    public void LockTrapPlacement(bool lockPlacement)
    {
        lockTrapPlacement = lockPlacement;
        CancelTrapPlacement();
    }

    public void LockPlayer(bool lockPlayer)
    {
        LockPlayerMovementInput(lockPlayer);
        LockPlayerRotation(lockPlayer);
        LockTrapPlacement(lockPlayer);
    }

    public void PlayFootstepsParticles(bool play)
    {
        PlayMovementParticles(play, footstepsParitcles);
    }

    public void PlayTiptoeParticles(bool play)
    {
        PlayMovementParticles(play, tiptoeParitcles);
    }

    #region INPUT
    public void OnActionStart(InputAction.CallbackContext value)
    {
        ResolvePlayerInput(value, (context) => OnAction_Start?.Invoke(context));
    }

    public void OnActionSelect(InputAction.CallbackContext value)
    {
        ResolvePlayerInput(value, (context) => OnAction_Select?.Invoke(context));
    }

    public void OnActionLeftDirections(InputAction.CallbackContext value)
    {
        ResolvePlayerInput(value, (context) => OnLeftDirections?.Invoke(context));
    }

    public void OnActionRightDirections(InputAction.CallbackContext value)
    {
        ResolvePlayerInput(value, (context) => OnRightDirections?.Invoke(context));

        // TODO : Move to states?
        Vector2 axis = value.ReadValue<Vector2>();

        if (axis != Vector2.zero)
            targetRotationDirection = new Vector3(-axis.x, 0, -axis.y);
    }

    public void OnActionSouth(InputAction.CallbackContext value)
    {
        ResolvePlayerInput(value, (context) => OnActionS?.Invoke(context));
    }

    public void OnActionEast(InputAction.CallbackContext value)
    {
        ResolvePlayerInput(value, (context) => OnActionE?.Invoke(context));
    }

    public void OnActionWest(InputAction.CallbackContext value)
    {
        ResolvePlayerInput(value, (context) => OnActionW?.Invoke(context));
    }

    public void OnActionNorth(InputAction.CallbackContext value)
    {
        ResolvePlayerInput(value, (context) => OnActionN?.Invoke(context));
    }

    public void OnActionRightShoulder(InputAction.CallbackContext value)
    {
        ResolvePlayerInput(value, (context) => OnActionR1?.Invoke(context));
    }

    public void OnActionR2(InputAction.CallbackContext value)
    {
        ResolvePlayerInput(value, (context) => OnAction_R2?.Invoke(context));
    }

    public void OnActionF(InputAction.CallbackContext value)
    {
        if (movementInputLocked == true) return;
        if (Time.timeScale == 0f) return;
        if (value.phase != InputActionPhase.Started) return;

        if (shopFlag == true)
            if (UIEvents.OnOpenedShop != null)
                UIEvents.OnOpenedShop.Invoke();

        if (BagFlag == true)
        {
            if (UIEvents.OnOpenedBag != null)
            {
                UIEvents.OnOpenedBag.Invoke();
            }
        }

        if (interactable != null)
        {
            interactable.Interact();
        }
    }

    public void OnMouseMove(InputAction.CallbackContext value)
    {
        if (Camera.main == null) return;

        Vector2 mousePos = value.ReadValue<Vector2>();
        RaycastHit hit;

        Ray ray = Camera.main.ScreenPointToRay(mousePos);
        int layerMask = 1 << 16;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
        {
            Vector3 hitPos = hit.point;
            targetRotationDirection = Vector3.Normalize(transform.position - hitPos);
        }
    }
    #endregion

    public void SelectTrap()
    {
        if (lockTrapPlacement) return;

        if (trapPlacer.IsPlacingTrap)
        {
            CancelTrapPlacement();
        }
        else
        {
            //=====================================================
            // Changes here for integration
            //=====================================================
            if (trapSpawnerData.CurrentSelectedTrap == null)
            {
                ToggleTraps();
            }
            if (playerInventory.TrapList.Count != 0 && !playerInventory.TrapList.ContainsKey(trapSpawnerData.CurrentSelectedTrap.UID))
            {
                ToggleTraps();
            }
            if (playerInventory.TrapList.Count != 0)
            {
                trapPlacer.StartTrapPlacement(trapPlacementRoot.position);
            }
        }
    }

    public bool AttemptPlaceTrap()
    {
        if (lockTrapPlacement) return false;
        if (!trapPlacer.IsPlacingTrap) return false;
        if (playerInventory.TrapList.Count == 0) return false;
        if (trapPlacer.AttemptPlaceTrap())
        {
            trapPlacementTime = trapPlacer.PlacementTime;
            return true;
        }

        return false;
    }

    public void StopPlacingTrap()
    {
        trapPlacementTime = 0f;
    }

    public void PlaceTrap()
    {
        if (lockTrapPlacement) return;

        trapPlacer.PlaceTrap();
        inventoryManager.RemoveTrap(trapSpawnerData.CurrentSelectedTrap.UID);
        if (!playerInventory.TrapList.ContainsKey(trapSpawnerData.CurrentSelectedTrap.UID))
            ToggleTraps();
        if (GameplayEvents.OnPlacedTrap != null)
        {
            GameplayEvents.OnPlacedTrap.Invoke();
        }

    }

    //=====================================================
    // Changes here for integration
    //=====================================================
    public void ToggleTraps()
    {
        if (lockTrapPlacement) return;

        bool reselectTrap = false;

        if (trapPlacer.IsPlacingTrap)
        {
            CancelTrapPlacement();
            reselectTrap = true;
        }

        trapSpawnerData.CurrentTrapSelectionIndex++;

        if (trapSpawnerData.CurrentTrapSelectionIndex >= playerInventory.TrapList.Count)
        {
            trapSpawnerData.CurrentTrapSelectionIndex = 0;
        }

        for (int i = 0; i < trapList.Traps.Length; i++)
        {
            if (playerInventory.TrapList.Count != 0)
            {
                if (trapList.Traps[i].UID == playerInventory.TrapList.ElementAt(trapSpawnerData.CurrentTrapSelectionIndex).Key)
                {
                    trapSpawnerData.CurrentSelectedTrap = trapList.Traps[i];

                    lockTrapLocalOrientation = trapSpawnerData.CurrentSelectedTrap.LockTrapOrientation;

                    if (lockTrapLocalOrientation)
                    {
                        trapPlacer.UpdateActiveTrapOrientation(Quaternion.identity);
                    }
                }
            }
            else
            {
                trapSpawnerData.CurrentSelectedTrap = null;
                trapSpawnerData.CurrentTrapSelectionIndex = 0;
            }
        }

        GameplayEvents.OnToggleTrap?.Invoke();

        if (reselectTrap)
        {
            SelectTrap();
        }
    }

    // Sounds
    public void PlayWalkSFX(bool play)
    {
        if (play)
        {
            walkSound.AttemptPlaySound();
        }
        else
        {
            walkSound.StopSound();
        }
    }

    public void PlayTiptoeSFX(bool play)
    {
        if (play)
        {
            tiptoeSound.AttemptPlaySound();
        }
        else
        {
            tiptoeSound.StopSound();
        }
    }
    #endregion

    #region PRIVATE
    private void OnSceneLoaded(List<string> scene)
    {
        SetupPlayerMode();
    }

    private void SetupPlayerMode()
    {
        string activeSceneName = SceneManager.GetActiveScene().name;
        string officesSceneName = sceneSettings.OfficesScene.SceneName;
        string librarySceneName = sceneSettings.LibraryScene.SceneName;

        if (activeSceneName == officesSceneName)
        {
            playerMode = PlayerMode.RegularMode;
        }
        else if (activeSceneName == librarySceneName)
        {
            playerMode = PlayerMode.GameplayMode;
        }

        InitializePlayerMode();
    }

    private void InitializePlayerMode()
    {
        if (playerMode == PlayerMode.RegularMode)
        {
            spotLight.SetActive(false);
            LockPlayer(false);
            smellParticles.Stop();
        }
        else if (playerMode == PlayerMode.GameplayMode)
        {
            spotLight.SetActive(true);
            LockPlayer(true);

            if (enableSmellParticles)
                smellParticles.Play();
            else
                smellParticles.Stop();
        }
    }

    private void InitializePlayerData()
    {
        playerData.transform = transform;
        playerData.CameraTargetTransform = cameraTargetTransform;
        playerData.InventoryTrapAccessor = GetComponentInChildren<IInventoryTrapAccessor>();
    }

    private void LoadSettings()
    {
        rotationSpeed = playerSettings.RotationSpeed;
        maxHealth = playerSettings.MaxHealth;
        currentHealth = maxHealth;
        takenDamageCooldown = playerSettings.TakenDamageCooldown;
        enableFootstepsParticles = playerSettings.EnableFootstepsParticles;
        enableSmellParticles = playerSettings.EnableSmellParticles;

        playerData.CurrentHealth = currentHealth;
        playerData.MaxHealth = maxHealth;

        playerData.CurrentTrapsStock = ArrayUtility.CopyArray(playerSettings.TrapsStock);
    }

    private void SubscribeToevents()
    {
        fsm.OnStateChanged += OnStateChanged;
        UIEvents.OnConversationStarted += OnConversationStarted;
        UIEvents.OnConversationEnded += OnConversationEnded;
        GameplayEvents.OnLockPlayerMovementInput += LockPlayerMovementInput;
        GameplayEvents.OnLockTrapPlacement += LockTrapPlacement;
    }

    private void UnsubscribeFromEvents()
    {
        fsm.OnStateChanged -= OnStateChanged;
        UIEvents.OnConversationStarted -= OnConversationStarted;
        UIEvents.OnConversationEnded -= OnConversationEnded;
        GameplayEvents.OnLockPlayerMovementInput -= LockPlayerMovementInput;
        GameplayEvents.OnLockTrapPlacement -= LockTrapPlacement;
    }

    private void OnStateChanged(int stateHashCode)
    {
        int bufferCount = inputActionsBuffer.Count;
        if (bufferCount == 0) return;

        for (int i = 0; i < bufferCount; i++)
        {
            inputActionsBuffer[i]?.Invoke(inputContextBuffer[i]);
        }

        inputActionsBuffer.Clear();
        inputContextBuffer.Clear();
    }

    private void OnConversationStarted(List<ConversationData> conversationData, Sprite _sprite)
    {
        LockPlayer(true);
    }

    private void OnConversationEnded()
    {
        LockPlayer(false);
    }

    private void ProcessTakenDamageTime()
    {
        if (damageTakenCooldownRemaining <= 0) return;

        damageTakenCooldownRemaining = Mathf.Clamp(damageTakenCooldownRemaining - Time.deltaTime, 0f, float.MaxValue);
    }

    private void MovePlayer()
    {
        if (inputMovementDirection == Vector2.zero || movementLocked || movementInputLocked) return;

        Vector2 normalizedInput = inputMovementDirection.normalized;
        Vector3 newPos = transform.position;
        Vector3 displacement = normalizedInput * currentMovementSpeed * Time.fixedDeltaTime;
        displacement.z = displacement.y;
        displacement.y = 0;

        newPos += displacement;
        myRigidbody.MovePosition(newPos);

        int scaleDirection;

        if (playerMode == PlayerMode.GameplayMode)
        {
            scaleDirection = (targetRotationDirection.x > 0) ? Mathf.CeilToInt(targetRotationDirection.x) : Mathf.FloorToInt(targetRotationDirection.x);
        }
        else
        {
            scaleDirection = inputMovementDirection.x >= 0 ? -1 : 1;
        }

        if (scaleDirection != 0f)
        {
            Vector3 newScale = rendererTransform.localScale;
            newScale.x = scaleDirection * -1;
            rendererTransform.localScale = newScale;
        }
    }

    private void ApplyRotation()
    {
        if (targetRotationDirection == lightRotationRoot.forward) return;
        if (playerRotationLocked) return;

        Vector3 newRot = Vector3.RotateTowards(lightRotationRoot.forward, targetRotationDirection, rotationSpeed * Time.fixedDeltaTime, 0.0f);
        lightRotationRoot.rotation = Quaternion.LookRotation(newRot);
        cameraTargetRotationRoot.rotation = lightRotationRoot.rotation;

        if (!trapPlacementRotationLocked)
            trapPlacementRotationRoot.rotation = lightRotationRoot.rotation;

        if (!lockTrapLocalOrientation && !trapPlacementRotationLocked)
            trapPlacer.UpdateActiveTrapOrientation(lightRotationRoot.rotation);
    }

    private void UpdateTrapPlacementPosition()
    {
        if (!trapPlacer.IsPlacingTrap) return;

        trapPlacer.UpdateActiveTrapPosition(trapPlacementRoot.position);
    }

    private void CancelTrapPlacement()
    {
        if (!trapPlacer.IsPlacingTrap) return;

        trapPlacer.StopTrapPlacement();
    }

    private void ResolveCurrency(CurrencyDrop currencyDrop)
    {
        playerInventory.Currency += currencyDrop.CurrencyValue;
        currencyDrop.DestroyCurrency();
    }

    private void ResolvePlayerInput(InputAction.CallbackContext context, Action<InputAction.CallbackContext> action)
    {
        if (fsm.IsTransitioning)
        {
            inputActionsBuffer.Add(action);
            inputContextBuffer.Add(context);
        }
        else
        {
            if (action != null)
                action(context);
        }
    }

    // TODO : Needs refactoring.
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Shop")
            shopFlag = true;
        if (other.tag == "Bag")
        {
            BagFlag = true;
        }

        if (LayerMask.LayerToName(other.gameObject.layer) == "Interactable")
        {
            interactable = other.gameObject.GetComponent<IInteractable>();
        }
        if (other.tag == "Currency")
        {
            ResolveCurrency(other.GetComponent<CurrencyDrop>());
        }
    }

    // TODO : Needs refactoring.
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Shop")
            shopFlag = false;
        if (other.tag == "Bag")
        {
            BagFlag = false;
        }

        if (LayerMask.LayerToName(other.gameObject.layer) == "Interactable")
        {
            interactable = null;
        }

    }

    private void PlayMovementParticles(bool play, ParticleSystem particles)
    {
        if (!enableFootstepsParticles || playerMode != PlayerMode.GameplayMode) return;

        if (play)
            particles.Play();
        else
            particles.Stop();
    }
    #endregion
}
