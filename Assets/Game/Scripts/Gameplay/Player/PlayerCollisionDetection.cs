using System;
using UnityEngine;

// TODO : Not being used.
public class PlayerCollisionDetection : MonoBehaviour
{
    public Action<bool> OnRestockBag = null;
    public Action<CurrencyDrop> OnCurrencyDrop = null;

    #region UNITY
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Bag")
        {
            if (OnRestockBag != null)
                OnRestockBag(true);
        }

        if (other.tag == "Currency")
        {
            if (OnCurrencyDrop != null)
                OnCurrencyDrop(other.GetComponent<CurrencyDrop>());
        }
    }
    #endregion

    #region PUBLIC
    #endregion

    #region PRIVATE
    #endregion
}
