public interface IPlayerDamageDealer
{
    public void DealDamage(int damage);
}