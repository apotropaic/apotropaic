using UnityEngine;

public class TrapPlacer : MonoBehaviour
{
    [SerializeField] private TrapsSpawner trapSpawner = null;

    private ITrap activePlacementTrap = null;

    #region UNITY
    private void OnDrawGizmos()
    {
        // if (!placingTrap) return;

        // Gizmos.color = Color.green;
        // Gizmos.DrawSphere(currentTrapPos, 0.5f);
    }
    #endregion

    #region PUBLIC
    public bool IsPlacingTrap { get { return activePlacementTrap != null; } }
    public float PlacementTime { get { return activePlacementTrap.PlacementTime; } }

    public void StartTrapPlacement(Vector3 trapPos)
    {
        if (trapSpawner == null || activePlacementTrap != null) return;

        activePlacementTrap = trapSpawner.SpawnTrap(trapPos);
    }

    public void StopTrapPlacement()
    {
        if (trapSpawner == null || activePlacementTrap != null)
        {
            Destroy(activePlacementTrap.gameObject);
            activePlacementTrap = null;
        }
    }

    public bool AttemptPlaceTrap()
    {
        bool overlapping = activePlacementTrap.OverlapDetector.IsOverlapping;

        return !overlapping;
    }

    public void PlaceTrap()
    {
        activePlacementTrap.SetCurrentState(TrapState.Active);
        activePlacementTrap = null;
    }


    public void UpdateActiveTrapPosition(Vector3 trapPosition)
    {
        if (activePlacementTrap == null) return;

        activePlacementTrap.transform.position = trapPosition;
    }

    public void UpdateActiveTrapOrientation(Quaternion rotation)
    {
        if (activePlacementTrap == null) return;

        activePlacementTrap.transform.rotation = rotation;
    }
    #endregion

    #region PRIVATE
    #endregion
}
