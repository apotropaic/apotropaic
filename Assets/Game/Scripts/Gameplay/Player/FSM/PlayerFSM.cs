using UnityEngine;

public class PlayerFSM : FSM
{
    #region PUBLIC
    public int IdleState { get { return stateHashCodes[0]; } }
    public int WalkState { get { return stateHashCodes[1]; } }
    public int TiptoeState { get { return stateHashCodes[2]; } }
    public int PlacingTrapState { get { return stateHashCodes[3]; } }
    #endregion
}