using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerWalkState : PlayerMovementState
{
    #region UNITY
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);

        playerController.PlayTiptoeSFX(false);
        playerController.PlayWalkSFX(true);
        playerController.PlayTiptoeParticles(false);
        playerController.PlayFootstepsParticles(true);
    }
    #endregion

    #region PUBLIC
    public override void Initialize(GameObject _owner, FSM _fsm)
    {
        base.Initialize(_owner, _fsm);
        movementSpeed = playerSettings.WalkingSpeed;
    }
    #endregion

    #region PROTECTED
    protected override void OnNorth(InputAction.CallbackContext context)
    {
        base.OnNorth(context);

        if (context.started)
        {
            fsm.ChangeState(fsm.TiptoeState);
        }
    }
    #endregion
}