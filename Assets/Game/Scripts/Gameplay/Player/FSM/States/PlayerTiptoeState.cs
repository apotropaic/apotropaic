using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerTiptoeState : PlayerMovementState
{
    #region UNITY
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);

        playerController.PlayTiptoeSFX(true);
        playerController.PlayWalkSFX(false);
        playerController.PlayFootstepsParticles(false);
        playerController.PlayTiptoeParticles(true);
    }
    #endregion

    #region PUBLIC
    public override void Initialize(GameObject _owner, FSM _fsm)
    {
        base.Initialize(_owner, _fsm);
        movementSpeed = playerSettings.TiptoeSpeed;
    }
    #endregion

    #region PROTECTED
    protected override void OnNorth(InputAction.CallbackContext context)
    {
        base.OnNorth(context);

        if (!context.started)
        {
            fsm.ChangeState(fsm.WalkState);
        }
    }
    #endregion
}