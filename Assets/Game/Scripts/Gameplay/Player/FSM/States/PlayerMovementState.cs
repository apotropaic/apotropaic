using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovementState : PlayerActionState
{
    protected float movementSpeed = 0f;
    private float currentTargetRotationAngle = 0f;
    private Vector2 currentMovementDirection = Vector2.zero;

    #region UNITY
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);

        playerController.CurrentMovementSpeed = movementSpeed;

        currentTargetRotationAngle = playerController.TargetRotationAngle;
        currentMovementDirection = playerController.CurrentMovementDirection;

        PlayWalkAnimation();
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateUpdate(animator, stateInfo, layerIndex);

        if (currentTargetRotationAngle != playerController.TargetRotationAngle || currentMovementDirection != playerController.CurrentMovementDirection)
        {
            currentTargetRotationAngle = playerController.TargetRotationAngle;
            currentMovementDirection = playerController.CurrentMovementDirection;

            PlayWalkAnimation();
        }
    }
    #endregion

    #region PROTECTED
    protected override void OnLeftDirections(InputAction.CallbackContext context)
    {
        base.OnLeftDirections(context);

        // Unity new input system bug patch.
        // To reproduce: Hold down D and A at the same time and let go at the same time.
        if (context.phase == InputActionPhase.Waiting) return;

        Vector2 value = context.ReadValue<Vector2>();
        playerController.InputMovement = value;

        if (value == Vector2.zero)
            fsm.ChangeState(fsm.IdleState);
    }
    #endregion


    #region PRIVATE
    private void PlayWalkAnimation()
    {
        if (playerController.CurrentPlayerMode == PlayerController.PlayerMode.GameplayMode)
        {
            float movementAngle = Vector3.SignedAngle(Vector3.forward, new Vector3(currentMovementDirection.x, 0, currentMovementDirection.y), Vector3.up);
            // Alignment angle is used to determine the speed of the animation (inverse if the rotation is in the opposite direction of the movement)
            float alignmentAngle = (180f - Mathf.Abs(Mathf.DeltaAngle(movementAngle, currentTargetRotationAngle)));
            int speed = alignmentAngle < 90 ? 1 : -1;

            PlayWalkAnimation(currentTargetRotationAngle, speed, true);
        }
        else
        {
            float movementAngle = Vector3.Angle(Vector3.forward, new Vector3(currentMovementDirection.x, 0, currentMovementDirection.y));
            PlayWalkAnimation(movementAngle, 1, false);
        }
    }

    private void PlayWalkAnimation(float angle, int speed, bool gameplay)
    {
        if (Mathf.Abs(angle) <= 45)
        {
            if (gameplay)
                playerAnimator.PlayWalkFront(speed);
            else
                playerAnimator.PlayWalkBack(speed);
        }
        else if (Mathf.Abs(angle) <= 135)
        {
            playerAnimator.PlayWalkRight(speed);
        }
        else
        {
            if (gameplay)
                playerAnimator.PlayWalkBack(speed);
            else
                playerAnimator.PlayWalkFront(speed);
        }
    }
    #endregion
}