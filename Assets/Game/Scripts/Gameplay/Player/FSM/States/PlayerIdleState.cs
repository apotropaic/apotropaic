using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerIdleState : PlayerActionState
{
    [SerializeField] private InputActionAsset inputActions = null;

    private bool northIsDown = false;
    private System.Guid mapID = default;
    private System.Guid northActionID = default;

    #region UNITY
    public override void Initialize(GameObject _owner, FSM _fsm)
    {
        base.Initialize(_owner, _fsm);

        mapID = inputActions.FindActionMap("PlayerControls").id;
        northActionID = inputActions.FindActionMap(mapID).FindAction("Action_N").id;
        inputActions.FindActionMap(mapID).FindAction(northActionID).Enable();
    }

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);

        InputActionPhase northPhase = inputActions.FindActionMap(mapID).FindAction(northActionID).phase;
        northIsDown = (northPhase == InputActionPhase.Started || northPhase == InputActionPhase.Performed);
        if (playerController.CurrentMovementDirection.y <= 0)
        {
            playerAnimator.PlayIdle();
        }
        else
        {
            playerAnimator.PlayIdleBack();
        }

        playerController.PlayWalkSFX(false);
        playerController.PlayTiptoeSFX(false);
        playerController.PlayFootstepsParticles(false);
        playerController.PlayTiptoeParticles(false);
    }
    #endregion

    #region PROTECTED
    protected override void OnNorth(InputAction.CallbackContext context)
    {
        base.OnNorth(context);
        northIsDown = context.started || context.performed;
    }

    protected override void OnLeftDirections(InputAction.CallbackContext context)
    {
        base.OnLeftDirections(context);

        if (playerController.MovementInputLocked) return;

        Vector2 value = context.ReadValue<Vector2>();
        playerController.InputMovement = value;

        if (northIsDown)
        {
            fsm.ChangeState(fsm.TiptoeState);
        }
        else
        {
            fsm.ChangeState(fsm.WalkState);
        }
    }
    #endregion
}