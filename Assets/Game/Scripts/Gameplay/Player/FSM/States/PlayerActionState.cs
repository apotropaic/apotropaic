using UnityEngine;
using UnityEngine.InputSystem;

public abstract class PlayerActionState : PlayerBaseState
{
    #region UNITY
    #endregion

    #region PROTECTED
    protected override void OnRightShoulder(InputAction.CallbackContext context)
    {
        base.OnRightShoulder(context);
        if (!context.started) return;

        if (playerController.CurrentPlayerMode == PlayerController.PlayerMode.GameplayMode)
        {
            playerController.SelectTrap();
        }
    }

    protected override void OnR2(InputAction.CallbackContext context)
    {
        base.OnR2(context);
        if (!context.started) return;

        if (playerController.CurrentPlayerMode == PlayerController.PlayerMode.GameplayMode)
        {
            playerController.ToggleTraps();
        }
    }

    protected override void OnSouth(InputAction.CallbackContext context)
    {
        base.OnSouth(context);

        if (playerController.CurrentPlayerMode == PlayerController.PlayerMode.GameplayMode)
        {
            bool canPlaceTrap = playerController.AttemptPlaceTrap();

            if (canPlaceTrap)
            {
                playerController.LockTrapPlacementRotation(true);
                playerController.LockPlayerMovement(true);
                fsm.ChangeState(fsm.PlacingTrapState);
            }
        }
    }
    #endregion

    #region PRIVATE
    #endregion
}