using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerPlacingTrapState : PlayerBaseState
{
    [SerializeField] private InputActionAsset inputActions = null;

    private float trapPlacementTime = 0f;
    private float timePassed = 0f;

    private bool canCancelWithMovement = false;

    private bool northIsDown = false;
    private System.Guid mapID = default;
    private System.Guid northActionID = default;

    #region UNITY
    public override void Initialize(GameObject _owner, FSM _fsm)
    {
        base.Initialize(_owner, _fsm);

        mapID = inputActions.FindActionMap("PlayerControls").id;
        northActionID = inputActions.FindActionMap(mapID).FindAction("Action_N").id;
    }

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);

        InputActionPhase northPhase = inputActions.FindActionMap(mapID).FindAction(northActionID).phase;
        northIsDown = (northPhase == InputActionPhase.Started || northPhase == InputActionPhase.Performed);

        trapPlacementTime = playerController.TrapPlacementTime;
        timePassed = 0f;
        playerController.EnableTrapPlacementProgressBar();
        playerController.PlayWalkSFX(false);
        playerController.PlayTiptoeSFX(false);
        playerController.PlayTiptoeParticles(false);
        playerController.PlayFootstepsParticles(false);
        canCancelWithMovement = (playerController.InputMovement == Vector2.zero);
        playerAnimator.PlayPlacingTrap();
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateUpdate(animator, stateInfo, layerIndex);

        timePassed += Time.deltaTime;
        playerController.RefreshTrapPlacementProgressBar(Mathf.Clamp(timePassed, 0f, trapPlacementTime));

        if (timePassed >= trapPlacementTime)
        {
            playerController.PlaceTrap();
            ChangeState();
        }
    }
    #endregion

    #region PROTECTED
    protected override void OnNorth(InputAction.CallbackContext context)
    {
        base.OnNorth(context);
        northIsDown = context.started || context.performed;
    }

    protected override void OnLeftDirections(InputAction.CallbackContext context)
    {
        base.OnLeftDirections(context);
        if (context.phase == InputActionPhase.Waiting) return;
        if (playerController.MovementInputLocked) return;

        Vector2 value = context.ReadValue<Vector2>();
        playerController.InputMovement = value;

        if (value != Vector2.zero && canCancelWithMovement)
        {
            playerController.StopPlacingTrap();
            ChangeState();
        }
        else if (value == Vector2.zero)
        {
            canCancelWithMovement = true;
        }
    }

    protected override void OnTakenDamage(int currentHealth)
    {
        base.OnTakenDamage(currentHealth);

        playerController.StopPlacingTrap();
        ChangeState();
    }
    #endregion

    #region PRIVATE
    private void ChangeState()
    {
        playerController.DisableTrapPlacementProgressBar();
        playerController.LockTrapPlacementRotation(false);
        playerController.LockPlayerMovement(false);

        if (playerController.InputMovement != Vector2.zero)
        {
            if (northIsDown)
            {
                fsm.ChangeState(fsm.TiptoeState);
            }
            else
            {
                fsm.ChangeState(fsm.WalkState);
            }
        }
        else
        {
            fsm.ChangeState(fsm.IdleState);
        }
    }
    #endregion
}