using UnityEngine;
using UnityEngine.InputSystem;
using Apotropaic.Data;
using Apotropaic.Events;

public abstract class PlayerBaseState : FSMBaseState<PlayerFSM>
{
    protected PlayerController playerController { get; private set; }
    protected PlayerAnimator playerAnimator { get; private set; }
    protected PlayerSettings playerSettings { get; private set; }
    protected Transform transform = null;

    #region PUBLIC
    public override void Initialize(GameObject _owner, FSM _fsm)
    {
        base.Initialize(_owner, _fsm);
        playerController = _owner.GetComponent<PlayerController>();
        playerAnimator = _owner.GetComponent<PlayerAnimator>();
        playerSettings = playerController.Settings;
        transform = _owner.transform;
    }

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        SubscribeToInputEvents();
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
        UnsubscribeFromInputEvents();
    }
    #endregion

    #region PROTECTED
    protected virtual void OnRightDirections(InputAction.CallbackContext context) { }
    protected virtual void OnLeftDirections(InputAction.CallbackContext context) { }
    protected virtual void OnSouth(InputAction.CallbackContext context) { }
    protected virtual void OnEast(InputAction.CallbackContext context) { }
    protected virtual void OnWest(InputAction.CallbackContext context) { }
    protected virtual void OnNorth(InputAction.CallbackContext context) { }
    protected virtual void OnRightShoulder(InputAction.CallbackContext context) { }
    protected virtual void OnR2(InputAction.CallbackContext context) { }
    protected virtual void OnStart(InputAction.CallbackContext context) { }
    protected virtual void OnSelect(InputAction.CallbackContext context) { }
    protected virtual void OnTakenDamage(int currentHealth) { }
    #endregion

    #region PRIVATE
    private void SubscribeToInputEvents()
    {
        playerController.OnLeftDirections += OnLeftDirections;
        playerController.OnRightDirections += OnRightDirections;
        playerController.OnActionS += OnSouth;
        playerController.OnActionE += OnEast;
        playerController.OnActionW += OnWest;
        playerController.OnActionN += OnNorth;
        playerController.OnActionR1 += OnRightShoulder;
        playerController.OnAction_R2 += OnR2;
        playerController.OnAction_Start += OnStart;
        playerController.OnAction_Select += OnSelect;

        GameplayEvents.OnPlayerTakenDamage += OnTakenDamage;
    }

    private void UnsubscribeFromInputEvents()
    {
        playerController.OnLeftDirections -= OnLeftDirections;
        playerController.OnRightDirections -= OnRightDirections;
        playerController.OnActionS -= OnSouth;
        playerController.OnActionE -= OnEast;
        playerController.OnActionW -= OnWest;
        playerController.OnActionN -= OnNorth;
        playerController.OnActionR1 -= OnRightShoulder;
        playerController.OnAction_R2 -= OnR2;
        playerController.OnAction_Start -= OnStart;
        playerController.OnAction_Select -= OnSelect;

        GameplayEvents.OnPlayerTakenDamage -= OnTakenDamage;
    }
    #endregion
}
