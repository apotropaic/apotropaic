using UnityEngine;

public interface IInventoryTrapAccessor
{
    public bool AddTrap(int ID, int _currency, int _quantity);

    public int GetCurrency();

    public void AddCurrency(int _amount);

    public void TakeCurrency(int _chargeAmount);

    public bool RemoveTrap(int ID);
}
