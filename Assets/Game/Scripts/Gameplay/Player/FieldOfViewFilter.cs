using UnityEngine;

public class FieldOfViewFilter : MonoBehaviour
{
    [SerializeField] private float fov;
    [SerializeField] private float numberOfRays;
    [SerializeField] private LayerMask layerMask;
    [SerializeField] private float circleDistance;
    [SerializeField] private float coneDistance;
    [SerializeField] private Transform lookTarget;
    
    private float angleIncrement;
    private float angle = 0f;
    #region UNITY
    private void Start()
    {
        angleIncrement = fov / numberOfRays;
    }
    
    private void Update()
    {
        ConeOfVision();
    }
    #endregion

    #region PUBLIC
    #endregion

    #region PRIVATE
    private void ConeOfVision()
    {
        Vector3 lookDir = (lookTarget.position - transform.position).normalized;
        angle = Mathf.Atan2(lookDir.x, -lookDir.z) * Mathf.Rad2Deg - fov / 2.0f;
        float startAngle = angle;

        // Casting Rays in a cone in front of the player for a set number of rays. numberOFRays can be reduced or increased to balance performance and precision

        for (int i = 0; i < numberOfRays; i++)
        {
            Vector3 RayOutDir = new Vector3(Mathf.Cos(angle * (Mathf.PI / 180f)), 0.5f / coneDistance, Mathf.Sin(angle * Mathf.PI / 180f)).normalized;
            RaycastHit raycastHit;

            Physics.Raycast(new Vector3(transform.position.x, 0.5f, transform.position.z),
                            RayOutDir,
                            out raycastHit,
                            coneDistance,
                            layerMask);

            // Display enemy if detected
            if (raycastHit.collider != null)
            {
                if (raycastHit.collider.tag == "Enemy")
                {
                    raycastHit.collider.GetComponentInChildren<EnemyTransparencyController>().FadeIn();
                }
            }
            angle -= angleIncrement;
        }


        // casting shorter rays on other angles outside the cone to detect enemies in a radius. number of rays can change the density of rays cast as above.
        while (angle > (startAngle - 360))
        {

            Vector3 RayOutDir = new Vector3(Mathf.Cos(angle * (Mathf.PI / 180f)), 0.5f / circleDistance, Mathf.Sin(angle * Mathf.PI / 180f)).normalized;
            RaycastHit raycastHit;


            Physics.Raycast(new Vector3(transform.position.x, 0.5f, transform.position.z),
                            RayOutDir,
                            out raycastHit,
                            circleDistance,
                            layerMask);

            if (raycastHit.collider != null)
            {
                if (raycastHit.collider.tag == "Enemy")
                {
                    raycastHit.collider.GetComponentInChildren<EnemyTransparencyController>().FadeIn();
                }
            }
            angle -= angleIncrement;
        }
    }

    #endregion
}
