using UnityEngine;
using Apotropaic.Data;

public class CameraController : MonoBehaviour
{
    [SerializeField] private CameraSettings settings = null;
    [SerializeField] private PlayerData playerData;
    [SerializeField] private Transform upperRightCorner = null;
    [SerializeField] private Transform lowerLeftCorner = null;

    private float height;
    private float angle;
    private float distance;
    private float snapSpeed;
    private float size;

    private new Transform transform;
    private new Camera camera = null;

    // Cache
    private Transform cameraTargetTransform = null;

    #region UNITY
    private void Awake()
    {
        camera = GetComponent<Camera>();

        LoadSettings();

    }

    private void Start()
    {
        transform = gameObject.GetComponent<Transform>();
        transform.rotation = Quaternion.Euler(new Vector3(angle, 0, 0));
        cameraTargetTransform = playerData.CameraTargetTransform;


    }

    private void FixedUpdate()
    {
        Vector3 targetPos = cameraTargetTransform.position;

        targetPos.x = Mathf.Clamp(targetPos.x, lowerLeftCorner.position.x, upperRightCorner.position.x);
        targetPos.z = Mathf.Clamp(targetPos.z, lowerLeftCorner.position.z, upperRightCorner.position.z);

        transform.position = Vector3.Lerp(transform.position, new Vector3(targetPos.x, height, (targetPos.z - distance)), Time.fixedDeltaTime * snapSpeed);
    }

    #endregion

    #region PUBLIC


    #endregion

    #region PRIVATE

    private void LoadSettings()
    {
        height = settings.Height;
        angle = settings.Angle;
        distance = settings.Distance;
        snapSpeed = settings.SnapSpeed;

        camera.orthographicSize = settings.Size;

    }
    #endregion
}
