using UnityEngine;
using Apotropaic.Data;

public class InventoryManager : MonoBehaviour, IInventoryTrapAccessor
{
    [SerializeField] PlayerInventory inventoryData;
    [SerializeField] GameplayData gameplayData;
    [SerializeField] TrapSettingsList trapSettingsList;

    #region UNITY
    private void Start()
    {
        if(gameplayData.InfiniteTraps == true)
        {
            inventoryData.Slots = 4;
            inventoryData.MaximumQuantity = 1000;
            inventoryData.Currency = 10000;

            foreach(BasicTrapSettings trapSettings in trapSettingsList.Traps)
            {
                trapSettings.Unlocked = true;

                if (inventoryData.TrapList.ContainsKey(trapSettings.UID))
                {
                    inventoryData.TrapList[trapSettings.UID] = 1000;
                    continue;
                }
                inventoryData.TrapList.Add(trapSettings.UID, 1000);
            }
        }
    }
    #endregion

    #region PUBLIC
    // Add a trap to the inventory and take the currency
    public bool AddTrap(int ID, int _currency, int _quantity)
    {
        if(_currency <= inventoryData.Currency)
        {
            if(inventoryData.TrapList.ContainsKey(ID))
            {
                if((inventoryData.TrapList[ID] + _quantity) <= inventoryData.MaximumQuantity)
                {
                    inventoryData.TrapList[ID] = inventoryData.TrapList[ID] + _quantity;
                    inventoryData.Currency = inventoryData.Currency - _currency;
                    return true;
                }
            }
            else if(inventoryData.TrapList.Count < inventoryData.Slots)
            {
                if(_quantity < inventoryData.MaximumQuantity)
                {
                    inventoryData.TrapList.Add(ID, _quantity);
                    inventoryData.Currency = inventoryData.Currency - _currency;
                    return true;
                }
            }
        }
        return false;
    }


    public int GetCurrency()
    {
        return inventoryData.Currency;
    }


    public void TakeCurrency(int _chargeAmount)
    {
        inventoryData.Currency = inventoryData.Currency - _chargeAmount;
    }

    // removes the trap from the inventory after placing it
    public bool RemoveTrap(int ID)
    {
        if(inventoryData.TrapList.ContainsKey(ID))
        {
            if(inventoryData.TrapList[ID] == 1)
            {
                inventoryData.TrapList.Remove(ID);
                return true;
            }
            else if(inventoryData.TrapList[ID] > 1)
            {
                inventoryData.TrapList[ID] -= 1;
                return true;
            }
        }
        return false;
    }

    public void AddCurrency(int _currency)
    {
        inventoryData.Currency = inventoryData.Currency + _currency;
    }
    #endregion

    #region PRIVATE

    #endregion
}
