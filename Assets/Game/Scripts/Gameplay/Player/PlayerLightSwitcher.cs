using UnityEngine;

public class PlayerLightSwitcher : MonoBehaviour
{
    [SerializeField] Light lightComponent;
    [SerializeField] GameplayData gameplayData;
    #region UNITY
    private void Start()
    {
        lightComponent = GetComponent<Light>();
    }

    private void Update()
    {
        if (gameplayData.TimeOfDay != TimeOfDay.Night && lightComponent.enabled)
            lightComponent.enabled = false;
        else if (gameplayData.TimeOfDay == TimeOfDay.Night && !lightComponent.enabled)
            lightComponent.enabled = true;
    }
    #endregion

    #region PUBLIC

    #endregion

    #region PRIVATE
    #endregion
}
