using UnityEngine;
using Apotropaic.Data;
using System.Collections.Generic;
using System.Linq;

public class TrapManager : MonoBehaviour
{
    private BasicTrapSettings[] traps = null;
    [SerializeField] TrapSettingsList trapsList = null;
    [SerializeField] NPCData[] nPCDatas;
    #region UNITY

    private void Awake()
    {
        Initialize();
    }
    private void Start()
    {
        
    }
    #endregion

    #region PUBLIC
    // TODO: Remove, useless now
    public BasicTrapSettings GetTrap(int ID)
    {
        for (int i = 0; i < traps.Length; i++)
        {
            if (traps[i].UID == ID)
                return traps[i];
        }
        return null;
    }

    #endregion

    #region PRIVATE
    // Caches all traps into a scriptable object acessible to scripts that need it.
    private void Initialize()
    {
        traps = Resources.LoadAll<BasicTrapSettings>("Trap Settings");
        trapsList.Traps = traps;
    }
    #endregion
}