using Apotropaic.Data;
using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class Trap<T, U> : MonoBehaviour, ITrap where T : TrapSettings<U> where U : TrapPreset
{
    [Header("Settings")]
    [SerializeField] protected T settings = null;

    // Settings
    private string trapName = "";
    private TrapType trapType = TrapType.Physical;
    private TrapDetection trapDetection = TrapDetection.Collision;
    private TrapLifespan lifespan = TrapLifespan.Permanent;
    private int usesCount = 0;
    private float lifespanTime = 0f;
    private int damage = 0;
    private List<RepulsionOdds> repulsionOdds = null;
    private bool lockTrapOrientation = true;
    private float enemyDelay = 0f;
    private float placementTime = 0f;

    private float timePassed = 0f;

    [Space(20)]
    [SerializeField] private TrapOverlapDetector overlapDetector = null;
    [SerializeField] private Collider enemyCollider = null;
    [SerializeField] private SoundPlayer soundPlayer = null;
    [SerializeField] private MeshRendererAnimator rendererAnimator = null;

    private int usesLeft = 0;

    private TrapState currentState = TrapState.Placement;

    #region UNITY
    private void Awake()
    {
        LoadSettings();
        Initialize();
    }

    private void OnEnable()
    {
        ResetTrap();
        RefreshTrap();
    }

    private void Update()
    {
        if (lifespan == TrapLifespan.Time && currentState == TrapState.Active)
        {
            timePassed += Time.deltaTime;

            if (rendererAnimator != null)
            {
                float delta = Mathf.Clamp(timePassed / lifespanTime, 0f, 1f);
                rendererAnimator.SetFrame(delta);
            }

            if (timePassed > lifespanTime)
            {
                DestroyTrap();
            }
        }
    }
    #endregion

    #region PUBLIC
    public void UseTrap()
    {
        if (lifespan == TrapLifespan.MultipleUses)
            usesLeft = Mathf.Clamp(usesLeft - 1, 0, int.MaxValue);

        if (rendererAnimator != null)
            rendererAnimator.SetFrame((float)usesLeft / usesCount);
    }

    public void DestroyTrap()
    {
        Destroy(gameObject);
    }

    public void SetCurrentState(TrapState state)
    {
        currentState = state;
        RefreshTrap();
    }

    public TrapOverlapDetector OverlapDetector { get { return overlapDetector; } }
    public float PlacementTime { get { return placementTime; } }
    public TrapLifespan Lifespan { get { return lifespan; } }
    public TrapDetection TrapDetection { get { return trapDetection; } }
    public int Damage { get { return damage; } }
    public float EnemyDelay { get { return enemyDelay; } }
    public List<RepulsionOdds> RepulsionOdds { get { return repulsionOdds; } }
    public bool LockTrapOrientation { get { return lockTrapOrientation; } }
    public int UsesLeft { get { return usesLeft; } }
    #endregion

    #region PROTECTED
    protected virtual void LoadSettings()
    {
        trapName = settings.Name;
        trapType = settings.Type;
        trapDetection = settings.TrapDetection;
        lifespan = settings.Lifespan;
        usesCount = settings.UsesCount;
        usesLeft = usesCount;
        lifespanTime = settings.LifespanTime;
        damage = settings.Damage;
        repulsionOdds = settings.RepulsionOdds;
        lockTrapOrientation = settings.LockTrapOrientation;
        enemyDelay = settings.EnemyDelay;
        placementTime = settings.PlacementTime;
    }
    #endregion

    #region PRIVATE
    // TODO : Refactor
    private void Initialize()
    {
        if (soundPlayer == null) return;
        ISoundSystem soundSystem = FindObjectOfType<SoundSystem>();
        soundPlayer.Initialize(soundSystem);
    }

    private void ResetTrap()
    {
        usesLeft = usesCount;
        timePassed = 0f;
    }

    private void RefreshTrap()
    {
        if (currentState == TrapState.Placement)
        {
            overlapDetector.gameObject.SetActive(true);

            if (enemyCollider != null)
                enemyCollider.enabled = false;
        }
        else if (currentState == TrapState.Active)
        {
            overlapDetector.gameObject.SetActive(false);

            if (enemyCollider != null)
                enemyCollider.enabled = true;

            if (soundPlayer != null)
            {
                soundPlayer.AttemptPlaySound();
            }
        }
    }
    #endregion
}