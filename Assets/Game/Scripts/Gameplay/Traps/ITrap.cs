using UnityEngine;

public interface ITrap
{
    Transform transform { get; }
    GameObject gameObject { get; }
    TrapOverlapDetector OverlapDetector { get; }
    void SetCurrentState(TrapState state);
    float PlacementTime { get; }
}