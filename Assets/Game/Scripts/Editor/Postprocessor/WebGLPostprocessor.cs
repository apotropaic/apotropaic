using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;

public class WebGLPostprocessor
{
    const string configPath = "/Game/Scripts/BuildConfigurations/web.config";

    [PostProcessBuildAttribute(1)]
    public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
    {
        if (target != BuildTarget.WebGL) return;

        string config = Application.dataPath + configPath;
        FileUtil.CopyFileOrDirectory(config, pathToBuiltProject + "/web.config");
    }
}