using UnityEngine;

public class SpriteAnimator : SpriteAnimatorBase<SpriteRenderer>
{
    #region PROTECTED
    protected override void SetRendererSprite(Sprite sprite)
    {
        renderer.sprite = sprite;
    }
    #endregion
}