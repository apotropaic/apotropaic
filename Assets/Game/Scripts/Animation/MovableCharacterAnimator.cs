using UnityEngine;

public class MovableCharacterAnimator<T> : CharacterAnimatorBase<T> where T : MovableCharacterAnimationsPackage
{
    #region PUBLIC
    public void PlayIdleBack(float speedMultiplier = 1f)
    {
        PlayAnimation(characterAnimationsPackage.IdleBackAnimation, speedMultiplier);
    }

    public void PlayWalkRight(float speedMultiplier = 1f)
    {
        PlayAnimation(characterAnimationsPackage.WalkRightAnimation, speedMultiplier);
    }

    public void PlayWalkFront(float speedMultiplier = 1f)
    {
        PlayAnimation(characterAnimationsPackage.WalkFrontAnimation, speedMultiplier);
    }

    public void PlayWalkBack(float speedMultiplier = 1f)
    {
        PlayAnimation(characterAnimationsPackage.WalkBackAnimation, speedMultiplier);
    }
    #endregion
}