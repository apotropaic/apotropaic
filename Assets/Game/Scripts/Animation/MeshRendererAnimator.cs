using UnityEngine;

public class MeshRendererAnimator : SpriteAnimatorBase<MeshRenderer>
{
    #region PROTECTED
    protected override void SetRendererSprite(Sprite sprite)
    {
        renderer.material.mainTexture = sprite.texture;
    }
    #endregion
}