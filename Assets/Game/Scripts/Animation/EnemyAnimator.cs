using UnityEngine;

public class EnemyAnimator : MovableCharacterAnimator<EnemyAnimationsPackage>
{
    #region PUBLIC
    public void PlayDeath(float speedMultiplier = 1f)
    {
        PlayAnimation(characterAnimationsPackage.DeathAnimation, speedMultiplier);
    }

    public void PlayAttack(float speedMultiplier = 1f)
    {
        PlayAnimation(characterAnimationsPackage.AttackAnimation, speedMultiplier);
    }
    #endregion
}
