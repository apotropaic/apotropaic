using System;
using UnityEngine;

[RequireComponent(typeof(MeshRendererAnimator))]
public class CharacterAnimatorBase<T> : MonoBehaviour where T : CharacterAnimationsPackage
{
    [SerializeField] protected T characterAnimationsPackage = null;
    public Action OnAnimationPlaybackComplete = null;

    private MeshRendererAnimator rendererAnimator = null;

    #region UNITY
    private void Awake()
    {
        rendererAnimator = GetComponent<MeshRendererAnimator>();
        rendererAnimator.OnPlaybackComplete += OnPlaybackComplete;
    }

    private void OnDestroy()
    {
        if (rendererAnimator == null) return;

        rendererAnimator.OnPlaybackComplete -= OnPlaybackComplete;
    }
    #endregion

    #region PUBLIC
    public void PlayIdle(float speedMultiplier = 1f)
    {
        PlayAnimation(characterAnimationsPackage.IdleAnimation, speedMultiplier);
    }
    #endregion

    #region PROTECTED
    protected void PlayAnimation(SpriteAnimation animation, float speedMultiplier)
    {
        rendererAnimator.Play(animation);
        rendererAnimator.RefreshAnimationSpeed(animation.AnimationSpeed * speedMultiplier);
    }

    protected virtual void OnPlaybackComplete()
    {
        OnAnimationPlaybackComplete?.Invoke();
    }
    #endregion
}