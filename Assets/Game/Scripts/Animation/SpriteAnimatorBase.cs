using UnityEngine;
using Apotropaic.Utility;
using EasyButtons;
using Apotropaic.Data;
using System;

public abstract class SpriteAnimatorBase<T> : MonoBehaviour where T : Renderer
{
    public Action OnPlaybackComplete = null;

    [Header("Settings")]
    [SerializeField] private AnimationSettings animationSettings = null;

    // Settings
    private int frameRate = 24;

    [Space(20)]
    [SerializeField] protected new T renderer = default;

    [SerializeField] private Sprite[] frames = null;
    [SerializeField] private int initialFrameIndex = 0;
    [SerializeField] private bool loop = true;
    [SerializeField] private bool resetOnEnable = false;
    [SerializeField] private bool playOnAwake = true;
    [SerializeField] private float animationSpeed = 1f;

    private int currentFrameIndex = 0;
    private float previousFrameTime = 0f;
    private float nextFrameTime = 0f;
    private float currentAnimationSpeed = 1f;

    private float currentTimeScale = 0f;
    private int currentFrameRate = 24;

    private int currentPlaybackHashCode = 0;

    private float pausedTime = 0f;
    private bool reverseAnimation = false;

    private bool isPaused = false;
    private bool isStopped = true;

    #region UNITY
    private void Awake()
    {
        if (renderer == null)
            renderer = GetComponent<T>();

        if (Assert.Validate(renderer != null, "No SpriteRenderer component was found.")) return;

        LoadSettings();

        if (playOnAwake)
        {
            Play(currentFrameIndex);
        }
    }

    private void OnEnable()
    {
        if (resetOnEnable)
        {
            InitializeMembers();
        }
        else
        {
            ShiftTime();
        }
    }

    private void OnDisable()
    {
        pausedTime = Time.time;
    }

    private void Update()
    {
        if (Time.timeScale == 0 || (!loop && currentFrameIndex == frames.Length - 1) || isPaused || isStopped || animationSpeed == 0) return;

        MonitorAnimationSpeed();
        RefreshNextFrame();
        UpdateFrames();
    }
    #endregion

    #region PUBLIC
    // TODO : Remove debugging button attributes.
    [Button]
    public void Play()
    {
        Play(initialFrameIndex);
    }

    public void Play(float unitInterval)
    {
        int frameIndex = (int)(Mathf.Lerp(0f, frames.Length - 1, unitInterval));
        Play(frameIndex);
    }

    public void Play(SpriteAnimation animation)
    {
        if (currentPlaybackHashCode == animation.NameHashCode) return;

        if (!animation.UseDefaultFrameRate)
        {
            currentFrameRate = animation.FrameRate;
        }

        loop = animation.Loop;

        frames = animation.Frames;
        Play(initialFrameIndex);

        currentPlaybackHashCode = animation.NameHashCode;

        RefreshAnimationSpeed(animation.AnimationSpeed);
    }

    public void Play(int frameIndex)
    {
        InitializeMembers(frameIndex);
    }

    [Button]
    public void Pause()
    {
        if (isPaused || isStopped || Time.timeScale == 0) return;

        isPaused = true;
        pausedTime = Time.time;
    }

    [Button]
    public void Resume()
    {
        if (!isPaused) return;

        ShiftTime();
        isPaused = false;
    }

    [Button]
    public void Stop()
    {
        if (isStopped || Time.timeScale == 0) return;

        isStopped = true;
    }

    public void SetFrame(float unitInterval)
    {
        int frameIndex = (int)(Mathf.Lerp(0f, frames.Length - 1, unitInterval));
        SetRendererSprite(frames[frameIndex]);
    }

    public void RefreshAnimationSpeed(float speed)
    {
        reverseAnimation = speed < 0;
        animationSpeed = speed;
        currentAnimationSpeed = animationSpeed;
    }
    #endregion

    #region PROTECTED
    protected abstract void SetRendererSprite(Sprite sprite);
    #endregion

    #region PRIVATE
    private void LoadSettings()
    {
        frameRate = animationSettings.DefaultFrameRate;
        currentFrameRate = frameRate;
    }

    private void InitializeMembers(int? initialFrameIndexOverride = null)
    {
        currentFrameIndex = (initialFrameIndexOverride == null) ? initialFrameIndex : initialFrameIndexOverride.Value;
        SetRendererSprite(frames[currentFrameIndex]);
        nextFrameTime = Time.time;
        IncrementNextFrame();

        currentTimeScale = Time.timeScale;
        currentFrameRate = frameRate;

        isPaused = false;
        isStopped = false;

        currentAnimationSpeed = animationSpeed;
        RefreshAnimationSpeed(currentAnimationSpeed);
    }

    private float FrameTime { get { return (((60f / frameRate) * (1f / 60f)) * Time.timeScale); } }

    private void UpdateFrames()
    {
        if (Time.time >= nextFrameTime)
        {
            IncrementNextFrame();

            if (!reverseAnimation)
            {
                UpdateFrame(0, frames.Length - 1, 1);
            }
            else
            {
                UpdateFrame(frames.Length - 1, 0, -1);
            }


            SetRendererSprite(frames[currentFrameIndex]);
        }
    }

    private void UpdateFrame(int firstFrameIndex, int lastFrameIndex, int increment)
    {
        if (currentFrameIndex != lastFrameIndex)
        {
            currentFrameIndex += increment;

            if (currentFrameIndex == lastFrameIndex && !loop)
            {
                if (OnPlaybackComplete != null)
                    OnPlaybackComplete();

                isStopped = true;
            }
        }
        else if (loop)
        {
            currentFrameIndex = firstFrameIndex;
        }
    }

    private void IncrementNextFrame()
    {
        previousFrameTime = nextFrameTime;
        nextFrameTime = previousFrameTime + (FrameTime / Mathf.Abs(animationSpeed));
    }

    private void RefreshNextFrame()
    {
        if (frameRate == currentFrameRate && Time.timeScale == currentTimeScale) return;

        nextFrameTime = previousFrameTime + (FrameTime / Mathf.Abs(animationSpeed));

        currentFrameRate = frameRate;
        currentTimeScale = Time.timeScale;
    }

    private void ShiftTime()
    {
        float difference = Time.time - pausedTime;
        previousFrameTime = difference;
        nextFrameTime += difference;
    }

    private void MonitorAnimationSpeed()
    {
        if (animationSpeed != currentAnimationSpeed)
            RefreshAnimationSpeed(animationSpeed);
    }
    #endregion
}