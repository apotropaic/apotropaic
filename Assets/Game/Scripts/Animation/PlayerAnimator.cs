using UnityEngine;

public class PlayerAnimator : MovableCharacterAnimator<PlayerAnimationsPackage>
{
    #region PUBLIC
    public void PlayTiptoe(float speedMultiplier = 1f)
    {
        PlayAnimation(characterAnimationsPackage.TiptoeAnimation, speedMultiplier);
    }

    public void PlayPlacingTrap(float speedMultiplier = 1f)
    {
        PlayAnimation(characterAnimationsPackage.PlacingTrapAnimation, speedMultiplier);
    }
    #endregion
}