using UnityEngine;

public class LinkButton : MonoBehaviour
{
    [SerializeField] private string url = "";

    #region PUBLIC
    public void OpenLink()
    {
        Application.OpenURL(url);
    }
    #endregion
}
