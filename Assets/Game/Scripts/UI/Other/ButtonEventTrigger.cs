using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class ButtonEventTrigger : MonoBehaviour
{
    [SerializeField] private UnityEvent buttonCallback = null;

    #region PUBLIC
    public void OnButtonDown(InputAction.CallbackContext context)
    {
        if (context.phase != InputActionPhase.Started) return;
        if (!gameObject.activeInHierarchy) return;
        buttonCallback?.Invoke();
    }
    #endregion
}