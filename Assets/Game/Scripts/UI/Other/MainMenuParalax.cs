using UnityEngine;
using UnityEngine.InputSystem;

namespace Apotropaic.UI
{
    public class MainMenuParalax : MonoBehaviour
    {
        [SerializeField] private Transform layer = null;
        [SerializeField] private Transform lowerLeftCorner = null;
        [SerializeField] private Transform upperRightCorner = null;

        #region UNITY
        private void Update()
        {
            UpdateLayersPositions();
        }
        #endregion

        #region PRIVATE
        private void UpdateLayersPositions()
        {
            Vector2 mousePos = Mouse.current.position.ReadValue();
            float x = mousePos.x / Screen.width;
            float y = mousePos.y / Screen.height;

            float newX = Mathf.Lerp(lowerLeftCorner.position.x, upperRightCorner.position.x, x);
            float newY = Mathf.Lerp(lowerLeftCorner.position.y, upperRightCorner.position.y, y);

            layer.position = new Vector2(newX, newY);
        }
        #endregion
    }
}