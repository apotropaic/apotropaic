using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PointerSelectionManager : PointerInputModule
{

    [SerializeField] private Button startButton;

    //GameObject cachedSelection = null;
    //int flag = 0;
    public override void Process()
    {
        throw new System.NotImplementedException();
    }

    public GameObject PointedObject()
    {
        PointerEventData data;
        GetPointerData(kMouseLeftId, out data, false);
        if (data != null)
        {
            return data.pointerCurrentRaycast.gameObject;
        }
        return null;
    }

    protected override void Start()
    {
        base.Start();
        //flag = 0;
        eventSystem.SetSelectedGameObject(startButton.gameObject);
    }
    /*
    private void Update()
    {
        if (eventSystem.IsPointerOverGameObject())
        {
            if (PointedObject().GetComponent<Button>() != null)
            {
                cachedSelection = PointedObject();
                eventSystem.SetSelectedGameObject(null);
                flag = 0;
            }
            else
            {
                if (flag == 0)
                {
                    eventSystem.SetSelectedGameObject(cachedSelection);
                    flag = 1;
                }
            }

        }

    }
    */
}
