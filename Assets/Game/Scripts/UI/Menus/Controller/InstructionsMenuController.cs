using Apotropaic.Events;
using UnityEngine;

namespace Apotropaic.UI
{
    public class InstructionsMenuController : Menu
    {
        private IPauseEventsInvoker pauseEventsInvoker = null;

        #region UNITY
        #endregion

        #region PUBLIC
        public override void Initialize(IMenuManager manager)
        {
            base.Initialize(manager);
            pauseEventsInvoker = GetComponent<IPauseEventsInvoker>();
        }
        #endregion

        #region PUBLIC
        public void Continue()
        {
            Time.timeScale = 1f;
            pauseEventsInvoker.Resume();
            MenuManager.HideMenu(menuClassifier);
        }
        #endregion

        #region PRIVATE
        #endregion
    }
}