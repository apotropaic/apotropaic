using UnityEngine;
using Apotropaic.Data;
using UnityEngine.UI;
using Project.Build.Commands;
using UnityEngine.EventSystems;

namespace Apotropaic.UI
{
    public class MainMenuController : Menu
    {
        [SerializeField] private SceneLoaderTemp sceneLoader = null;

        [SerializeField] private GameplayData gameplayData = null;
        [SerializeField] private ControlsMenuController controlsMenuController = null;
        [SerializeField] private MenuClassifier controlsMenuClassifier = null;
        [SerializeField] private SceneSettings sceneSettings = null;
        [SerializeField] private BuildData buildData = null;

        [SerializeField] private GameObject settingsPanel = null;
        [SerializeField] private GameObject artGalleryPanel = null;
        [SerializeField] private GameObject creditsPanel = null;
        [SerializeField] private GameObject audioPanel = null;
        [SerializeField] private GameObject hacksPanel = null;
        [SerializeField] private GameObject dataPanel = null;

        // Hacks
        [SerializeField] private Toggle skipTutorialToggle = null;
        [SerializeField] private Toggle invincibilityToggle = null;
        [SerializeField] private Toggle infiniteTrapsToggle = null;

        [SerializeField] private Text buildNoText = null;

        [SerializeField] private Button startButton = null;
        [SerializeField] private EventSystem eventSystem = null;

        #region UNITY
        private void Start()
        {
            buildNoText.text = "Ver." + buildData.buildNumber;
            InitializeHacks();
        }


        #endregion

        #region PUBLIC
        public override void OnShowMenu(string options = "")
        {
            base.OnShowMenu(options);
            eventSystem.SetSelectedGameObject(startButton.gameObject);
        }
        public void LoadOfficesScene()
        {
            MenuManager.HideMenu(menuClassifier);

            if (gameplayData.FinishedNightTutorial == false)
            {
                sceneLoader.LoadScene(sceneSettings.LibraryScene);
            }
            else
            {
                sceneLoader.LoadScene(sceneSettings.OfficesScene);
            }
        }
        public void SetSelectedButton(Button _button)
        {
            eventSystem.SetSelectedGameObject(_button.gameObject);
        }
        public void OpenControls()
        {
            controlsMenuController.SetPreviousMenuClassifier(menuClassifier);
            MenuManager.ShowMenu(controlsMenuClassifier);
        }

        #region PANELS

        public void OpenSettings(bool open)
        {
            settingsPanel.SetActive(open);
        }

        public void OpenArtGallery(bool open)
        {
            artGalleryPanel.SetActive(open);
        }

        public void OpenCredits(bool open)
        {
            creditsPanel.SetActive(open);
        }

        public void OpenAudio(bool open)
        {
            audioPanel.SetActive(open);
        }

        public void OpenHacks(bool open)
        {
            hacksPanel.SetActive(open);
        }

        public void OpenData(bool open)
        {
            dataPanel.SetActive(open);
        }

        #endregion

        public void QuitGame()
        {
            Application.Quit();
        }

        public void OnClickSkipTutorial(bool finishedTutorial)
        {
            gameplayData.FinishedNightTutorial = finishedTutorial;
        }

        public void OnClickInvincibility(bool invincible)
        {
            gameplayData.Invincibility = invincible;
        }

        public void OnClickInfiniteTraps(bool infiniteTraps)
        {
            gameplayData.InfiniteTraps = infiniteTraps;
        }
        #endregion

        #region PRIVATE
        private void InitializeHacks()
        {
            skipTutorialToggle.isOn = gameplayData.FinishedNightTutorial;
            invincibilityToggle.isOn = gameplayData.Invincibility;
            infiniteTrapsToggle.isOn = gameplayData.InfiniteTraps;
        }
        #endregion
    }
}
