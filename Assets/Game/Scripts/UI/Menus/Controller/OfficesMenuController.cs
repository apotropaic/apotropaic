using Apotropaic.Events;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Apotropaic.UI
{
    public class OfficesMenuController : Menu
    {
        [Space(20)]
        [SerializeField] private MenuClassifier pauseMenuClassifier = null;
        [SerializeField] private PauseMenuController pauseMenu;
        [SerializeField] private MenuClassifier shopMenuClassifier = null;
        [SerializeField] private MenuClassifier dialogMenuClassifier = null;

        [Space(20)]
        [SerializeField] private Text convText = null;
        [SerializeField] private GameplayData gameplayData = null;

        private IPauseEventsInvoker pauseEventsInvoker = null;


        // [SerializeField] private Button startButton = null;
        // [SerializeField] private EventSystem eventSystem = null;

        #region UNITY
        #endregion

        #region PUBLIC
        public override void Initialize(IMenuManager manager)
        {
            base.Initialize(manager);
            pauseEventsInvoker = GetComponent<IPauseEventsInvoker>();
            pauseMenu.SetPreviousMenuClassifier(menuClassifier);
        }

        public override void OnShowMenu(string options = "")
        {
            base.OnShowMenu(options);

            UIEvents.OnOpenedShop += OnShopOpen;
            UIEvents.OnConversationStarted += OnConversationOpen;

            convText.text = (gameplayData.ConversationsPerDay - gameplayData.ConversationsThisDay).ToString();

        }

        public override void OnHideMenu(string options = "")
        {
            base.OnHideMenu(options);

            UIEvents.OnOpenedShop -= OnShopOpen;
            UIEvents.OnConversationStarted -= OnConversationOpen;
        }

        public void Pause()
        {
            MenuManager.HideMenu(menuClassifier);
            MenuManager.ShowMenu(pauseMenuClassifier);
            Time.timeScale = 0f;
            pauseEventsInvoker.Pause();
        }
        #endregion

        #region PRIVATE
        private void OnShopOpen()
        {
            MenuManager.ShowMenu(shopMenuClassifier);
            MenuManager.HideMenu(menuClassifier);
        }

        private void OnConversationOpen(List<ConversationData> openConversations, Sprite _sprite)
        {
            MenuManager.ShowMenu(dialogMenuClassifier);

            UIEvents.OnConversationSet?.Invoke(openConversations, _sprite);

            MenuManager.HideMenu(menuClassifier);
        }

        #endregion
    }
}