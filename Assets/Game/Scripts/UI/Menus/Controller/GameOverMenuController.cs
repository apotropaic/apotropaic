using UnityEngine;
using Apotropaic.Events;
using UnityEngine.UI;
using Apotropaic.Data;

namespace Apotropaic.UI
{
    public class GameOverMenuController : Menu
    {
        [SerializeField] private PlayerData playerData = null;

        [SerializeField] private SceneLoaderTemp sceneLoader = null;
        [SerializeField] private SceneSettings sceneSettings = null;

        [SerializeField] private GameObject conclusionPanel = null;
        [SerializeField] private GameObject murmurPanel = null;

        [SerializeField] private Text gameOverMessage = null;
        [SerializeField] private string winMessage = "";
        [SerializeField] private string loseMessage = "";

        private IPauseEventsInvoker pauseEventsInvoker = null;
        private bool playerWon = false;

        #region UNITY
        #endregion

        #region PUBLIC
        public override void Initialize(IMenuManager manager)
        {
            base.Initialize(manager);
            pauseEventsInvoker = GetComponent<IPauseEventsInvoker>();
        }

        public override void OnShowMenu(string options = "")
        {
            base.OnShowMenu(options);

            bool playerWins = playerData.CurrentHealth > 0;
            SetWinStatus(playerWins);
            murmurPanel.SetActive(false);
            conclusionPanel.SetActive(true);

            Time.timeScale = 0f;
            pauseEventsInvoker.Pause();
        }

        public void Continue()
        {
            if (!playerWon)
            {
                CloseMenuAndResume();
                sceneLoader.LoadScene(sceneSettings.OfficesScene);
            }
            else
            {
                conclusionPanel.SetActive(false);
                murmurPanel.SetActive(true);
            }
        }

        public void ContinueMurmurPanel()
        {
            CloseMenuAndResume();

            if (GameplayEvents.OnMorningPhase != null)
                GameplayEvents.OnMorningPhase();
        }
        #endregion

        #region PRIVATE
        private void SetWinStatus(bool playerWon)
        {
            this.playerWon = playerWon;
            gameOverMessage.text = playerWon ? winMessage : loseMessage;
        }

        private void CloseMenuAndResume()
        {
            MenuManager.HideMenu(menuClassifier);
            Time.timeScale = 1f;
            pauseEventsInvoker.Resume();
        }
        #endregion
    }
}