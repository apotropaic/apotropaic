using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Apotropaic.Events;

namespace Apotropaic.UI
{
    public class DialogMenuController : Menu
    {
        [SerializeField] private MenuClassifier previousMenuClassifier = null;
        [SerializeField] private GameObject choiceListingPrefab;
        [SerializeField] private GameObject contentObject;
        [SerializeField] private GameObject choicesPanel;
        [SerializeField] private GameplayData gameplayData;


        private List<GameObject> listingObjects = new List<GameObject>();

        private List<ConversationData> currentConversationList = null;
        private ConversationData selectedConversation = null;
        private int SentenceCounter = 0;
        private Sprite neutralSprite;


        private bool choiceMade;

        [Space(30)]
        [SerializeField] private Image speakerImageBox;
        [SerializeField] private Text dialogTextBox;
        [SerializeField] private Text dialogNameBox;
        [SerializeField] private Button nextButton;
        [SerializeField] private Image eloiseImage;


        // [SerializeField] private Button startButton = null;
        [SerializeField] private EventSystem eventSystem = null;

        #region UNITY
        public override void Initialize(IMenuManager manager)
        {
            base.Initialize(manager);
            UIEvents.OnSetDialogPreviousMenu += SetPreviousMenu;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            UIEvents.OnSetDialogPreviousMenu -= SetPreviousMenu;
        }

        public override void OnShowMenu(string options = "")
        {
            base.OnShowMenu(options);
            UIEvents.OnConversationSet += OnstartConversation;
            SentenceCounter = 0;
            choiceMade = false;
            nextButton.interactable = true;
            selectedConversation = null;
        }

        public override void OnHideMenu(string options = "")
        {
            UIEvents.OnConversationSet -= OnstartConversation;
            currentConversationList = null;
            base.OnHideMenu(options);
        }

        #endregion

        #region PUBLIC
        // TODO refactor
        public void SetPreviousMenu(MenuClassifier previousMenu)
        {
            previousMenuClassifier = previousMenu;
        }
        #endregion

        #region PRIVATE

        private void OnstartConversation(List<ConversationData> _currentConversationList, Sprite _sprite)
        {
            if (currentConversationList != null)
            {
                currentConversationList.Clear();
            }
            currentConversationList = _currentConversationList;
            neutralSprite = _sprite;

            UpdateDialog();

            SetSprites(_sprite);

        }

        private void OnEndClick()
        {
            SentenceCounter = 0;
            gameplayData.ConversationsThisDay++;

            if (selectedConversation != null && selectedConversation.TrapToUnlock != null)
            {
                selectedConversation.TrapToUnlock.Unlocked = true;
            }

            selectedConversation.Talked = true;
            UIEvents.OnConversationEnded?.Invoke();
            MenuManager.ShowMenu(previousMenuClassifier);
            if (selectedConversation.TransitionToNight == true)
            {
                CoreEvents.OnNightTransition?.Invoke();
                selectedConversation.Talked = false;
            }
            else if (selectedConversation.TransitionToOffice == true)
            {
                CoreEvents.OnOfficeTransition?.Invoke();
                selectedConversation.Talked = false;
            }
            MenuManager.HideMenu(menuClassifier);
        }

        private void UpdateDialog()
        {
            if (choiceMade)
            {
                ShowConversation();
            }
            else
            {
                ShowChoices();
            }
        }

        private void ShowChoices()
        {

            listingObjects.Clear();
            choicesPanel.SetActive(true);
            selectedConversation = null;
            dialogTextBox.enabled = false;
            dialogNameBox.enabled = false;

            for (int i = 0; i < currentConversationList.Count; i++)
            {
                GameObject go = Instantiate(choiceListingPrefab, contentObject.transform);
                go.GetComponent<DialogChoiceSetter>().SetData(currentConversationList[i], choicesPanel, (float)i / (float)currentConversationList.Count);
                go.GetComponent<DialogChoiceSetter>().AddListenerForButton(SetSelectedConversation);
                listingObjects.Add(go);
            }
            /*
            foreach(ConversationData conversationData in currentConversationList)
            {
                GameObject go = Instantiate(choiceListingPrefab, contentObject.transform);
                go.GetComponent<DialogChoiceSetter>().SetData(conversationData, choicesPanel, );
                go.GetComponent<DialogChoiceSetter>().AddListenerForButton(SetSelectedConversation);
                listingObjects.Add(go);
            }
            */
            nextButton.gameObject.SetActive(false);
            eventSystem.SetSelectedGameObject(listingObjects[0]);

            if (currentConversationList.Count == 1)
            {
                SetSelectedConversation(currentConversationList[0]);
            }
        }

        private void SetSelectedConversation(ConversationData _conversationData)
        {
            selectedConversation = _conversationData;
            ClearChoices();
            ShowConversation();
        }

        private void ClearChoices()
        {
            foreach (GameObject go in listingObjects)
            {
                Destroy(go);
            }
            choiceMade = true;
            choicesPanel.SetActive(false);
            dialogTextBox.enabled = true;
            dialogNameBox.enabled = true;
        }


        private void ShowConversation()
        {
            nextButton.gameObject.SetActive(true);
            eventSystem.SetSelectedGameObject(nextButton.gameObject);

            if (SentenceCounter < selectedConversation.Sentences.Length)
            {
                dialogNameBox.text = selectedConversation.Sentences[SentenceCounter].SpeakerName + ": ";

                dialogTextBox.text = selectedConversation.Sentences[SentenceCounter].Sentence;
                dialogNameBox.color = selectedConversation.TextColor;
                dialogTextBox.color = selectedConversation.TextColor;

                if (selectedConversation.Sentences[SentenceCounter].ReactionSprite != null)
                {
                    speakerImageBox.sprite = selectedConversation.Sentences[SentenceCounter].ReactionSprite;
                }
                else
                {
                    speakerImageBox.sprite = neutralSprite;
                }

                speakerImageBox.gameObject.SetActive(selectedConversation.Sentences[SentenceCounter].ShowSpeaker);
                eloiseImage.gameObject.SetActive(selectedConversation.Sentences[SentenceCounter].ShowEloise);

                SentenceCounter++;
            }
            else
            {
                OnEndClick();
            }
        }

        private void SetSprites(Sprite _sprite)
        {
            speakerImageBox.sprite = _sprite;
        }

        public void OnNextClick()
        {
            UpdateDialog();
        }

        #endregion
    }

}
