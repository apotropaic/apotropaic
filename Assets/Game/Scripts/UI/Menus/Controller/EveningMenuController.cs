using UnityEngine;
using Apotropaic.Events;

namespace Apotropaic.UI
{
    public class EveningMenuController : GameplayMenuController
    {
        // TODO : shouldn't be referenced.
        [SerializeField] private BagMenuController bagMenu = null;

        [SerializeField] private GameplayData gameplayData = null;
        [SerializeField] private ColorBar eveningBar = null;
        [SerializeField] private MenuClassifier shopMenuClassifier = null;
        [SerializeField] private MenuClassifier bagMenuClassifier = null;
        [SerializeField] private TrapStockPrototype trapStockPrototype = null;
        [SerializeField] private QuestInformationPanel questInformation = null;
        [SerializeField] private QuestSystemData questSystemData = null;
        #region UNITY
        private void Update()
        {
            UpdateEveningPhase();
        }

        #endregion


        #region PUBLIC
        public override void OnShowMenu(string options = "")
        {
            base.OnShowMenu(options);
            SubscribeToEvents();

            if (trapStockPrototype != null)
                trapStockPrototype.InitializeTrapStockPrototype();
            if (questSystemData.QuestDatas.Length > questSystemData.CurrentActiveQuest)
            {
                questInformation.UpdateInfo(questSystemData.QuestDatas[questSystemData.CurrentActiveQuest]);
            }
            else
            {
                questInformation.UpdateInfo(null);
            }
        }

        public override void OnHideMenu(string options = "")
        {
            base.OnHideMenu(options);
            UnsubscribeFromEvents();
        }
        #endregion

        #region PRIVATE
        // TODO : Remove from here.
        private void SubscribeToEvents()
        {
            UIEvents.OnOpenedShop += OnShopOpen;
            UIEvents.OnOpenedBag += OpenBag;
        }

        private void UnsubscribeFromEvents()
        {
            UIEvents.OnOpenedShop -= OnShopOpen;
            UIEvents.OnOpenedBag -= OpenBag;
        }

        private void OpenBag()
        {
            bagMenu.SetPreviousMenu(menuClassifier);

            MenuManager.ShowMenu(bagMenuClassifier);
            MenuManager.HideMenu(menuClassifier);
        }

        private void UpdateEveningPhase()
        {
            eveningBar.RefreshBar(1f - gameplayData.CurrentEveningProgress);
        }

        private void OnShopOpen()
        {
            MenuManager.ShowMenu(shopMenuClassifier);
            MenuManager.HideMenu(menuClassifier);
        }
        #endregion
    }
}