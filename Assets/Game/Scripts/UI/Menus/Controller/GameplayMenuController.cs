using Apotropaic.Events;
using Apotropaic.Data;
using UnityEngine;

namespace Apotropaic.UI
{
    public abstract class GameplayMenuController : Menu
    {
        [Header("Settings")]
        [SerializeField] private PlaytestSettings playtestSettings = null;

        [Space(20)]
        [SerializeField] private MenuClassifier pauseMenuClassifier = null;
        [SerializeField] private PauseMenuController pauseMenu = null;

        [SerializeField] private GameObject trapStock = null;

        private IPauseEventsInvoker pauseEventsInvoker = null;

        #region UNITY

        public override void OnShowMenu(string options = "")
        {
            base.OnShowMenu(options);
            if(trapStock != null)
                trapStock.GetComponent<TrapStockPrototype>().InitializeTrapStockPrototype();

        }
        public override void Initialize(IMenuManager manager)
        {
            base.Initialize(manager);
            pauseEventsInvoker = GetComponent<IPauseEventsInvoker>();
            LoadSettings();
        }
        #endregion

        #region PUBLIC
        public void Pause()
        {
            pauseMenu.SetPreviousMenuClassifier(menuClassifier);
            MenuManager.HideMenu(menuClassifier);
            MenuManager.ShowMenu(pauseMenuClassifier);
            Time.timeScale = 0f;
            pauseEventsInvoker.Pause();
        }
        #endregion

        #region PROTECTED
        protected PlaytestSettings PlaytestSettings { get { return playtestSettings; } }
        protected IPauseEventsInvoker PauseEventsInvoker { get { return pauseEventsInvoker; } }
        protected virtual void LoadSettings() { }
        #endregion

        #region PRIVATE
        #endregion
    }
}