using Apotropaic.Events;
using Apotropaic.Data;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Apotropaic.UI
{
    public class ShopMenuController : Menu
    {
        [SerializeField] private MenuClassifier previousMenuClassifier = null;

        [SerializeField] private PlayerData playerData = null;
        [SerializeField] private PlayerInventory shopInventoryData = null;
        [SerializeField] private PlayerInventory BagInventoryData = null;
        [SerializeField] private TrapSettingsList trapListData = null;


        [Space(30)]
        [SerializeField] private Text currencyText;
        [SerializeField] private GameObject contentPanel = null;
        [SerializeField] private GameObject listingPrefab = null;
        [SerializeField] private Text descriptionBox = null;
        [SerializeField] private Image descriptionImage = null;
        [SerializeField] private Scrollbar scrollBar = null;


        private IInventoryTrapAccessor inventoryTrapAccessor = null;
        private IPauseEventsInvoker pauseEventsInvoker = null;

        private bool shopState = true;

        private BasicTrapSettings selectedTrap = null;

        #region UNITY
        private void Awake()
        {
            populateShopInventory();

        }

        public override void OnShowMenu(string options = "")
        {
            base.OnShowMenu(options);
            inventoryTrapAccessor = playerData.InventoryTrapAccessor;
            shopState = true;
            selectedTrap = null;
            UpdateShopList();
            UpdateCurrency();
            Pause();
        }

        public override void OnHideMenu(string options = "")
        {
            base.OnHideMenu(options);
            Resume();
        }
        #endregion

        #region PUBLIC
        public override void Initialize(IMenuManager manager)
        {
            base.Initialize(manager);
            pauseEventsInvoker = GetComponent<IPauseEventsInvoker>();
        }

        public void CloseShop()
        {
            MenuManager.HideMenu(menuClassifier);
            MenuManager.ShowMenu(previousMenuClassifier);
        }

        public void OnBuyClick()
        {
            if (selectedTrap != null)
            {
                if (inventoryTrapAccessor.AddTrap(selectedTrap.UID, selectedTrap.Cost * 1, 1))
                {
                    Sell(selectedTrap.UID, 1);
                    UpdateCurrency();
                }
                //Else cant buy print message
            }
        }

        public void OnBuyToBag()
        {
            if (shopState)
            {
                BuyToBag();
            }
            else
            {
                SellFromBag();
            }
        }

        public void onSelectedChanged(BasicTrapSettings _incomingTrap)
        {
            selectedTrap = _incomingTrap;
            UpdateDescription();
        }

        public void OnBuyTabClicked()
        {
            shopState = true;
            selectedTrap = null;
            UpdateShopList();
        }

        public void OnSellTabClicked()
        {
            shopState = false;
            selectedTrap = null;
            UpdateShopList();
        }
        #endregion

        #region PRIVATE
        private void Pause()
        {
            Time.timeScale = 0f;
            pauseEventsInvoker.Pause();
        }

        private void Resume()
        {
            Time.timeScale = 1f;
            pauseEventsInvoker.Resume();
        }

        private void BuyToBag()
        {
            if (selectedTrap != null)
            {
                if (BagInventoryData.TrapList.Count < BagInventoryData.Slots)
                {
                    if (BagInventoryData.TrapList.ContainsKey(selectedTrap.UID))
                    {
                        if (BagInventoryData.TrapList[selectedTrap.UID] < BagInventoryData.MaximumQuantity)
                        {
                            if (inventoryTrapAccessor.GetCurrency() >= selectedTrap.Cost)
                            {
                                BagInventoryData.TrapList[selectedTrap.UID] = BagInventoryData.TrapList[selectedTrap.UID] + 1;
                                Sell(selectedTrap.UID, 1);
                                inventoryTrapAccessor.TakeCurrency(selectedTrap.Cost);
                                UpdateCurrency();
                                UpdateShopList();
                            }
                        }
                    }
                    else
                    {
                        if (inventoryTrapAccessor.GetCurrency() >= selectedTrap.Cost)
                        {
                            BagInventoryData.TrapList.Add(selectedTrap.UID, 1);
                            Sell(selectedTrap.UID, 1);
                            inventoryTrapAccessor.TakeCurrency(selectedTrap.Cost);
                            UpdateCurrency();
                            UpdateShopList();
                        }
                    }
                }
                else if (BagInventoryData.TrapList.Count == BagInventoryData.Slots)
                {
                    if (BagInventoryData.TrapList.ContainsKey(selectedTrap.UID))
                    {
                        if (BagInventoryData.TrapList[selectedTrap.UID] < BagInventoryData.MaximumQuantity)
                        {
                            if (inventoryTrapAccessor.GetCurrency() >= selectedTrap.Cost)
                            {
                                BagInventoryData.TrapList[selectedTrap.UID] = BagInventoryData.TrapList[selectedTrap.UID] + 1;
                                Sell(selectedTrap.UID, 1);
                                inventoryTrapAccessor.TakeCurrency(selectedTrap.Cost);
                                UpdateCurrency();
                                UpdateShopList();
                            }
                        }
                    }
                }
            }
        }

        private void SellFromBag()
        {
            if (BagInventoryData.TrapList.ContainsKey(selectedTrap.UID))
            {
                if (BagInventoryData.TrapList[selectedTrap.UID] > 1)
                {
                    BagInventoryData.TrapList[selectedTrap.UID] = BagInventoryData.TrapList[selectedTrap.UID] - 1;
                    inventoryTrapAccessor.AddCurrency(selectedTrap.Cost / 2);
                    UpdateShopList();
                    UpdateCurrency();
                }
                else if (BagInventoryData.TrapList[selectedTrap.UID] == 1)
                {
                    BagInventoryData.TrapList.Remove(selectedTrap.UID);
                    inventoryTrapAccessor.AddCurrency(selectedTrap.Cost / 2);
                    selectedTrap = null;
                    UpdateShopList();
                    UpdateCurrency();
                }
            }
        }

        private void UpdateDescription()
        {
            if (selectedTrap.Description != null)
            {
                descriptionBox.text = selectedTrap.Description;
            }


            descriptionImage.sprite = selectedTrap.SpriteFile;
        }

        private bool Sell(int ID, int _quantity)
        {
            if (shopInventoryData.TrapList[ID] < _quantity)
            {
                shopInventoryData.TrapList[ID] = shopInventoryData.TrapList[ID] - _quantity;
                UpdateShopList();
                return true;
            }
            else if (shopInventoryData.TrapList[ID] == _quantity)
            {
                shopInventoryData.TrapList.Remove(ID);
                UpdateShopList();
                return true;
            }
            return false;
        }

        private void UpdateShopList()
        {
            // Clear scrollView
            foreach (Transform child in contentPanel.transform)
            {
                child.GetComponent<Button>().onClick.RemoveAllListeners();
                Destroy(child.gameObject);
            }
            //Populate ScrollView
            if (shopState)
            {
                for (int i = 0; i < trapListData.Traps.Length; i++)
                {
                    if (trapListData.Traps[i].Unlocked)
                    {
                        GameObject go = Instantiate(listingPrefab, contentPanel.transform);

                        go.GetComponent<ItemListingSetter>().ApplySettings(trapListData.Traps[i], shopState, scrollBar, ((float)i / (float)(trapListData.Traps.Length - 1)));
                        go.GetComponent<ItemListingSetter>().AddListenerForButton(OnBuyToBag);
                        go.GetComponent<ItemListingSetter>().AddListenerForSetSelected(onSelectedChanged);
                        go.GetComponent<ItemListingSetter>().AddCallbackForEast(CloseShop);

                        if (BagInventoryData.TrapList.ContainsKey(trapListData.Traps[i].UID))
                        {
                            go.GetComponent<ItemListingSetter>().ApplyOwned(BagInventoryData.TrapList[trapListData.Traps[i].UID]);
                        }
                        else
                        {
                            go.GetComponent<ItemListingSetter>().ApplyOwned(0);
                        }
                        if (selectedTrap == null)
                        {
                            EventSystem.current.SetSelectedGameObject(go);
                            onSelectedChanged(go.GetComponent<ItemListingSetter>().GetTrapSettings());
                        }
                        else if (selectedTrap == trapListData.Traps[i])
                        {
                            EventSystem.current.SetSelectedGameObject(go);
                        }
                    }
                }
            }
            else
            {
                float j = 0.0f;
                for (int i = 0; i < trapListData.Traps.Length; i++)
                {
                    if (BagInventoryData.TrapList.ContainsKey(trapListData.Traps[i].UID))
                    {
                        GameObject go = Instantiate(listingPrefab, contentPanel.transform);

                        go.GetComponent<ItemListingSetter>().ApplySettings(trapListData.Traps[i], shopState, scrollBar, (j / (float)(BagInventoryData.TrapList.Count - 1)));
                        go.GetComponent<ItemListingSetter>().AddListenerForButton(OnBuyToBag);
                        go.GetComponent<ItemListingSetter>().AddListenerForSetSelected(onSelectedChanged);
                        go.GetComponent<ItemListingSetter>().AddCallbackForEast(CloseShop);
                        go.GetComponent<ItemListingSetter>().ApplyOwned(BagInventoryData.TrapList[trapListData.Traps[i].UID]);
                        j = j + 1.0f;
                        if (selectedTrap == null)
                        {
                            EventSystem.current.SetSelectedGameObject(go);
                            onSelectedChanged(go.GetComponent<ItemListingSetter>().GetTrapSettings());
                        }
                        else if (selectedTrap == trapListData.Traps[i])
                        {
                            EventSystem.current.SetSelectedGameObject(go);
                        }
                        else
                        {

                        }
                    }
                }
            }
        }

        private void UpdateCurrency()
        {
            currencyText.text = "" + inventoryTrapAccessor.GetCurrency();
        }

        //Temp func to be removed later
        private void populateShopInventory()
        {
            for (int i = 0; i < trapListData.Traps.Length; i++)
            {
                if (!shopInventoryData.TrapList.ContainsKey(trapListData.Traps[i].UID))
                    shopInventoryData.TrapList.Add(trapListData.Traps[i].UID, 100);
                else
                    shopInventoryData.TrapList[trapListData.Traps[i].UID] = 100;
            }
        }
        #endregion
    }

}
