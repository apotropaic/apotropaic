using Apotropaic.Core;
using Apotropaic.Data;
using UnityEngine;

namespace Apotropaic.UI
{
    public class MorningMenuController : GameplayMenuController
    {
        [SerializeField] private SceneLoaderTemp sceneLoader = null;
        [SerializeField] private SceneSettings sceneSettings = null;

        [SerializeField] private QuestInformationPanel questInformation = null;
        [SerializeField] private QuestSystemData questSystemData = null;

        #region UNITY
        #endregion

        #region PUBLIC
        public override void OnShowMenu(string options = "")
        {
            base.OnShowMenu(options);

            if (questSystemData.QuestDatas.Length > questSystemData.CurrentActiveQuest)
            {
                questInformation.UpdateInfo(questSystemData.QuestDatas[questSystemData.CurrentActiveQuest]);
            }
            else
            {
                questInformation.UpdateInfo(null);
            }

        }
        public void QuitToOffices()
        {
            MenuManager.HideMenu(menuClassifier);
            sceneLoader.LoadScene(sceneSettings.OfficesScene);
        }
        #endregion

        #region PRIVATE
        #endregion
    }
}