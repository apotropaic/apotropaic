using System;
using UnityEngine;
using Apotropaic.Events;

namespace Apotropaic.UI
{
    public class NightMenuController : GameplayMenuController
    {
        // TODO : shouldn't be referenced.
        [SerializeField] private BagMenuController bagMenu = null;

        public Action OnShowMenuAction = null;

        [Space(20)]
        [SerializeField] private ColorBar nightBar = null;
        [SerializeField] private ColorBar healthBar = null;

        [SerializeField] private GameplayData gameplayData = null;
        [SerializeField] private PlayerData playerData = null;
        [SerializeField] private TrapStockPrototype trapStockPrototype = null;

        [SerializeField] private MenuClassifier bagMenuClassifier = null;

        [SerializeField] private QuestInformationPanel questInformation = null;
        [SerializeField] private QuestSystemData questSystemData = null;

        // Cache
        int maxPlayerHealth = 0;
        int currentPlayerHealth = 0;

        #region UNITY
        private void Update()
        {
            UpdateNightPhase();
        }
        #endregion

        #region PUBLIC
        public override void OnShowMenu(string options = "")
        {
            base.OnShowMenu(options);
            SubscribeToEvents();

            trapStockPrototype.InitializeTrapStockPrototype();

            if (OnShowMenuAction != null)
                OnShowMenuAction();

            if (questSystemData.QuestDatas.Length > questSystemData.CurrentActiveQuest)
            {
                questInformation.UpdateInfo(questSystemData.QuestDatas[questSystemData.CurrentActiveQuest]);
            }
            else
            {
                questInformation.UpdateInfo(null);
            }
        }

        public override void OnHideMenu(string options = "")
        {
            base.OnHideMenu(options);
            UnsubscribeFromEvents();
        }
        #endregion

        #region PROTECTED
        #endregion

        #region PRIVATE
        // TODO : Remove from here.
        private void SubscribeToEvents()
        {
            UIEvents.OnOpenedBag += OpenBag;
        }

        private void UnsubscribeFromEvents()
        {
            UIEvents.OnOpenedBag -= OpenBag;
        }

        private void OpenBag()
        {
            bagMenu.SetPreviousMenu(menuClassifier);

            MenuManager.ShowMenu(bagMenuClassifier);
            MenuManager.HideMenu(menuClassifier);
        }

        private void UpdateNightPhase()
        {
            nightBar.RefreshBar(1f - gameplayData.CurrentNightProgress);

            if (playerData.CurrentHealth != currentPlayerHealth)
            {
                maxPlayerHealth = playerData.MaxHealth;
                currentPlayerHealth = playerData.CurrentHealth;
                healthBar.RefreshBar((float)currentPlayerHealth / maxPlayerHealth);
            }
        }
        #endregion
    }
}
