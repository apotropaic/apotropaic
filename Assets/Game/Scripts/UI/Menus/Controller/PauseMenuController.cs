using UnityEngine;
using Apotropaic.Events;
using Apotropaic.Data;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Apotropaic.UI
{
    public class PauseMenuController : Menu
    {
        public enum QuitScene
        {
            MainMenu = 0,
            Offices = 1,
            Library = 2
        }

        [SerializeField] private SceneLoaderTemp sceneLoader = null;
        [SerializeField] private SceneSettings sceneSettings = null;

        [SerializeField] private ControlsMenuController controlsMenuController = null;
        [SerializeField] private MenuClassifier controlsMenuClassifier = null;
        [SerializeField] private MenuClassifier journalMenuClassifier = null;

        [SerializeField] private GameplayData gameplayData = null;

        [SerializeField] private QuitScene quitScene = QuitScene.Offices;

        [SerializeField] private Button startButton = null;
        [SerializeField] private EventSystem eventSystem = null;
        // TODO
        [SerializeField] private MenuClassifier previousMenu;
        private IPauseEventsInvoker pauseEventsInvoker = null;

        public override void Initialize(IMenuManager manager)
        {
            base.Initialize(manager);

            pauseEventsInvoker = GetComponent<IPauseEventsInvoker>();
        }

        public override void OnShowMenu(string options = "")
        {
            base.OnShowMenu(options);

            if (eventSystem == null || startButton == null) return;

            eventSystem.SetSelectedGameObject(startButton.gameObject);
        }

        public void SetPreviousMenuClassifier(MenuClassifier _previousMenu)
        {
            previousMenu = _previousMenu;
        }

        public void ShowControls()
        {
            controlsMenuController.SetPreviousMenuClassifier(menuClassifier);
            MenuManager.ShowMenu(controlsMenuClassifier);
        }

        public void SetSelectedButton(Button _button)
        {
            eventSystem.SetSelectedGameObject(_button.gameObject);
        }

        public void Resume()
        {
            MenuManager.HideMenu(menuClassifier);
            MenuManager.ShowMenu(previousMenu);
            Time.timeScale = 1f;
            pauseEventsInvoker.Resume();
        }

        public void OpenJournal()
        {
            MenuManager.ShowMenu(journalMenuClassifier);
        }

        public void Quit()
        {
            Time.timeScale = 1f;

            MenuManager.HideMenu(menuClassifier);

            SceneReference targetScene = null;

            switch (quitScene)
            {
                case QuitScene.MainMenu:
                    targetScene = sceneSettings.MainMenuScene;
                    break;

                case QuitScene.Offices:
                    if (gameplayData.FinishedNightTutorial)
                    {
                        targetScene = sceneSettings.OfficesScene;
                    }
                    else
                    {
                        targetScene = sceneSettings.MainMenuScene;
                    }
                    break;
                case QuitScene.Library:
                    targetScene = sceneSettings.LibraryScene;
                    break;
            }

            if (CoreEvents.OnSaveGame != null)
                CoreEvents.OnSaveGame();

            sceneLoader.LoadScene(targetScene);
        }
    }
}