using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Apotropaic.UI
{
    public class ControlsMenuController : Menu
    {
        private MenuClassifier previousMenu;
        [SerializeField] private Button button;

        #region UNITY
        public override void OnShowMenu(string options = "")
        {
            base.OnShowMenu(options);
            EventSystem.current.SetSelectedGameObject(button.gameObject);
        }
        #endregion

        #region PUBLIC
        public void SetPreviousMenuClassifier(MenuClassifier _previousMenu)
        {
            previousMenu = _previousMenu;
        }

        public void Back()
        {
            MenuManager.HideMenu(menuClassifier);
            MenuManager.ShowMenu(previousMenu);
        }
        #endregion

        #region PRIVATE
        #endregion
    }
}