using Apotropaic.Events;
using Apotropaic.Data;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Apotropaic.UI
{
    public class BagMenuController : Menu
    {
        [SerializeField] private PlayerData playerData = null;
        [SerializeField] private PlayerInventory BagInventoryData = null;
        [SerializeField] private TrapSettingsList trapListData = null;

        [Space(30)]
        [SerializeField] private Text descriptionBox = null;
        [SerializeField] private Image descriptionImage = null;
        [SerializeField] private GameObject contentPanel = null;
        [SerializeField] private GameObject listingPrefab = null;
        [SerializeField] private Scrollbar scrollBar = null;

        private MenuClassifier previousMenuClassifier = null;

        private IInventoryTrapAccessor inventoryTrapAccessor = null;
        private IPauseEventsInvoker pauseEventsInvoker = null;

        private BasicTrapSettings selectedTrap = null;

        #region UNITY
        public override void OnShowMenu(string options = "")
        {
            base.OnShowMenu(options);
            inventoryTrapAccessor = playerData.InventoryTrapAccessor;
            UpdateBagList();
            Pause();
        }

        public override void OnHideMenu(string options = "")
        {
            base.OnHideMenu(options);
            Resume();
        }

        private void Awake()
        {
        }
        #endregion

        #region PUBLIC
        public override void Initialize(IMenuManager manager)
        {
            base.Initialize(manager);
            pauseEventsInvoker = GetComponent<IPauseEventsInvoker>();
        }

        public void SetPreviousMenu(MenuClassifier previousMenu)
        {
            previousMenuClassifier = previousMenu;
        }

        public void CloseShop()
        {
            MenuManager.HideMenu(menuClassifier);
            MenuManager.ShowMenu(previousMenuClassifier);
        }
        //  on taking a trap from the bag
        public void OnTakeClick()
        {
            if (selectedTrap != null)
            {
                if (inventoryTrapAccessor.AddTrap(selectedTrap.UID, 0, 1))
                {
                    if (!RemoveTrap(selectedTrap.UID, 1))
                    {
                        inventoryTrapAccessor.RemoveTrap(selectedTrap.UID);

                    }
                }
            }
            UpdateBagList();
        }
        // updates selected trap
        public void onSelectedChanged(BasicTrapSettings _incomingTrap)
        {
            selectedTrap = _incomingTrap;
            UpdateDescription();
        }
        #endregion

        #region PRIVATE
        private void Pause()
        {
            Time.timeScale = 0f;
            pauseEventsInvoker.Pause();
        }

        private void Resume()
        {
            Time.timeScale = 1f;
            pauseEventsInvoker.Resume();
        }
        // Removes trap from the bag when taken
        private bool RemoveTrap(int ID, int _quantity)
        {
            if (!BagInventoryData.TrapList.ContainsKey(ID))
            {
                UpdateBagList();
                return false;
            }

            if (BagInventoryData.TrapList[ID] > _quantity)
            {
                BagInventoryData.TrapList[ID] = BagInventoryData.TrapList[ID] - _quantity;

                return true;
            }
            else if (BagInventoryData.TrapList[ID] == _quantity)
            {
                BagInventoryData.TrapList.Remove(ID);

                return true;
            }
            return false;
        }

        // updates ui based on selected trap
        private void UpdateDescription()
        {
            if (selectedTrap.Description != null)
            {
                descriptionBox.text = selectedTrap.Description;
            }
            descriptionImage.sprite = selectedTrap.SpriteFile;
        }

        // Refreshes and populates the bag list 
        private void UpdateBagList()
        {
            // Clear scrollView
            foreach (Transform child in contentPanel.transform)
            {
                Destroy(child.gameObject);
            }
            float j = 0.0f;
            foreach (KeyValuePair<int, int> trap in BagInventoryData.TrapList)
            {
                for (int i = 0; i < trapListData.Traps.Length; i++)
                {
                    if (trap.Key == trapListData.Traps[i].UID)
                    {
                        GameObject go = Instantiate(listingPrefab, contentPanel.transform);
                        go.GetComponent<ItemListingSetterBag>().ApplySettings(trapListData.Traps[i], scrollBar, (j / (float)(BagInventoryData.TrapList.Count - 1)));
                        go.GetComponent<ItemListingSetterBag>().AddListenerForButton(OnTakeClick);
                        go.GetComponent<ItemListingSetterBag>().AddListenerForSetSelected(onSelectedChanged);
                        go.GetComponent<ItemListingSetterBag>().AddCallbackForEast(CloseShop);
                        j = j + 1.0f;
                        if (selectedTrap == null)
                        {
                            EventSystem.current.SetSelectedGameObject(go);
                            onSelectedChanged(go.GetComponent<ItemListingSetterBag>().GetTrapSettings());

                        }
                        else if (selectedTrap.UID == trap.Key)
                        {
                            EventSystem.current.SetSelectedGameObject(go);
                            //onSelectedChanged(go.GetComponent<ItemListingSetterBag>().GetTrapSettings());
                        }
                    }
                }
            }
        }
        #endregion
    }

}
