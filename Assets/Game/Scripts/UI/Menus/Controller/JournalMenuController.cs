using Apotropaic.Data;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Apotropaic.UI
{
    public class JournalMenuController : Menu
    {
        public enum JournalElementTypes
        {
            Traps = 0,
            Monsters = 1
        }

        [SerializeField] private JournalElementTypes defaultMenu = JournalElementTypes.Traps;
        [SerializeField] private Image detailsImage = null;
        [SerializeField] private Text descriptionText = null;
        [SerializeField] private GameObject trapsGrid = null;
        [SerializeField] private GameObject monstersGrid = null;
        [SerializeField] private TrapSettingsList trapsList = null;
        [SerializeField] private GameObject trapElementPrefab = null;
        [SerializeField] private GameObject monsterElementPrefab = null;

        [SerializeField] private EnemySettings[] enemies = null;

        #region UNITY
        #endregion

        #region PUBLIC
        public override void OnShowMenu(string options = "")
        {
            base.OnShowMenu(options);
            Initialize();
        }

        public override void OnHideMenu(string options = "")
        {
            base.OnHideMenu(options);
            ResetGrids();
        }

        public void ShowTraps()
        {
            monstersGrid.SetActive(false);
            trapsGrid.SetActive(true);
        }

        public void ShowMonsters()
        {
            trapsGrid.SetActive(false);
            monstersGrid.SetActive(true);
        }

        public void CloseMenu()
        {
            MenuManager.HideMenu(menuClassifier);
        }
        #endregion

        #region PRIVATE
        private void Initialize()
        {
            InitializeTrapsGrid();
            InitializeMonstersGrid();

            switch (defaultMenu)
            {
                case JournalElementTypes.Traps:
                    ShowTraps();
                    break;
                case JournalElementTypes.Monsters:
                    ShowMonsters();
                    break;
            }
        }

        private void InitializeTrapsGrid()
        {
            for (int i = 0; i < trapsList.Traps.Length; i++)
            {
                if (trapsList.Traps[i].Unlocked)
                {
                    InitializeGrid(trapElementPrefab, trapsGrid.transform, trapsList.Traps[i], OnTrapSelectionChanged);
                }
            }
        }

        private void InitializeMonstersGrid()
        {
            for (int i = 0; i < enemies.Length; i++)
            {
                InitializeGrid(monsterElementPrefab, monstersGrid.transform, enemies[i], OnMonsterSelectionChanged);
            }
        }

        private void InitializeGrid<T>(GameObject elementPrefab, Transform gridTransform, T settings, Action<T> selectionChangedAction)
        {
            GameObject newElement = Instantiate(elementPrefab, gridTransform);
            JournalElement<T> journalElement = newElement.GetComponent<JournalElement<T>>();
            journalElement.Initialize(settings);
            journalElement.OnSelectionChanged += selectionChangedAction;
        }

        private void ResetGrids()
        {
            ResetTrapsGrid();
            ResetMonstersGrid();
        }

        private void ResetTrapsGrid()
        {
            ResetGrid<JournalTrapElement, BasicTrapSettings>(trapsGrid.transform, OnTrapSelectionChanged);
        }

        private void ResetMonstersGrid()
        {
            ResetGrid<JournalMonsterElement, EnemySettings>(monstersGrid.transform, OnMonsterSelectionChanged);
        }

        private void ResetGrid<T, U>(Transform grid, Action<U> onSelectionChanged) where T : JournalElement<U>
        {
            if (grid.childCount == 0) return;

            for (int i = grid.childCount - 1; i >= 0; i--)
            {
                T journalTrap = grid.GetChild(i).GetComponent<T>();
                journalTrap.OnSelectionChanged -= onSelectionChanged;
                Destroy(grid.GetChild(i).gameObject);
            }
        }

        private void OnTrapSelectionChanged(BasicTrapSettings _incomingTrap)
        {
            detailsImage.sprite = _incomingTrap.JournalDetailedSprite;
            descriptionText.text = _incomingTrap.Description;
        }

        private void OnMonsterSelectionChanged(EnemySettings enemy)
        {
            detailsImage.sprite = enemy.EnemySprite;
            // TODO : Only apply description if enemy was mentioned by NPCs
            descriptionText.text = enemy.EnemyDescription;
        }
        #endregion
    }
}