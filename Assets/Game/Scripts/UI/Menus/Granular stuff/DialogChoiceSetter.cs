using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DialogChoiceSetter : MonoBehaviour
{
    private ConversationData data;
    [SerializeField] private Button button;
    [SerializeField] private Text prefaceText;

    private ScrollRect scrollRect;
    private float optionPos;

    #region UNITY
    private void Start()
    {

    }
    #endregion

    #region PUBLIC

    public void SetData(ConversationData _data, GameObject _scrollObj, float _optionPos)
    {
        data = _data;
        prefaceText.text = data.OptionPreface;
        scrollRect = _scrollObj.GetComponent<ScrollRect>();
        optionPos = _optionPos;
    }
    public void AddListenerForButton(UnityAction<ConversationData> callback)
    {
        button.onClick.AddListener(() => { callback(data); });
        
    }

    public void SetSelectedButton(Button _button)
    {
        EventSystem.current.SetSelectedGameObject(_button.gameObject);
        // scrollRect.verticalNormalizedPosition = optionPos / 2.0f;
    }
    #endregion

    #region PRIVATE
    private void OnDestroy()
    {
        button.onClick.RemoveAllListeners();
    }
    #endregion
}
