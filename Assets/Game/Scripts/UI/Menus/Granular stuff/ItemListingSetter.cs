using Apotropaic.Data;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class ItemListingSetter : MonoBehaviour
{
    private BasicTrapSettings settings = null;
    [SerializeField] private Button button = null;
    [SerializeField] private Image imageBox = null;
    [SerializeField] private Text costBox = null;
    [SerializeField] private Text nameBox = null;
    [SerializeField] private Text ownedBox = null;
    [SerializeField] private Scrollbar scrollbar = null;
    [SerializeField] private float itemPosition = 0.0f;
    [SerializeField] private InputData inputData = null;

    private UnityAction<BasicTrapSettings> setSelectedCallback;
    private UnityAction buyCallback;
    private UnityAction eastCallback;
    #region UNITY
    private void Start()
    { 
    }
    #endregion

    #region PUBLIC
    public void ApplySettings(BasicTrapSettings trapSettings, bool shopState, Scrollbar _scrollbar, float _itemPosition)
    {
        settings = trapSettings;
        imageBox.sprite = settings.SpriteFile;
        nameBox.text = settings.Name;
        if (shopState)
            costBox.text = "  Cost: " + settings.Cost;
        else
            costBox.text = " Sell Price: " + settings.Cost / 2;

        scrollbar = _scrollbar;
        itemPosition = _itemPosition;
    }

    public void ApplyOwned(int _owned)
    {
        ownedBox.text = ""+_owned;
    }

    public void AddListenerForButton(UnityAction callback)
    {
        buyCallback = callback;
        button.onClick.AddListener(callback);
    }

    public void AddListenerForSetSelected(UnityAction<BasicTrapSettings> _callback)
    {
        setSelectedCallback = _callback;
    }
    public void AddCallbackForEast(UnityAction callback)
    {
        eastCallback = callback;
    }

    public BasicTrapSettings GetTrapSettings()
    {
        return settings;
    }

    public void SetSelectedButton(Button _button)
    {
        EventSystem.current.SetSelectedGameObject(_button.gameObject);
        
    }
    public void onEastPress()
    {
        eastCallback.Invoke();
    }
    public void UpdateSelected()
    {
        setSelectedCallback.Invoke(settings);

        if (inputData.CurrentInputType == InputType.Gamepad)
            scrollbar.value = 1.0f - itemPosition;
    }

    public void Scroll(BaseEventData baseEventData)
    {
        PointerEventData pointerEventData = baseEventData as PointerEventData;
        if (inputData.CurrentInputType == InputType.KeyboardMouse)
            scrollbar.value += pointerEventData.scrollDelta.y / 100.0f;
    }

    /*
    public void InvokeButton()
    {
        button.onClick.Invoke();
    }
    */

    #endregion

    #region PRIVATE

    private void OnDestroy()
    {
        button.onClick.RemoveAllListeners();
    }
    #endregion
}
