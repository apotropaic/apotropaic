using Apotropaic.Data;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemListingSetterBag : MonoBehaviour
{
    private BasicTrapSettings settings = null;
    [SerializeField] private Button button = null;
    [SerializeField] private Image imageBox = null;
    [SerializeField] private Image imageBox1 = null;
    [SerializeField] private Text nameBox = null;
    [SerializeField] private Text InvQtyBox = null;

    [SerializeField] private Scrollbar scrollbar = null;
    [SerializeField] private float itemPosition = 0.0f;
    [SerializeField] private InputData inputData = null;

    [SerializeField] private PlayerInventory bagInventoryData = null;
    [SerializeField] private PlayerInventory playerInventoryData = null;


    private UnityAction<BasicTrapSettings> setSelectedCallback;
    private UnityAction eastCallback;
    #region UNITY
    private void Start()
    {

    }
    #endregion

    #region PUBLIC
    public void ApplySettings(BasicTrapSettings trapSettings, Scrollbar _scrollbar, float _itemPosition)
    {
        
        settings = trapSettings;
        imageBox.sprite = settings.SpriteFile;
        imageBox1.sprite = settings.SpriteFile;
        nameBox.text = settings.Name + " " + bagInventoryData.TrapList[settings.UID];
        if(playerInventoryData.TrapList.ContainsKey(settings.UID))
        {
            InvQtyBox.text = "" + playerInventoryData.TrapList[settings.UID];
        }
        else
        {
            InvQtyBox.text = "" + 0;
        }

        scrollbar = _scrollbar;
        itemPosition = _itemPosition;
    }

    public BasicTrapSettings GetTrapSettings()
    {
        return settings;
    }

    public void SetSelectedButton(Button _button)
    {
        EventSystem.current.SetSelectedGameObject(_button.gameObject);
        //setSelectedCallback.Invoke(settings);
    }

    public void AddListenerForButton(UnityAction callback)
    {
        //buyCallback = callback;
        button.onClick.AddListener(callback);
    }

    public void AddListenerForSetSelected(UnityAction<BasicTrapSettings> _callback)
    {
        setSelectedCallback = _callback;
    }

    public void AddCallbackForEast(UnityAction callback)
    {
        eastCallback = callback;
    }

    public void UpdateSelected()
    {
        setSelectedCallback.Invoke(settings);

        if (inputData.CurrentInputType == InputType.Gamepad)
            scrollbar.value = 1.0f - itemPosition;
    }

    public void Scroll(BaseEventData baseEventData)
    {
        PointerEventData pointerEventData = baseEventData as PointerEventData;
        if (inputData.CurrentInputType == InputType.KeyboardMouse)
            scrollbar.value += pointerEventData.scrollDelta.y / 100.0f;
    }

    public void onEastPress()
    {
        eastCallback.Invoke();
    }
    #endregion

    #region PRIVATE

    private void OnDestroy()
    {
        button.onClick.RemoveAllListeners();
    }

    #endregion
}
