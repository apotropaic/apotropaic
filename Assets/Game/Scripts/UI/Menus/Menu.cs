using UnityEngine;
using UnityEngine.Events;

namespace Apotropaic.UI
{
    public class Menu : MonoBehaviour, IMenuReader
    {
        [SerializeField] protected MenuClassifier menuClassifier;

        public enum StartingMenuStates
        {
            Ignore = 0,
            Active = 1,
            Disable = 2
        }

        [SerializeField] private StartingMenuStates startMenuState = StartingMenuStates.Active;
        [SerializeField] private bool resetPosition = true;

        public UnityEvent OnRefreshMenu = new UnityEvent();

        private IMenuManager menuManager = null;
        private Animator animator = null;
        private bool isOpen = false;

        #region UNITY
        protected virtual void OnDestroy()
        {
            if (menuManager == null) return;

            menuManager.Remove(this);
        }
        #endregion


        #region PUBLIC
        public virtual void Initialize(IMenuManager manager)
        {
            if (resetPosition == true)
            {
                RectTransform rectTransform = GetComponent<RectTransform>();
                rectTransform.localPosition = Vector3.zero;
            }

            animator = GetComponent<Animator>();
            menuManager = manager;
            menuManager.AddMenu(this);

            switch (startMenuState)
            {
                case StartingMenuStates.Active:
                    IsOpen = true;
                    break;
                case StartingMenuStates.Disable:
                    IsOpen = false;
                    break;
            }
        }

        public bool Initialized { get; set; }
        public string ObjectScene { get { return gameObject.scene.path; } }

        public bool IsOpen
        {
            get
            {
                return isOpen;
            }

            set
            {
                isOpen = value;

                if (animator != null)
                {
                    if (isOpen)
                        gameObject.SetActive(isOpen);

                    animator.SetBool("IsOpen", isOpen);
                }
                else
                {
                    gameObject.SetActive(isOpen);
                }

                if (isOpen)
                {
                    OnRefreshMenu.Invoke();
                }
            }
        }

        public MenuClassifier GetMenuClassifier()
        {
            return menuClassifier;
        }

        public virtual void OnShowMenu(string options = "")
        {
            IsOpen = true;
        }

        public virtual void OnHideMenu(string options = "")
        {
            IsOpen = false;
        }
        #endregion

        #region PROTECTED

        protected IMenuManager MenuManager { get { return menuManager; } }

        #endregion
    }
}