using UnityEngine;

namespace Apotropaic.UI
{
    public interface IMenuManager
    {
        public T GetMenu<T>(MenuClassifier menuClassifier) where T : Menu;

        public void AddMenu(Menu menu);

        public void Remove(Menu menu);

        public void ShowMenu(MenuClassifier classifier, string options = "");

        public void HideMenu(MenuClassifier classifier, string options = "");
    }
}