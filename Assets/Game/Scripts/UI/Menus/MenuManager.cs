using System.Collections.Generic;
using UnityEngine;
using Apotropaic.Core;
using System.Linq;
using Apotropaic.Data;

namespace Apotropaic.UI
{
    public class MenuManager : MonoBehaviour, IMenuManager
    {
        [SerializeField] private Menu loadingMenu = null;
        private Dictionary<string, Menu> menuList = new Dictionary<string, Menu>();

        #region UNITY
        private void Awake()
        {
            InitializeLoadingMenu();
            SceneLoader.Instance.onSceneLoadedEvent.AddListener(OnSceneLoaded);
        }

        private void OnDestroy()
        {
            if (SceneLoader.isValidSingleton())
            {
                SceneLoader.Instance.onSceneLoadedEvent.RemoveListener(OnSceneLoaded);
            }
        }
        #endregion

        #region PUBLIC
        public T GetMenu<T>(MenuClassifier menuClassifier) where T : Menu
        {
            Menu menu;

            if (menuList.TryGetValue(menuClassifier.MenuName, out menu))
            {
                return (T)menu;
            }

            return null;
        }

        public void AddMenu(Menu menu)
        {
            string menuName = menu.GetMenuClassifier().MenuName;

            if (menuList.ContainsKey(menuName))
            {
                Debug.Assert(false, $"Menu is already registered {menuName}");
            }

            menuList.Add(menuName, menu);
        }

        public void Remove(Menu menu)
        {
            menuList.Remove(menu.GetMenuClassifier().MenuName);
        }

        public void ShowMenu(MenuClassifier classifier, string options = "")
        {
            if (classifier == null) return;

            ToggleMenu(classifier, options, true);
        }

        public void HideMenu(MenuClassifier classifier, string options = "")
        {
            if (classifier == null) return;

            ToggleMenu(classifier, options, false);
        }
        #endregion

        private void InitializeLoadingMenu()
        {
            loadingMenu.Initialize(this);
            loadingMenu.Initialized = true;
        }

        private void OnSceneLoaded(List<string> scene)
        {
            for (int i = 0; i < scene.Count; i++)
            {
                InitializeInterfaces(scene[i]);
            }
        }

        private void InitializeInterfaces(string scene)
        {
            IEnumerable<IMenuReader> menus = FindObjectsOfType<MonoBehaviour>(true).OfType<IMenuReader>();

            foreach (IMenuReader menuReader in menus)
            {
                if (menuReader.ObjectScene == scene && !menuReader.Initialized)
                {
                    menuReader.Initialize(this);
                    menuReader.Initialized = true;
                }
            }
        }

        private void ToggleMenu(MenuClassifier classifier, string options, bool showMenu)
        {
            Menu menu;

            if (menuList.TryGetValue(classifier.MenuName, out menu))
            {
                if (showMenu && !menu.IsOpen)
                    menu.OnShowMenu(options);
                else if (!showMenu && menu.IsOpen)
                    menu.OnHideMenu(options);
            }
        }
    }
}
