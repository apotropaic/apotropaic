using UnityEngine;

namespace Apotropaic.UI
{
    public interface IMenuReader
    {
        public void Initialize(IMenuManager manager);
        public bool Initialized { get; set; }
        public string ObjectScene { get; }
    }
}