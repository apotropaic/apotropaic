using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Apotropaic.UI
{
    public class JournalElement<T> : MonoBehaviour
    {
        [SerializeField] protected Image previewImage = null;
        [SerializeField] protected Text title = null;
        public Action<T> OnSelectionChanged = null;

        protected T settings = default;

        #region PUBLIC
        public virtual void Initialize(T settings)
        {
            this.settings = settings;
        }

        public void SetSelectedButton(Button _button)
        {
            EventSystem.current.SetSelectedGameObject(_button.gameObject);
            OnSelectionChanged?.Invoke(settings);
        }
        #endregion
    }
}