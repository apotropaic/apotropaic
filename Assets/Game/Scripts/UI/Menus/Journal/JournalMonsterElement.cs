using Apotropaic.Data;

namespace Apotropaic.UI
{
    public class JournalMonsterElement : JournalElement<EnemySettings>
    {
        #region PUBLIC
        public override void Initialize(EnemySettings enemySettings)
        {
            base.Initialize(enemySettings);
            previewImage.sprite = enemySettings.EnemySprite;
            title.text = enemySettings.EnemyName;
        }
        #endregion
    }
}