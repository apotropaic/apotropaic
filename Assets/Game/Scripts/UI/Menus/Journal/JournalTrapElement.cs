using Apotropaic.Data;

namespace Apotropaic.UI
{
    public class JournalTrapElement : JournalElement<BasicTrapSettings>
    {
        #region PUBLIC
        public override void Initialize(BasicTrapSettings trapSettings)
        {
            base.Initialize(trapSettings);
            previewImage.sprite = trapSettings.SpriteFile;
            title.text = trapSettings.Name;
        }
        #endregion
    }
}