using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class AudioPanel : MonoBehaviour
{
    [SerializeField] private AudioMixer audioMixer = null;

    [SerializeField] private Slider masterSlider = null;
    [SerializeField] private Slider musicSlider = null;
    [SerializeField] private Slider sfxSlider = null;
    [SerializeField] private Color highlightColor;

    private int selectedSlider;
    private Slider[] sliderList;

    private const string masterVolName = "MasterVolume";
    private const string musicVolName = "MusicVolume";
    private const string sfxVolName = "SfxVolume";

    private const float minAttenuation = -80f;

    #region UNITY
    private void OnEnable()
    {
        RefreshSliderValues(masterSlider, masterVolName);
        RefreshSliderValues(musicSlider, musicVolName);
        RefreshSliderValues(sfxSlider, sfxVolName);

        //selectedSlider = 0;
        //sliderList = new Slider[] { masterSlider, musicSlider, sfxSlider};
        //sliderList[selectedSlider].handleRect.GetComponent<Image>().color = highlightColor;
    }
    #endregion

    #region PUBLIC
    public void UpdateMasterSlider(float value)
    {
        UpdateSlider(value, masterVolName);
    }

    public void UpdateMusicSlider(float value)
    {
        UpdateSlider(value, musicVolName);
    }

    public void UpdateSfxSlider(float value)
    {
        UpdateSlider(value, sfxVolName);
    }

    public void HandlePlayerInput(InputAction.CallbackContext callbackContext)
    {
        Vector2 value = callbackContext.ReadValue<Vector2>();
        SetSelectedSlider(value.y);
        AdjustVolume(value.x);
        Debug.Log("Called Input");
    }
    #endregion

    #region PRIVATE
    private void RefreshSliderValues(Slider slider, string volName)
    {
        float vol = 0f;

        if (!audioMixer.GetFloat(volName, out vol))
        {
            Debug.LogWarning("Mixer Variable not found.");
        }

        slider.value = 1f - (vol / minAttenuation);
    }

    private void SetSelectedSlider(float _value)
    {
        if (Mathf.Abs(_value) < 0.1f)
            return;

        if(_value > 0)
        {
            sliderList[selectedSlider].handleRect.GetComponent<Image>().color = Color.white;
            selectedSlider++;
            if (selectedSlider > sliderList.Length - 1)
                selectedSlider = 0;
        }
        if(_value < 0)
        {
            sliderList[selectedSlider].handleRect.GetComponent<Image>().color = Color.white;
            selectedSlider--;
            if (selectedSlider < 0)
                selectedSlider = sliderList.Length - 1;
        }
        ApplySelection();
    }

    private void AdjustVolume(float _value)
    {
        if (Mathf.Abs(_value) < 0.1f)
            return;

        if(_value > 0)
        {

        }
        if (_value < 0)
        {

        }
    }
    private void ApplySelection()
    {
        sliderList[selectedSlider].handleRect.GetComponent<Image>().color = highlightColor;
    }

    private void UpdateSlider(float value, string volName)
    {
        float attenuation = (1f - value) * minAttenuation;
        audioMixer.SetFloat(volName, attenuation);
    }
    #endregion
}