using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ArtGalleryPanel : MonoBehaviour
{
    [SerializeField] private Image artPreviewImage = null;
    [SerializeField] private Button nextBtn = null;
    [SerializeField] private Button previousBtn = null;

    private const string artworksPath = "ConceptArt";
    private Sprite[] artSprites = null;
    private int currentArtIndex = 0;

    #region UNITY
    private void Start()
    {
        Initialize();
    }

    private void OnEnable()
    {
        nextBtn.gameObject.SetActive(true);
        previousBtn.gameObject.SetActive(false);
        currentArtIndex = 0;

        if (artSprites != null && artSprites.Length > 0)
            artPreviewImage.sprite = artSprites[currentArtIndex];
    }
    #endregion

    #region PUBLIC
    public void NextArtwork()
    {
        if (artSprites == null || artSprites.Length == 0) return;
        if (currentArtIndex == artSprites.Length - 1) return;

        currentArtIndex++;
        artPreviewImage.sprite = artSprites[currentArtIndex];

        if (currentArtIndex == artSprites.Length - 1)
        {
            nextBtn.gameObject.SetActive(false);
            EventSystem.current.SetSelectedGameObject(previousBtn.gameObject);
        }
            

        if (currentArtIndex > 0 && !previousBtn.gameObject.activeInHierarchy)
            previousBtn.gameObject.SetActive(true);
    }

    public void PreviousArtwork()
    {
        if (artSprites == null || artSprites.Length == 0) return;
        if (currentArtIndex == 0) return;

        currentArtIndex--;
        artPreviewImage.sprite = artSprites[currentArtIndex];

        if (currentArtIndex == 0)
        {
            previousBtn.gameObject.SetActive(false);
            EventSystem.current.SetSelectedGameObject(nextBtn.gameObject);
        }

        if (currentArtIndex != artSprites.Length - 1 && !nextBtn.gameObject.activeInHierarchy)
            nextBtn.gameObject.SetActive(true);
    }
    #endregion

    #region PRIVATE
    private void Initialize()
    {
        artSprites = Resources.LoadAll<Sprite>(artworksPath);

        if (artSprites == null || artSprites.Length == 0)
            return;

        artPreviewImage.sprite = artSprites[currentArtIndex];
    }
    #endregion
}