using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SetDefaultSelectedButtons : MonoBehaviour
{
    [SerializeField] private GameObject Defaultbutton;
    [SerializeField] private GameObject PreviousButton;
    #region UNITY
    private void Start()
    {

    }

    private void OnEnable()
    {
        EventSystem.current.SetSelectedGameObject(Defaultbutton);
    }

    private void OnDisable()
    {
        if (EventSystem.current == null) return;

        EventSystem.current.SetSelectedGameObject(PreviousButton);
    }
    #endregion

    #region PUBLIC
    #endregion

    #region PRIVATE
    #endregion
}
