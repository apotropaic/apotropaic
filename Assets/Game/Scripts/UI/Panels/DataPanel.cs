using UnityEngine;
using UnityEngine.UI;
using Apotropaic.Events;

namespace Apotropaic.UI
{
    public class DataPanel : MonoBehaviour
    {
        [SerializeField] private Text dataDeletedMessage = null;

        #region UNITY
        private void OnEnable()
        {
            dataDeletedMessage.enabled = false;
        }
        #endregion

        #region PUBLIC
        public void DeleteAllData()
        {
            CoreEvents.OnDeleteSavedData?.Invoke();
            dataDeletedMessage.enabled = true;
        }
        #endregion

        #region PRIVATE
        #endregion
    }
}