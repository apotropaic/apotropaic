using UnityEngine;
using UnityEngine.UI;

namespace Apotropaic.UI
{
    public class ColorBar : MonoBehaviour
    {
        [SerializeField] private Image barImage = null;
        [SerializeField] private Gradient barColors = null;

        #region PUBLIC
        public void RefreshBar(float fillAmount)
        {
            if (fillAmount == float.NaN) return;

            barImage.fillAmount = fillAmount;
            barImage.color = barColors.Evaluate(fillAmount);
        }
        #endregion
    }
}