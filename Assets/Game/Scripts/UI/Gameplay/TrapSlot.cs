using Apotropaic.Data;
using UnityEngine;
using UnityEngine.UI;

// Updates Slots of traps in the hud based on the calls from TrapStockPrototype

public class TrapSlot : MonoBehaviour
{
    private BasicTrapSettings trapSettings = null;
    [SerializeField] Image trapIcon = null;
    [SerializeField] Text quantity = null;

    public BasicTrapSettings TrapSettings { get { return trapSettings; } }
    #region UNITY
    private void Start()
    {
    }
    #endregion

    #region PUBLIC
    public void SetSlot(BasicTrapSettings _trapSettings, int _quantity)
    {
        trapSettings = _trapSettings;

        if(_trapSettings == null)
        {
            trapIcon.enabled = false;
            quantity.enabled = false;
        }
        else
        {
            trapIcon.enabled = true;
            quantity.enabled = true;
            trapIcon.sprite = trapSettings.SpriteFile;
            quantity.text = "x" + _quantity;
        }
    }


    #endregion

    #region PRIVATE
    #endregion
}
