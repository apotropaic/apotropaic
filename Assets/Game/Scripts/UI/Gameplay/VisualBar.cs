using UnityEngine;

public class VisualBar : MonoBehaviour
{
    [SerializeField] private Transform targetObject = null;

    private float currentVal = 0f;
    private float maxVal = 0f;

    private Vector3 initialScale = Vector3.one;

    #region UNITY
    private void Update()
    {
        if (maxVal == 0f) return;

        if (gameObject.activeInHierarchy)
        {
            targetObject.localScale = new Vector3(initialScale.x * (currentVal / maxVal), targetObject.localScale.y, targetObject.localScale.z);
        }
    }
    #endregion

    #region PUBLIC
    public void EnableBar(bool enable)
    {
        gameObject.SetActive(enable);

        if (enable)
        {
            initialScale = targetObject.localScale;
        }
        else
        {
            targetObject.localScale = initialScale;
        }
    }

    public void SetCurrentVal(float _currentVal)
    {
        currentVal = _currentVal;
    }

    public void SetMaxVal(float _maxVal)
    {
        maxVal = _maxVal;
        currentVal = _maxVal;
    }
    #endregion

    #region PRIVATE
    #endregion
}