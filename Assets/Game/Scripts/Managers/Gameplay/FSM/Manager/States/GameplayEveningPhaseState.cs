using System.Collections.Generic;
using UnityEngine;
using Apotropaic.Events;

public class GameplayEveningPhaseState : GameplayBaseState
{
    // Settings
    private float eveningLength = 0f;

    private float timePassed = 0f;

    #region UNITY
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);

        //fsm.GameplayManager.EnableEveningMenu();
        UIEvents.OnConversationStarted += OnConversationOpen;
        GameplayEvents.OnEveningEnter?.Invoke();
        fsm.GameplayManager.EnableShop(true);
        fsm.GameplayData.TimeOfDay = TimeOfDay.Evening;
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
        fsm.GameplayManager.EnableShop(false);
        UIEvents.OnConversationStarted -= OnConversationOpen;

    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateUpdate(animator, stateInfo, layerIndex);

        ProcessTime();
    }
    #endregion

    #region PUBLIC
    #endregion

    #region PROTECTED
    protected override void LoadSettings()
    {
        base.LoadSettings();

        eveningLength = fsm.GameplaySettings.EveningLength;
    }
    #endregion

    #region PRIVATE
    private void ProcessTime()
    {
        timePassed = Mathf.Clamp(timePassed + Time.deltaTime, 0f, eveningLength);
        fsm.GameplayData.CurrentEveningProgress = timePassed / eveningLength;

        if (timePassed == eveningLength)
        {
            fsm.ChangeState(fsm.NightPhaseState);
        }
    }

    private void OnConversationOpen(List<ConversationData> openConversations, Sprite _sprite)
    {
        fsm.GameplayManager.EnableDialogMenu();

        UIEvents.OnConversationSet?.Invoke(openConversations, _sprite);

    }
    #endregion
}