using UnityEngine;
using Apotropaic.Events;

public class GameplayNightPhaseState : GameplayBaseState
{
    // Settings
    private float nightLength = 0f;
    private float nightInitialProgress = 0f;

    private float timePassed = 0f;

    #region UNITY
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        fsm.GameplayManager.EnableNightMenu();
        SubscribeToEvents();
        fsm.GameplayData.TimeOfDay = TimeOfDay.Night;

        if (GameplayEvents.OnNightPhase != null)
            GameplayEvents.OnNightPhase();

        if (CoreEvents.OnSaveGame != null)
            CoreEvents.OnSaveGame();
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
        UnsubscribeFromEvents();
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateUpdate(animator, stateInfo, layerIndex);

        ProcessNightPhase();
    }
    #endregion

    #region PROTECTED
    protected override void LoadSettings()
    {
        base.LoadSettings();

        nightLength = fsm.GameplaySettings.NightLength;
        nightInitialProgress = fsm.GameplaySettings.NightInitialProgress;

        timePassed = nightInitialProgress * nightLength;
    }
    #endregion

    #region PRIVATE
    private void SubscribeToEvents()
    {
        GameplayEvents.OnPlayerDied += OnPlayerDied;
    }

    private void UnsubscribeFromEvents()
    {
        GameplayEvents.OnPlayerDied -= OnPlayerDied;
    }

    private void ProcessNightPhase()
    {
        ProcessTime();
    }

    private void OnPlayerDied()
    {
        fsm.ChangeState(fsm.GameOverState);
    }

    private void ProcessTime()
    {
        timePassed = Mathf.Clamp(timePassed + Time.deltaTime, 0f, nightLength);
        fsm.GameplayData.CurrentNightProgress = timePassed / nightLength;

        if (timePassed == nightLength)
        {
            fsm.ChangeState(fsm.GameOverState);
        }
    }
    #endregion
}