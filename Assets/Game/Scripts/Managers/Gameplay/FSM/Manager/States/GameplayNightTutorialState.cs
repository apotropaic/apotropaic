using System.Collections.Generic;
using UnityEngine;
using Apotropaic.Events;

public class GameplayNightTutorialState : GameplayBaseState
{
    #region UNITY
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        SubscribeToEvents();
        GameplayEvents.OnNightTutorial?.Invoke();
    }

    private void OnDestroy()
    {
        UnsubscribeFromEvents();
    }
    #endregion

    #region PUBLIC
    #endregion

    #region PRIVATE
    private void SubscribeToEvents()
    {
        UIEvents.OnConversationStarted += OnConversationStarted;
        CoreEvents.OnNightTutorialEnded += OnTutorialEnded;
    }

    private void UnsubscribeFromEvents()
    {
        UIEvents.OnConversationStarted -= OnConversationStarted;
        CoreEvents.OnNightTutorialEnded -= OnTutorialEnded;
    }

    private void OnConversationStarted(List<ConversationData> conversationData, Sprite _sprite)
    {
        fsm.GameplayManager.EnableDialogMenu();
        UIEvents.OnConversationSet?.Invoke(conversationData, _sprite);
    }

    private void OnTutorialEnded()
    {
        fsm.GameplayManager.DisableAllMenus();
    }
    #endregion
}