using Apotropaic.Core;
using Apotropaic.Data;
using System.Collections.Generic;
using UnityEngine;

public class GameplayIntroState : GameplayBaseState
{
    [SerializeField] private SceneSettings sceneSettings = null;

    #region UNITY
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        SubscribeToEvents();
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
        UnsubscribeFromEvents();
    }
    #endregion

    #region PROTECTED
    #endregion

    #region PRIVATE
    private void StartGameplay()
    {
        fsm.GameplayData.CurrentEveningProgress = 0f;
        fsm.GameplayData.CurrentNightProgress = 0f;
        // TODO : Start phase depending on settings (For ease of testing)
        // If night tutorial not cleared, go to night phase directly.
        if (!GameplayData.FinishedNightTutorial)
        {
            fsm.ChangeState(fsm.NightTutorialState);
        }
        else
        {
            fsm.ChangeState(fsm.EveningPhaseState);
        }
    }

    private void SubscribeToEvents()
    {
        SceneLoader.Instance.onSceneLoadedEvent.AddListener(OnSceneLoaded);
    }

    private void UnsubscribeFromEvents()
    {
        SceneLoader.Instance.onSceneLoadedEvent.RemoveListener(OnSceneLoaded);
    }

    private void OnSceneLoaded(List<string> scenes)
    {
        for (int i = 0; i < scenes.Count; i++)
        {
            if (scenes[i] == sceneSettings.LibraryScene)
            {
                StartGameplay();
            }
        }
    }
    #endregion
}