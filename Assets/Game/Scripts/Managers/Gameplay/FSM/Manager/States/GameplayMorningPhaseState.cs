using System.Collections.Generic;
using UnityEngine;
using Apotropaic.Events;

public class GameplayMorningPhaseState : GameplayBaseState
{
    #region UNITY
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        fsm.GameplayManager.EnableMorningMenu();
        fsm.GameplayManager.EnableMorningMurmur(true);
        GameplayEvents.OnLockTrapPlacement?.Invoke(true);
        UIEvents.OnConversationStarted += OnConversationStarted;
        CoreEvents.OnOfficeTransition += OnOfficeSceneLoad;

        fsm.GameplayData.TimeOfDay = TimeOfDay.Morning;
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
        fsm.GameplayManager.EnableMorningMurmur(false);
        UIEvents.OnConversationStarted -= OnConversationStarted;
        CoreEvents.OnOfficeTransition -= OnOfficeSceneLoad;
    }
    #endregion

    #region PUBLIC
    #endregion

    #region PRIVATE
    private void OnConversationStarted(List<ConversationData> data, Sprite sprite)
    {
        fsm.GameplayManager.EnableDialogMenu();
        UIEvents.OnConversationSet?.Invoke(data, sprite);
    }

    private void OnOfficeSceneLoad()
    {
        fsm.GameplayManager.DisableAllMenus();
        fsm.GameplayManager.EnableMorningMurmur(false);
        UIEvents.OnConversationStarted -= OnConversationStarted;
        CoreEvents.OnOfficeTransition -= OnOfficeSceneLoad;
    }
    #endregion
}