using Apotropaic.Data;
using UnityEngine;
using Apotropaic.Events;

public class GameplayGameOverState : GameplayBaseState
{
    [SerializeField] private PlayerData playerData = null;
    [SerializeField] private GameplaySettings gameplaySettings = null;
    [SerializeField] private PlayerInventory playerInventory = null;

    #region UNITY
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);

        bool playerWins = playerData.CurrentHealth > 0;

        if (playerWins)
        {
            GameplayData.NightsCleared++;
            playerInventory.Currency += gameplaySettings.ClearedNightReward;
        }

        fsm.GameplayManager.EnableGameoverMenu();

        if (GameplayEvents.OnGameOver != null)
            GameplayEvents.OnGameOver();

        if (CoreEvents.OnSaveGame != null)
            CoreEvents.OnSaveGame();

        SubscribeToEvents();
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);

        UnsubscribeFromEvents();
    }
    #endregion

    #region PUBLIC
    #endregion

    #region PRIVATE
    private void SubscribeToEvents()
    {
        GameplayEvents.OnMorningPhase += OnMorningPhase;
    }

    private void UnsubscribeFromEvents()
    {
        GameplayEvents.OnMorningPhase -= OnMorningPhase;
    }

    private void OnMorningPhase()
    {
        fsm.ChangeState(fsm.MorningPhaseState);
    }
    #endregion
}