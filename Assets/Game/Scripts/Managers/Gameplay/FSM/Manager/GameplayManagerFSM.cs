using Apotropaic.Data;
using UnityEngine;

public class GameplayManagerFSM : FSM
{
    [Header("Settings")]
    [SerializeField] private GameplaySettings settings = null;

    [SerializeField] private GameplayData gameplayData = null;
    [SerializeField] private GameplayManager gameplayManager = null;

    #region PUBLIC
    public int IntroState { get { return stateHashCodes[0]; } }
    public int EveningPhaseState { get { return stateHashCodes[1]; } }
    public int NightPhaseState { get { return stateHashCodes[2]; } }
    public int MorningPhaseState { get { return stateHashCodes[3]; } }
    public int GameOverState { get { return stateHashCodes[4]; } }
    public int NightTutorialState { get { return stateHashCodes[5]; } }

    public GameplayData GameplayData { get { return gameplayData; } }
    public GameplaySettings GameplaySettings { get { return settings; } }
    public GameplayManager GameplayManager { get { return gameplayManager; } }
    #endregion
}