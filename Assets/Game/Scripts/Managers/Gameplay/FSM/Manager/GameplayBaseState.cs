using UnityEngine;

public abstract class GameplayBaseState : FSMBaseState<GameplayManagerFSM>
{
    [SerializeField] private GameplayData gameplayData = null;

    #region UNITY
    public override void Initialize(GameObject _owner, FSM _fsm)
    {
        base.Initialize(_owner, _fsm);
        LoadSettings();
    }
    #endregion

    #region PUBLIC
    #endregion

    #region PROTECTED
    protected virtual void LoadSettings() { }
    protected GameplayData GameplayData { get { return gameplayData; } }
    #endregion

    #region PRIVATE
    #endregion
}