using EasyButtons;
using System.Collections.Generic;
using UnityEngine;
using Apotropaic.Data;
using Apotropaic.Events;

public class EnemySpawner : MonoBehaviour
{
    private enum EnemyType
    {
        Regular = 0,
        Tutorial = 1,
        Quest = 2
    }

    [Header("Settings")]
    [SerializeField] private EnemySpawnGlobalSettings enemySpawnSettings = null;

    // Settings
    private float minSpawnPointDist = 0f;
    private int? maxEnemiesCount = null;

    [Space(20)]
    [SerializeField] private GameplayData gameplayData = null;
    [SerializeField] private PlayerData playerData = null;
    [SerializeField] private SoundSystem soundSystem = null;
    [SerializeField] private Transform spawnPointsRoot = null;
    [SerializeField] private Transform enemiesRoot = null;
    [SerializeField] private int enemyPoolSize = 0;
    [Header("Tutorial")]
    [SerializeField] private GameObject tutorialEnemyPrefab = null;
    [SerializeField] private EnemySpawnPoint tutorialSpawnPoint = null;
    [Header("Quest")]
    [SerializeField] private GameObject questEnemyPrefab = null;
    [SerializeField] private Transform questSpawnPointsRoot = null;

    private GameObject[] enemyPrefabs = null;

    private Transform playerTransform = null;
    private EnemySpawnPoint[] spawnPoints = null;
    private EnemySpawnPoint[] questSpawnPoints = null;
    private List<List<GameObject>> enemyPools = null;

    private List<SpawnSettingsBundle> spawnData = null;
    private int currentSpawnDataIndex = 0;
    private bool isNightPhase = false;

    private List<EnemyController> activeEnemies = null;
    private Vector3 questPosition = Vector3.zero;
    // private int activeEnemiesCount = 0;

    #region UNITY
    private void Awake()
    {
        LoadSettings();
        LoadEnemyPrefabs();
        InitializeNightSpawnSettings();
        InitializeSpawnPoints();
        SubscribeToEvents();
    }

    private void Start()
    {
        CachePlayerTransform();
        InitializeEnemyPools();
    }

    private void Update()
    {
        if (isNightPhase)
            ProcessNightPhase();
    }

    private void OnDestroy()
    {
        UnsubscribeFromEvents();
    }
    #endregion

    #region PUBLIC
    [Button]
    public void SpawnQuestEnemyTest()
    {
        Vector3 questPos = Vector3.zero;
        SpawnQuestEnemy(questPos);
    }
    #endregion

    #region PRIVATE
    private void LoadSettings()
    {
        minSpawnPointDist = enemySpawnSettings.MinSpawnPointDist;
        maxEnemiesCount = enemySpawnSettings.MaxEnemiesCount;
    }

    private void LoadEnemyPrefabs()
    {
        enemyPrefabs = Resources.LoadAll<GameObject>("Prefabs/Enemies");
    }

    private void CachePlayerTransform()
    {
        playerTransform = playerData.transform;
    }

    private void SubscribeToEvents()
    {
        GameplayEvents.OnNightPhase += OnNightPhase;
        GameplayEvents.OnGameOver += OnGameOver;
        GameplayEvents.OnSpawnTutorialEnemy += SpawnTutorialEnemy;
        GameplayEvents.OnBookSpawned += SpawnQuestEnemy;
    }

    private void UnsubscribeFromEvents()
    {
        GameplayEvents.OnNightPhase -= OnNightPhase;
        GameplayEvents.OnGameOver -= OnGameOver;
        GameplayEvents.OnSpawnTutorialEnemy -= SpawnTutorialEnemy;
        GameplayEvents.OnBookSpawned -= SpawnQuestEnemy;
    }

    private void OnNightPhase()
    {
        isNightPhase = true;
    }

    private void OnGameOver()
    {
        isNightPhase = false;
        DespawnAllEnemies();
    }

    private void SpawnTutorialEnemy()
    {
        SpawnEnemyPrefab(tutorialEnemyPrefab, EnemyType.Tutorial);
    }

    private void SpawnQuestEnemy(Vector3 questPos)
    {
        questPosition = questPos;
        SpawnEnemyPrefab(questEnemyPrefab, EnemyType.Quest);
    }

    private void SpawnEnemyPrefab(GameObject prefab, EnemyType enemyType)
    {
        GameObject newEnemy = Instantiate(prefab);
        IPlayerDamageDealer playerDamageDealer = playerTransform.GetComponent<IPlayerDamageDealer>();
        newEnemy.GetComponent<EnemyController>().Initialize(playerDamageDealer);
        newEnemy.SetActive(false);
        SpawnEnemy(newEnemy, enemyType);
    }

    private void InitializeNightSpawnSettings()
    {
        int nightsCleared = gameplayData.NightsCleared;
        int enemiesCount = enemyPrefabs.Length;

        spawnData = new List<SpawnSettingsBundle>();
        EnemySettings[] enemySettings = new EnemySettings[enemiesCount];

        // Cache the enemy settings
        for (int i = 0; i < enemiesCount; i++)
        {
            enemySettings[i] = enemyPrefabs[i].GetComponent<EnemyController>().Settings;
        }

        for (int i = 0; i < enemiesCount; i++)
        {
            int nightsClamped = nightsCleared;
            if (nightsClamped >= enemySettings[i].EnemySpawnSettings.Count)
            {
                nightsClamped = enemySettings[i].EnemySpawnSettings.Count - 1;
            }

            // Get the night spwan settings of the appropirate night of the current enemy
            List<NightSpawnSettings> nightSpawnSettings = enemySettings[i].EnemySpawnSettings[nightsClamped].NightSpawnSettings;

            // Iterate through the night spawn settings, and add them based on chronological order
            for (int j = 0; j < nightSpawnSettings.Count; j++)
            {
                SpawnSettingsBundle bundle = new SpawnSettingsBundle(i, nightSpawnSettings[j]);
                // If there are no elements in the list, add the first element.
                if (spawnData.Count == 0)
                {
                    spawnData.Add(bundle);
                }
                else
                {
                    for (int k = 0; k < spawnData.Count; k++)
                    {
                        // If the spawn time is less than the spawn time of the current element, insert.
                        if (nightSpawnSettings[j].SpawnTime < spawnData[k].SpawnSettings.SpawnTime)
                        {
                            spawnData.Insert(k, bundle);
                            break;
                        }
                        else if (k == spawnData.Count - 1)
                        {
                            spawnData.Add(bundle);
                            break;
                        }
                    }
                }
            }
        }
    }

    private void InitializeSpawnPoints()
    {
        InitializeSpawnPoints(spawnPointsRoot, ref spawnPoints);
        InitializeSpawnPoints(questSpawnPointsRoot, ref questSpawnPoints);
    }

    private void InitializeSpawnPoints(Transform root, ref EnemySpawnPoint[] spawnPoints)
    {
        spawnPoints = root.GetComponentsInChildren<EnemySpawnPoint>();

        for (int i = 0; i < spawnPoints.Length; i++)
        {
            spawnPoints[i].Initialize(enemySpawnSettings.EnableSpawnPointGizmos, minSpawnPointDist, enemySpawnSettings.GizmosColor);
        }
    }

    private void InitializeEnemyPools()
    {
        int enemyPrefabsCount = enemyPrefabs.Length;
        enemyPools = new List<List<GameObject>>(enemyPrefabsCount);

        for (int i = 0; i < enemyPrefabsCount; i++)
        {
            enemyPools.Add(new List<GameObject>(enemyPoolSize));

            for (int j = 0; j < enemyPoolSize; j++)
            {
                AddEnemyToPool(i);
            }
        }

        activeEnemies = new List<EnemyController>();
    }

    private bool ReachedMaxEnemiesCount { get { return (maxEnemiesCount != null && activeEnemies.Count >= maxEnemiesCount.Value); } }

    private void ProcessNightPhase()
    {
        AttemptSpawn();
    }

    private void AttemptSpawn()
    {
        if (ReachedMaxEnemiesCount) return;

        // We spawned the last enemy already, return.
        if (currentSpawnDataIndex == spawnData.Count) return;

        float nightProgress = gameplayData.CurrentNightProgress;

        for (int i = currentSpawnDataIndex; i < spawnData.Count; i++)
        {
            NightSpawnSettings spawnSettings = spawnData[i].SpawnSettings;
            if (spawnSettings.SpawnTime < nightProgress)
            {
                for (int j = 0; j < spawnSettings.SpawnQuantity; j++)
                {
                    SpawnEnemy(spawnData[i].EnemyPrefabIndex);

                    if (ReachedMaxEnemiesCount)
                    {
                        currentSpawnDataIndex++;
                        return;
                    }
                }

                currentSpawnDataIndex++;
            }
            else
            {
                break;
            }
        }
    }

    private void SpawnEnemy(int enemyIndex)
    {
        GameObject enemy = GetInactiveEnemy(enemyIndex);

        if (enemy == null)
        {
            enemy = AddEnemyToPool(enemyIndex);
        }

        SpawnEnemy(enemy);
    }

    private void SpawnEnemy(GameObject enemy, EnemyType enemyType = EnemyType.Regular)
    {
        List<EnemySpawnPoint> points = new List<EnemySpawnPoint>();
        EnemySpawnPoint spawnPoint = null;

        if (enemyType == EnemyType.Tutorial)
        {
            spawnPoint = tutorialSpawnPoint;
        }
        else if (enemyType == EnemyType.Quest)
        {
            if (questSpawnPoints.Length == 0) return;

            int closestPointIndex = 0;
            float closestDistance = Vector3.SqrMagnitude(questPosition - questSpawnPoints[0].transform.position);

            for (int i = 0; i < questSpawnPoints.Length; i++)
            {
                float distance = Vector3.SqrMagnitude(questPosition - questSpawnPoints[i].transform.position);

                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    closestPointIndex = i;
                }
            }

            spawnPoint = questSpawnPoints[closestPointIndex];
        }
        else
        {
            for (int i = 0; i < spawnPoints.Length; i++)
            {
                if (Vector3.SqrMagnitude(playerTransform.position - spawnPoints[i].transform.position) > Mathf.Pow(minSpawnPointDist, 2f))
                {
                    points.Add(spawnPoints[i]);
                }
            }

            if (points.Count > 0)
                spawnPoint = points[Random.Range(0, points.Count)];
            else
                return;
        }

        enemy.transform.position = spawnPoint.transform.position;
        enemy.SetActive(true);
        EnemyController enemyController = enemy.GetComponent<EnemyController>();
        enemyController.InitializeNewPatrolArea(spawnPoint.PatrolArea);

        if (enemyType == EnemyType.Regular)
            enemyController.OnClearedPatrolArea += OnRegularEnemyClearedPatrolArea;

        enemyController.OnDestroyed += OnEnemyDestroyed;
        enemyController.ResetEnemy();
        soundSystem.OnSoundTriggered += enemyController.OnSoundDetected;
        activeEnemies.Add(enemyController);
    }

    private GameObject AddEnemyToPool(int enemyIndex)
    {
        GameObject newEnemy = Instantiate(enemyPrefabs[enemyIndex], enemiesRoot);
        IPlayerDamageDealer playerDamageDealer = playerTransform.GetComponent<IPlayerDamageDealer>();
        newEnemy.GetComponent<EnemyController>().Initialize(playerDamageDealer);
        newEnemy.SetActive(false);
        enemyPools[enemyIndex].Add(newEnemy);
        return newEnemy;
    }

    private GameObject GetInactiveEnemy(int poolIndex)
    {
        for (int i = 0; i < enemyPools[poolIndex].Count; i++)
        {
            if (!enemyPools[poolIndex][i].activeInHierarchy)
            {
                return enemyPools[poolIndex][i].gameObject;
            }
        }

        return null;
    }

    private void OnEnemyClearedPatrolArea(PatrolArea area, EnemyController enemy, EnemySpawnPoint[] spawnPoints)
    {
        List<PatrolArea> patrolAreas = new List<PatrolArea>();

        for (int i = 0; i < spawnPoints.Length; i++)
        {
            if (spawnPoints[i].PatrolArea != area)
            {
                patrolAreas.Add(spawnPoints[i].PatrolArea);
            }
        }

        // If we have a single patrol area, reuse it.
        if (patrolAreas.Count == 0) { patrolAreas.Add(spawnPoints[0].PatrolArea); }

        PatrolArea newArea = patrolAreas[Random.Range(0, patrolAreas.Count)];
        enemy.InitializeNewPatrolArea(newArea);
    }

    private void OnRegularEnemyClearedPatrolArea(PatrolArea area, EnemyController enemy)
    {
        OnEnemyClearedPatrolArea(area, enemy, spawnPoints);
    }

    private void OnEnemyDestroyed(EnemyController enemy)
    {
        enemy.OnDestroyed -= OnEnemyDestroyed;
        soundSystem.OnSoundTriggered -= enemy.OnSoundDetected;
        activeEnemies.Remove(enemy);
    }

    private void DespawnAllEnemies()
    {
        for (int i = 0; i < activeEnemies.Count; i++)
        {
            activeEnemies[i].OnDestroyed -= OnEnemyDestroyed;
            soundSystem.OnSoundTriggered -= activeEnemies[i].OnSoundDetected;
            activeEnemies[i].gameObject.SetActive(false);
        }

        activeEnemies.Clear();
    }
    #endregion

    // TODO Remove serializable
    [System.Serializable]
    public class SpawnSettingsBundle
    {
        [SerializeField] private int enemyPrefabIndex = 0;
        [SerializeField] private NightSpawnSettings spawnSettings = null;

        public SpawnSettingsBundle(int enemyPrefabIndex, NightSpawnSettings spawnSettings)
        {
            this.enemyPrefabIndex = enemyPrefabIndex;
            this.spawnSettings = spawnSettings;
        }

        public int EnemyPrefabIndex { get { return enemyPrefabIndex; } }
        public NightSpawnSettings SpawnSettings { get { return spawnSettings; } }
    }
}