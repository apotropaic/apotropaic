using Apotropaic.Data;
using Apotropaic.Events;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    // TODO : Shouldn't reference object in scene.
    [SerializeField] private DialogComponent murmurDialog = null;
    [SerializeField] private GameObject tutorialMurmur = null;
    [SerializeField] private GameObject bag = null;

    [SerializeField] private PlayerData playerData = null;
    [SerializeField] private GameplayData gameplayData = null;
    [SerializeField] private TrapSpawnerData trapSpawnerData = null;

    [SerializeField] private Transform playerBoundsUpperRight = null;
    [SerializeField] private Transform playerBoundsLowerLeft = null;
    [SerializeField] private Transform tutorialSpawnPoint = null;

    [SerializeField] private SceneLoaderTemp sceneLoader = null;
    [SerializeField] private SceneSettings sceneSettings = null;

    [Header("Conversation indexes")]
    [SerializeField] private int nightTutorialIndex = 0;
    [SerializeField] private int postTrapPlacementIndex = 1;
    [SerializeField] private int postEnemyDefeatedIndex = 2;

    [SerializeField] private BasicTrapSettings tutorialTrap = null;

    private bool placedTrap = false;
    private bool enemyDefeated = false;
    private bool tutorialActive = false;
    private bool spawnedTutorialEnemy = false;

    // TODO refactor
    [SerializeField] private MenuClassifier nightMenuClassifier = null;

    // Cache
    private Vector3 upperRightBounds = Vector3.zero;
    private Vector3 lowerLeftBounds = Vector3.zero;
    private IInventoryTrapAccessor inventoryTrapAccessor = null;

    #region UNITY
    private void Start()
    {
        upperRightBounds = playerBoundsUpperRight.position;
        lowerLeftBounds = playerBoundsLowerLeft.position;
        inventoryTrapAccessor = playerData.InventoryTrapAccessor;
        playerData.transform.position = tutorialSpawnPoint.position;
        SubscribeToEvents();
    }

    private void OnDestroy()
    {
        UnsubscribeFromEvents();
    }

    private void Update()
    {
        if (!tutorialActive) return;

        ProcessPlayerPosition();
    }
    #endregion

    #region PUBLIC
    #endregion

    #region PRIVATE
    private void SubscribeToEvents()
    {
        GameplayEvents.OnNightTutorial += OnNightTutorialStarted;
        UIEvents.OnConversationEnded += OnConversationEnded;
        GameplayEvents.OnPlacedTrap += OnPlacedTrap;
        GameplayEvents.OnEnemyDied += OnEnemyDied;
    }

    private void UnsubscribeFromEvents()
    {
        GameplayEvents.OnNightTutorial -= OnNightTutorialStarted;
        UIEvents.OnConversationEnded -= OnConversationEnded;
        GameplayEvents.OnPlacedTrap -= OnPlacedTrap;
        GameplayEvents.OnEnemyDied -= OnEnemyDied;
    }

    private void OnNightTutorialStarted()
    {
        UIEvents.OnSetDialogPreviousMenu?.Invoke(nightMenuClassifier);
        inventoryTrapAccessor.AddTrap(tutorialTrap.UID, 0, 1);
        trapSpawnerData.CurrentTrapSelectionIndex = 0;
        trapSpawnerData.CurrentSelectedTrap = tutorialTrap;

        tutorialActive = true;
        tutorialMurmur.SetActive(true);
        bag.SetActive(false);
        murmurDialog.Interact(nightTutorialIndex);
        gameplayData.TimeOfDay = TimeOfDay.Night;
    }

    private void OnConversationEnded()
    {
        if (!tutorialActive) return;

        if (!placedTrap)
        {
            GameplayEvents.OnLockPlayerMovementInput?.Invoke(true);
        }
        else if (!spawnedTutorialEnemy && placedTrap)
        {
            GameplayEvents.OnSpawnTutorialEnemy?.Invoke();
            spawnedTutorialEnemy = true;
        }

        if (enemyDefeated)
        {
            UIEvents.OnSetDialogPreviousMenu?.Invoke(null);
            CoreEvents.OnNightTutorialEnded?.Invoke();
            gameplayData.FinishedNightTutorial = true;
            sceneLoader.LoadScene(sceneSettings.OfficesScene);
        }
    }

    private void ProcessPlayerPosition()
    {
        Vector3 currentPosition = playerData.transform.position;
        Vector3 newPos = currentPosition;

        if (currentPosition.x < lowerLeftBounds.x)
        {
            newPos.x = lowerLeftBounds.x;
            SnapPlayer(newPos);
        }
        else if (currentPosition.x > upperRightBounds.x)
        {
            newPos.x = upperRightBounds.x;
            SnapPlayer(newPos);
        }
        else if (currentPosition.z > upperRightBounds.z)
        {
            newPos.z = upperRightBounds.z;
            SnapPlayer(newPos);
        }
        else if (currentPosition.z < lowerLeftBounds.z)
        {
            newPos.z = lowerLeftBounds.z;
            SnapPlayer(newPos);
        }
    }

    private void SnapPlayer(Vector3 newPos)
    {
        playerData.transform.position = newPos;
    }

    private void OnPlacedTrap()
    {
        if (!tutorialActive) return;

        placedTrap = true;
        murmurDialog.Interact(postTrapPlacementIndex);
    }

    private void OnEnemyDied()
    {
        if (!tutorialActive) return;

        enemyDefeated = true;
        murmurDialog.Interact(postEnemyDefeatedIndex);
    }
    #endregion
}