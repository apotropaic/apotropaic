using UnityEngine;
using Apotropaic.Data;
using UnityEngine.SceneManagement;
using Apotropaic.Core;
using System.Collections.Generic;
using System.Linq;

public class TrapsSpawner : MonoBehaviour
{
    // TODO : Remove.
    //[SerializeField] private GameObject[] trapPrefabs = null;
    [SerializeField] private TrapSpawnerData trapSpawnerData = null;
    [SerializeField] private PlayerInventory playerInventory = null;
    [SerializeField] private TrapSettingsList trapList = null;
    private bool initialized = false;

    private GameObject trapsRoot = null;

    #region UNITY
    private void Awake()
    {
        //trapSpawnerData.TrapsCount = trapPrefabs.Length;

        if (SceneLoader.isValidSingleton())
            SceneLoader.Instance.onSceneLoadedEvent.AddListener(OnSceneLoaded);
    }

    private void OnDestroy()
    {
        if (SceneLoader.isValidSingleton())
            SceneLoader.Instance.onSceneLoadedEvent.RemoveListener(OnSceneLoaded);
    }
    #endregion

    #region PUBLIC
    // TODO : Spawn trap by ID.
    public ITrap SpawnTrap(Vector3 spawnPos)
    {
        return SpawnTrap(trapSpawnerData.CurrentSelectedTrap.Prefab, spawnPos);
    }
    #endregion

    #region PRIVATE
    private void Initialize()
    {

        for (int i = 0; i < trapList.Traps.Length; i++)
        {
            if (playerInventory.TrapList.Count > trapSpawnerData.CurrentTrapSelectionIndex)
            {
                if (trapList.Traps[i].UID == playerInventory.TrapList.ElementAt(trapSpawnerData.CurrentTrapSelectionIndex).Key)
                    trapSpawnerData.CurrentSelectedTrap = trapList.Traps[i];
            }
        }


        trapsRoot = Instantiate(new GameObject());
        trapsRoot.name = "Traps root";
        initialized = true;
    }

    private void OnSceneLoaded(List<string> scene)
    {
        Initialize();
    }

    private ITrap SpawnTrap(GameObject prefab, Vector3 spawnPos)
    {
        if (!initialized) Initialize();

        GameObject newTrap = Instantiate(prefab, trapsRoot.transform);
        newTrap.transform.position = spawnPos;
        ITrap trap = newTrap.GetComponent<ITrap>();
        trap.SetCurrentState(TrapState.Placement);
        return trap;
    }
    #endregion
}