using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] Text score = null;
    [SerializeField] GameObject winPanel = null;

    public void UpdateScore(int _score)
    {
        score.text = "Score: " + _score.ToString();
    }

    public void EnableWinPanel(bool _enable)
    {
        winPanel.SetActive(_enable);
    }
}
