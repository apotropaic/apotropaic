using UnityEngine;
using Apotropaic.Data;
using Apotropaic.UI;

public class OfficesManager : SceneInitializer
{
    [SerializeField] private SceneLoaderTemp sceneLoader = null;
    [SerializeField] private SceneSettings sceneSettings = null;
    [SerializeField] private GameplayData gameplayData = null;

    [SerializeField] private GameObject murmurObject = null;

    #region PUBLIC
    public void StartNightPhase()
    {
        menuManager.HideMenu(MenuClassifier);
        sceneLoader.LoadScene(sceneSettings.LibraryScene);
    }
    public override void Initialize(IMenuManager manager)
    {
        base.Initialize(manager);
    }


    private void Awake()
    {
        gameplayData.ConversationsThisDay = 0;
    }
    #endregion

    #region UNITY
    private void Start()
    {
        murmurObject.GetComponent<NightTrigger>().SetOfficesManager(this);
        murmurObject.SetActive(false);
    }

    private void Update()
    {
        if ((gameplayData.ConversationsThisDay == gameplayData.ConversationsPerDay || gameplayData.EnableOfficeMurmur) && !murmurObject.activeInHierarchy)
            murmurObject.SetActive(true);
    }
    #endregion
}