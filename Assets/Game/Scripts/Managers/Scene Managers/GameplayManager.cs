using Apotropaic.UI;
using System;
using UnityEngine;

public class GameplayManager : SceneInitializer
{
    [SerializeField] private GameObject murmurShop = null;
    [SerializeField] private GameObject murmurMorning = null;

    [SerializeField] private GameplayManagerFSM fsm = null;
    [SerializeField] private MenuClassifier nightMenuClassifier = null;
    [SerializeField] private MenuClassifier eveningMenuClassifier = null;
    [SerializeField] private MenuClassifier morningMenuClassifier = null;
    [SerializeField] private MenuClassifier gameoverMenuClassifier = null;
    [SerializeField] private MenuClassifier dialogMenuClassifier = null;

    private int? currentStateHashCode = null;

    #region UNITY
    private void Start()
    {
        fsm.OnStateChanged += OnStateChanged;
    }

    private void OnDestroy()
    {
        fsm.OnStateChanged -= OnStateChanged;
    }
    #endregion

    #region PUBLIC
    public void EnableNightMenu()
    {
        HideAllMenus();
        menuManager.ShowMenu(nightMenuClassifier);
    }

    public void EnableEveningMenu()
    {
        HideAllMenus();
        menuManager.ShowMenu(eveningMenuClassifier);
    }

    public void EnableQuestDialog()
    {
        HideAllMenus();
        menuManager.ShowMenu(dialogMenuClassifier);
    }

    public void EnableMorningMenu()
    {
        HideAllMenus();
        menuManager.ShowMenu(morningMenuClassifier);
    }

    public void EnableGameoverMenu()
    {
        HideAllMenus();
        menuManager.ShowMenu(gameoverMenuClassifier);
    }

    public void EnableDialogMenu()
    {
        HideAllMenus();
        menuManager.ShowMenu(dialogMenuClassifier);
    }

    public void DisableAllMenus()
    {
        HideAllMenus();
    }

    public void OnStateChanged(int stateHashCode)
    {
        currentStateHashCode = stateHashCode;
    }

    public void EnableShop(bool enable)
    {
        murmurShop.SetActive(enable);
    }

    public void EnableMorningMurmur(bool enable)
    {
        murmurMorning.SetActive(enable);
    }

    public int? CurrentStateHashCode { get { return currentStateHashCode; } }
    #endregion

    #region PRIVATE
    private void HideAllMenus()
    {
        menuManager.HideMenu(nightMenuClassifier);
        menuManager.HideMenu(eveningMenuClassifier);
        menuManager.HideMenu(morningMenuClassifier);
        menuManager.HideMenu(gameoverMenuClassifier);
        menuManager.HideMenu(dialogMenuClassifier);
    }
    #endregion
}