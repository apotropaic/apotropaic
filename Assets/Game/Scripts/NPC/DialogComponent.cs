using EasyButtons;
using System.Collections.Generic;
using UnityEngine;
using Apotropaic.Events;

public class DialogComponent : MonoBehaviour, IInteractable
{
    [SerializeField] protected NPCData npcData;
    [SerializeField] protected GameplayData gameplayData;
    [SerializeField] GameObject dialogIndicator;
    [SerializeField] GameObject interactIndicator;
    // TODO refactor
    [SerializeField] protected MenuClassifier previousMenuClassifier;

    protected List<ConversationData> openConversations = new List<ConversationData>();
    protected int ConvLimit;

    public virtual void Start()
    {
        ConvLimit = gameplayData.ConversationsPerDay;
        UIEvents.OnConversationEnded += RefreshUI;
        UIEvents.OnConversationEnded += ResetInteractIndicator;

    }

    private void Awake()
    {
        RefreshUI();
        if (interactIndicator != null)
            interactIndicator.SetActive(false);
    }

    private void OnDestroy()
    {
        UIEvents.OnConversationEnded -= RefreshUI;
        UIEvents.OnConversationEnded -= ResetInteractIndicator;
    }

    // generic interact for conversations in the office
    [Button]
    public virtual void Interact()
    {
        if (previousMenuClassifier != null)
            UIEvents.OnSetDialogPreviousMenu?.Invoke(previousMenuClassifier);

        CalculateOpenConversations();
        // Start Conversation if there are available conversations
        if (openConversations.Count != 0)
        {
            if (gameplayData.ConversationsThisDay < ConvLimit)
            {
                UIEvents.OnConversationStarted?.Invoke(openConversations, npcData.GetSprite);
            }
        }
        else
        {
            // Do nothing
        }
    }

    public void Interact(int conversationIndex)
    {
        openConversations.Clear();
        openConversations.Add(npcData.PersonalConversationList[conversationIndex]);
        UIEvents.OnConversationStarted?.Invoke(openConversations, npcData.GetSprite);
    }

    public void Interact(int[] conversationIndexes)
    {
        openConversations.Clear();

        for (int i = 0; i < conversationIndexes.Length; i++)
        {
            openConversations.Add(npcData.PersonalConversationList[conversationIndexes[i]]);
        }

        UIEvents.OnConversationStarted?.Invoke(openConversations, npcData.GetSprite);
    }

    // Reset all talked flags. will be applied by the save system on load.
    private void OnApplicationQuit()
    {
        if (npcData == null || npcData.PersonalConversationList == null) return;

        for (int i = 0; i < npcData.PersonalConversationList.Length; i++)
        {
            npcData.PersonalConversationList[i].Talked = false;
        }
        for (int i = 0; i < npcData.TrapConversationList.Length; i++)
        {
            npcData.TrapConversationList[i].Talked = false;
        }
    }

    private void ResetInteractIndicator()
    {
        RefreshUI();
    }


    // Evaluate NPC data and  compile a list of available conversations for the dialog menu
    protected void CalculateOpenConversations()
    {
        openConversations.Clear();
        if (npcData.PersonalConversationList != null)
        {
            for (int i = 0; i < npcData.PersonalConversationList.Length; i++)
            {
                if (npcData.PersonalConversationList[i].UnlockDay <= gameplayData.NightsCleared && !npcData.PersonalConversationList[i].Talked)
                {
                    openConversations.Add(npcData.PersonalConversationList[i]);
                    break;
                }
            }
        }

        for (int i = 0; i < npcData.TrapConversationList.Length; i++)
        {
            if (npcData.TrapConversationList[i].UnlockDay <= gameplayData.NightsCleared && !npcData.TrapConversationList[i].Talked)
            {
                openConversations.Add(npcData.TrapConversationList[i]);
            }
        }
    }
    // refresh if NPC has available conversations and update Offices scene
    private void RefreshUI()
    {
        CalculateOpenConversations();
        if (openConversations.Count != 0)
        {
            if (dialogIndicator != null)
                dialogIndicator.SetActive(true);
        }
        else
        {
            if (dialogIndicator != null)
                dialogIndicator.SetActive(false);
        }
    }

    // Enable or Disable indicators in offices if they are interactable or not.
    private void OnTriggerEnter(Collider other)
    {
        CalculateOpenConversations();
        if (openConversations.Count != 0)
        {
            if (interactIndicator != null)
                interactIndicator.SetActive(true);
        }
        else
        {
            if (interactIndicator != null)
                interactIndicator.SetActive(false);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (interactIndicator != null)
            interactIndicator.SetActive(false);

    }
}
