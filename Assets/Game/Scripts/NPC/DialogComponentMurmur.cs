using UnityEngine;
using Apotropaic.Events;

public class DialogComponentMurmur : DialogComponent
{
    #region UNITY
    public override void Start()
    {
    }
    #endregion
    // override to exclude Murmur from conversation count limit for the day
    #region PUBLIC
    public override void Interact()
    {
        if (previousMenuClassifier != null)
            UIEvents.OnSetDialogPreviousMenu?.Invoke(previousMenuClassifier);

        CalculateOpenConversations();

        if (openConversations.Count != 0)
        {
            gameplayData.ConversationsThisDay--;
            UIEvents.OnConversationStarted?.Invoke(openConversations, npcData.GetSprite);
        }
        else
        {
            // Do Nothing
        }
    }

    #endregion

    #region PRIVATE
    #endregion
}
