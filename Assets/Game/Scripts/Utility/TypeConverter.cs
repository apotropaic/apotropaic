namespace Apotropaic.Utility
{
    public class TypeConverter
    {
        #region PUBLIC
        public static T[] ConvertObjectArray<T>(object[] values)
        {
            T[] convertedVals = new T[values.Length];

            for (int i = 0; i < values.Length; i++)
            {
                convertedVals[i] = (T)values[i];
            }

            return convertedVals;
        }
        #endregion
    }
}