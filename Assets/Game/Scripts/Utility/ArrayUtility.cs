namespace Apotropaic.Utility
{
    public static class ArrayUtility
    {
        #region PUBLIC
        public static T[] CopyArray<T>(T[] arrayToCopy)
        {
            T[] copy = new T[arrayToCopy.Length];

            for (int i = 0; i < copy.Length; i++)
            {
                copy[i] = arrayToCopy[i];
            }

            return copy;
        }
        #endregion
    }
}