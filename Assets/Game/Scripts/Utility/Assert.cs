using UnityEngine;

namespace Apotropaic.Utility
{
    public class Assert : MonoBehaviour
    {
        #region STATIC
        public static bool Validate(bool condition, string message)
        {
            Debug.Assert(condition, message);

            return !condition;
        }
        #endregion
    }
}