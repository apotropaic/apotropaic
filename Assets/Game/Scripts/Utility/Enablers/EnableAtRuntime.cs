using UnityEngine;

namespace Apotropaic.Utility
{
    public abstract class EnableAtRuntime : MonoBehaviour
    {
        [SerializeField] protected bool enable = true;

        #region UNITY
        private void Awake()
        {
            Enable();
        }
        #endregion

        #region PROTECTED
        protected abstract void Enable();
        #endregion
    }
}