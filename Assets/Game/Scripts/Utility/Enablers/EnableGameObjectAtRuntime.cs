namespace Apotropaic.Utility
{
    public class EnableGameObjectAtRuntime : EnableAtRuntime
    {
        #region PROTECTED
        protected override void Enable()
        {
            gameObject.SetActive(enable);
        }
        #endregion
    }
}