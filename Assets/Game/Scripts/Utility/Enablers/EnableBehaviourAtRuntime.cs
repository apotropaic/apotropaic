using UnityEngine;

namespace Apotropaic.Utility
{
    public class EnableBehaviourAtRuntime : EnableAtRuntime
    {
        [SerializeField] private Behaviour behaviour;

        #region PROTECTED
        protected override void Enable()
        {
            behaviour.enabled = enable;
        }
        #endregion
    }
}