using UnityEngine;

namespace Apotropaic.Utility
{
    public class EnableRendererAtRuntime : EnableAtRuntime
    {
        [SerializeField] private new Renderer renderer = null;

        #region PROTECTED
        protected override void Enable()
        {
            renderer.enabled = enable;
        }
        #endregion
    }
}