// https://answers.unity.com/questions/1004666/change-material-rendering-mode-in-runtime.html
// Answer by bellicapax

using UnityEngine;

namespace Apotropaic.Utility
{
    public static class StandardShaderUtils
    {
        private static bool initialized = false;

        private static int srcBlend = 0;
        private static int dstBlend = 0;
        private static int zWrite = 0;
        private static string alphaTest;
        private static string alphaBlend;
        private static string alphaPreMultiply;


        public enum BlendMode
        {
            Opaque,
            Cutout,
            Fade,
            Transparent
        }

        public static void Initialize()
        {
            if (initialized) return;

            srcBlend = Shader.PropertyToID("_SrcBlend");
            dstBlend = Shader.PropertyToID("_DstBlend");
            zWrite = Shader.PropertyToID("_ZWrite");
            alphaTest = "_ALPHATEST_ON";
            alphaBlend = "_ALPHABLEND_ON";
            alphaPreMultiply = "_ALPHAPREMULTIPLY_ON";

            initialized = true;
        }

        public static void ChangeRenderMode(Material standardShaderMaterial, BlendMode blendMode)
        {
            if (!initialized)
            {
                Debug.LogError("StandardShaderUtils must be initialized before usage.");
                return;
            }

            switch (blendMode)
            {
                case BlendMode.Opaque:
                    standardShaderMaterial.SetInt(srcBlend, (int)UnityEngine.Rendering.BlendMode.One);
                    standardShaderMaterial.SetInt(dstBlend, (int)UnityEngine.Rendering.BlendMode.Zero);
                    standardShaderMaterial.SetInt(zWrite, 1);
                    standardShaderMaterial.DisableKeyword(alphaTest);
                    standardShaderMaterial.DisableKeyword(alphaBlend);
                    standardShaderMaterial.DisableKeyword(alphaPreMultiply);
                    standardShaderMaterial.renderQueue = -1;
                    break;
                case BlendMode.Cutout:
                    standardShaderMaterial.SetInt(srcBlend, (int)UnityEngine.Rendering.BlendMode.One);
                    standardShaderMaterial.SetInt(dstBlend, (int)UnityEngine.Rendering.BlendMode.Zero);
                    standardShaderMaterial.SetInt(zWrite, 1);
                    standardShaderMaterial.EnableKeyword(alphaTest);
                    standardShaderMaterial.DisableKeyword(alphaBlend);
                    standardShaderMaterial.DisableKeyword(alphaPreMultiply);
                    standardShaderMaterial.renderQueue = 2450;
                    break;
                case BlendMode.Fade:
                    standardShaderMaterial.SetInt(srcBlend, (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                    standardShaderMaterial.SetInt(dstBlend, (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                    standardShaderMaterial.SetInt(zWrite, 0);
                    standardShaderMaterial.DisableKeyword(alphaTest);
                    standardShaderMaterial.EnableKeyword(alphaBlend);
                    standardShaderMaterial.DisableKeyword(alphaPreMultiply);
                    standardShaderMaterial.renderQueue = 3000;
                    break;
                case BlendMode.Transparent:
                    standardShaderMaterial.SetInt(srcBlend, (int)UnityEngine.Rendering.BlendMode.One);
                    standardShaderMaterial.SetInt(dstBlend, (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                    standardShaderMaterial.SetInt(zWrite, 0);
                    standardShaderMaterial.DisableKeyword(alphaTest);
                    standardShaderMaterial.DisableKeyword(alphaBlend);
                    standardShaderMaterial.EnableKeyword(alphaPreMultiply);
                    standardShaderMaterial.renderQueue = 3000;
                    break;
            }

        }
    }
}