using System;

namespace Apotropaic.Utility
{
    public class Comparison
    {
        #region PUBLIC
        public static bool Compare(bool comparisonVal, bool[] values, Operator oprt)
        {
            int compVal = comparisonVal ? 1 : 0;
            int[] vals = new int[values.Length];

            for (int i = 0; i < values.Length; i++)
            {
                vals[i] = values[i] ? 1 : 0;
            }

            return Compare(compVal, vals, oprt);
        }

        public static bool Compare(Enum comparisonVal, Enum[] values, Operator oprt)
        {
            int compVal = Convert.ToInt32(comparisonVal);
            int[] vals = new int[values.Length];

            for (int i = 0; i < values.Length; i++)
            {
                vals[i] = Convert.ToInt32(values[i]);
            }

            return Compare(compVal, vals, oprt);
        }

        public static bool Compare(int comparisonVal, int[] values, Operator oprt)
        {
            switch (oprt)
            {
                case Operator.AND:
                    for (int i = 0; i < values.Length; i++)
                    {
                        if (comparisonVal != values[i]) return false;
                    }
                    return true;
                case Operator.OR:
                    for (int i = 0; i < values.Length; i++)
                    {
                        if (comparisonVal == values[i]) return true;
                    }
                    return false;
                case Operator.NOT:
                    for (int i = 0; i < values.Length; i++)
                    {
                        if (comparisonVal == values[i]) return false;
                    }
                    return true;
            }

            return false;
        }
        #endregion

        #region PRIVATE

        #endregion
    }
}