using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FSM : MonoBehaviour
{
    public Action<int> OnStateChanged = null;

    [SerializeField] private RuntimeAnimatorController FSMController;
    [SerializeField] private bool keepAnimatorControllerStateOnDisable = true;
    [SerializeField] protected List<StateClassifier> stateClassifiers = null;

    public Animator fsmAnimator { get; private set; }

    private readonly int AnyLayer = 0;

    private int currentState = 0;

    protected int[] stateHashCodes = null;

    private bool initialized = false;
    private bool transitioning = false;
    private Coroutine transitionRoutine = null;

    #region UNITY
    private void OnEnable()
    {
        if (initialized) return;

        // Create a FSM GameObject and set this as a child to the gameObject
        GameObject FSMGO = new GameObject("FSM", typeof(Animator));
        FSMGO.transform.parent = transform;

        // Get the animator from the FSMGO
        fsmAnimator = FSMGO.GetComponent<Animator>();
        fsmAnimator.runtimeAnimatorController = FSMController;
        fsmAnimator.keepAnimatorControllerStateOnDisable = keepAnimatorControllerStateOnDisable;

        // Hide the Animator (optional)
        fsmAnimator.hideFlags = HideFlags.HideInInspector;

        // Iterate over all states in the controller and set the owner
        StateMachineBehaviour[] behaviours = fsmAnimator.GetBehaviours<StateMachineBehaviour>();
        foreach (var behaviour in behaviours)
        {
            IInitializableState state = (IInitializableState)behaviour;

            if (state != null)
            {
                state.Initialize(gameObject, this);
            }
        }

        InitializeHashCodes();

        initialized = true;
    }
    #endregion

    #region PUBLIC
    public bool ChangeState(string stateName)
    {
        currentState = Animator.StringToHash(stateName);
        return ChangeState(currentState);
    }

    public int GetCurrentState()
    {
        return currentState;
    }

    public bool ChangeState(int hashStateName)
    {
        if (!gameObject.activeInHierarchy) return false;
        if (currentState == hashStateName) return true;

        bool hasState = true;
#if UNITY_EDITOR
        hasState = fsmAnimator.HasState(AnyLayer, hashStateName);
#endif

        if (fsmAnimator.gameObject.activeInHierarchy)
        {
            fsmAnimator.CrossFade(hashStateName, 0.0f, AnyLayer);
        }

        if (transitionRoutine != null) StopCoroutine(transitionRoutine);
        transitionRoutine = StartCoroutine(ProcessTransition());

        currentState = hashStateName;
        return hasState;
    }

    public bool IsTransitioning { get { return transitioning; } }
    #endregion

    #region PROTECTED
    protected virtual void InitializeHashCodes()
    {
        stateHashCodes = new int[stateClassifiers.Count];
        for (int i = 0; i < stateHashCodes.Length; i++)
        {
            stateHashCodes[i] = Animator.StringToHash(stateClassifiers[i].Classifier);
        }
    }
    #endregion

    #region PRIVATE
    private IEnumerator ProcessTransition()
    {
        transitioning = true;

        yield return new WaitForEndOfFrame();

        transitioning = false;

        if (OnStateChanged != null)
            OnStateChanged(currentState);
    }
    #endregion
}
