using UnityEngine;

public interface IInitializableState
{
    public void Initialize(GameObject _owner, FSM _fsm);

    public StateClassifier Classifier { get; }
}