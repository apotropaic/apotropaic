using UnityEngine;

[CreateAssetMenu(fileName = "State Classifier", menuName = "Scriptable Object/State Classifier", order = 0)]
public class StateClassifier : ScriptableObject
{
    [SerializeField] private string stateClassifier = "";

    public string Classifier { get { return stateClassifier; } }
}