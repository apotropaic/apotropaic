using UnityEngine;

public abstract class FSMBaseState<T> : StateMachineBehaviour, IInitializableState where T : FSM
{
    public StateClassifier Classifier { get { return classifier; } }

    [SerializeField] private StateClassifier classifier = null;
    protected T fsm { get; private set; }
    protected GameObject owner { get; private set; }

    public virtual void Initialize(GameObject _owner, FSM _fsm)
    {
        owner = _owner;
        fsm = (T)_fsm;
    }
}
