using Apotropaic.Events;
using System.Collections.Generic;
using UnityEngine;

public class QuestManager : MonoBehaviour
{
    [SerializeField] private QuestSystemData questSystemData = null;
    [SerializeField] private GameplayData gameplayData = null;
    [SerializeField] private MenuClassifier previousMenuClassifier = null;
    [SerializeField] private GameObject bookPrefab = null;
    [SerializeField] private GameObject wayPointParent = null;

    private List<Transform> wayPointTransforms = new List<Transform>();
    private GameObject bookSpawn = null;
    private IPauseEventsInvoker pauseEventsInvoker = null;

    [SerializeField] private Sprite murmurSprite;

    private QuestData currentQuestData = null;

    #region UNITY
    private void Start()
    {
        GameplayEvents.OnEveningEnter += EveningStart;

        pauseEventsInvoker = GetComponent<IPauseEventsInvoker>();

        foreach (Transform transform in wayPointParent.transform)
        {
            wayPointTransforms.Add(transform);
        }
    }

    private void OnDestroy()
    {
        GameplayEvents.OnEveningEnter -= EveningStart;

    }
    // Keeps track of spawingin in the current quest 
    private void Update()
    {
        if (currentQuestData != null)
        {
            if (gameplayData.CurrentNightProgress > currentQuestData.SpawnTime && bookSpawn == null)
            {
                bookSpawn = Instantiate(bookPrefab, currentQuestData.DecidedLocation, currentQuestData.Rotation);
                bookSpawn.GetComponent<QuestItem>().SetCurrentData(currentQuestData);
                GameplayEvents.OnBookSpawned?.Invoke(currentQuestData.DecidedLocation);
            }
            if (gameplayData.CurrentNightProgress > currentQuestData.EndTime && bookSpawn != null)
            {
                bookSpawn.gameObject.SetActive(false);
            }
        }
    }
    #endregion

    #region PUBLIC
    #endregion

    #region PRIVATE
    // Tracks and updates active Quest
    //
    // TODO : Requires refactoring. reduntant code.
    private void EveningStart()
    {
        if(currentQuestData == null)
        {
            currentQuestData = questSystemData.QuestDatas[questSystemData.CurrentActiveQuest];

            //currentQuestData.IsActive = f;
        }
        if(currentQuestData.IsActive == false)
        {
            currentQuestData.IsActive = true;

            int randomno = Random.Range(0, wayPointTransforms.Count);
            currentQuestData.DecidedLocation = wayPointTransforms[randomno].position;
            currentQuestData.Rotation = wayPointTransforms[randomno].rotation;
            
            List<ConversationData> openConversations = new List<ConversationData>();
            openConversations.Add(currentQuestData.StartConversation);
            UIEvents.OnConversationStarted?.Invoke(openConversations, murmurSprite);
            UIEvents.OnSetDialogPreviousMenu?.Invoke(previousMenuClassifier);
            UIEvents.OnConversationEnded += ConversationEnded;
            Pause();
        }
        else if(currentQuestData.ObjectiveComplete)
        {
            currentQuestData.IsActive = false;
            questSystemData.CurrentActiveQuest = questSystemData.CurrentActiveQuest + 1;

            if (questSystemData.CurrentActiveQuest >= questSystemData.QuestDatas.Length)
            {
                currentQuestData = null;
                List<ConversationData> openConversations = new List<ConversationData>();
                openConversations.Add(questSystemData.EndQuestSuccessConversation);
                UIEvents.OnConversationStarted?.Invoke(openConversations, murmurSprite);
                UIEvents.OnSetDialogPreviousMenu?.Invoke(previousMenuClassifier);
                UIEvents.OnConversationEnded += ConversationEnded;
                Pause();
            }
            else
            {
                currentQuestData = questSystemData.QuestDatas[questSystemData.CurrentActiveQuest];
                currentQuestData.IsActive = true;

                int randomno = Random.Range(0, wayPointTransforms.Count);
                currentQuestData.DecidedLocation = wayPointTransforms[randomno].position;
                currentQuestData.Rotation = wayPointTransforms[randomno].rotation;

                List<ConversationData> openConversations = new List<ConversationData>();
                openConversations.Add(currentQuestData.StartConversation);
                UIEvents.OnConversationStarted?.Invoke(openConversations, murmurSprite);
                UIEvents.OnSetDialogPreviousMenu?.Invoke(previousMenuClassifier);
                UIEvents.OnConversationEnded += ConversationEnded;
                Pause();
            }
        }
        else
        {
            List<ConversationData> openConversations = new List<ConversationData>();
            openConversations.Add(currentQuestData.FailureConversations[0]);
            UIEvents.OnConversationStarted?.Invoke(openConversations, murmurSprite);
            UIEvents.OnSetDialogPreviousMenu?.Invoke(previousMenuClassifier);
            UIEvents.OnConversationEnded += ConversationEnded;
            Pause();

        }
    }
    // Pause and resume game as needed during conversations.
    private void ConversationEnded()
    {
        Resume();
        UIEvents.OnConversationEnded -= ConversationEnded;
    }

    private void Pause()
    {
        Time.timeScale = 0f;
        pauseEventsInvoker.Pause();
    }

    private void Resume()
    {
        Time.timeScale = 1f;
        pauseEventsInvoker.Resume();
    }

    private void resetPreviousMenuClassifier()
    {
        UIEvents.OnSetDialogPreviousMenu?.Invoke(null);
    }

    #endregion
}
