using UnityEngine;

// script ont he quest pick up, updates the quest status to objective completed.

public class QuestItem : MonoBehaviour
{
    private QuestData currentQuestData;
    #region UNITY
    private void Start()
    {
        
    }
    #endregion

    #region PUBLIC
    public void SetCurrentData(QuestData _questData)
    {
        currentQuestData = _questData;
    }
    #endregion

    #region PRIVATE
    private void OnTriggerEnter(Collider other)
    {
        if(LayerMask.LayerToName(other.gameObject.layer) == "PlayerTrigger")
        {
            currentQuestData.ObjectiveComplete = true;
            gameObject.SetActive(false);
        }
    }
    #endregion
}
