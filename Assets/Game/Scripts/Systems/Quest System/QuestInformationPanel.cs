using EasyButtons;
using UnityEngine;
using UnityEngine.UI;
using Apotropaic.Events;


// Updates the Quest information Panel in the gameplay for hints
public class QuestInformationPanel : MonoBehaviour
{
    private QuestData questData = null;
    [SerializeField] private GameObject label = null;
    [SerializeField] private GameObject informationText = null;
    [SerializeField] private string waitForSpawnText;
    [SerializeField] private string findBookText;
    [SerializeField] private string afterBookPickedText;

    int flag = 0;
    #region UNITY
    private void Start()
    {
        flag = 0;
        GameplayEvents.OnBookSpawned += OnBookSpawn;
    }

    private void Update()
    {
        Refresh();
    }

    private void OnDestroy()
    {
        GameplayEvents.OnBookSpawned -= OnBookSpawn;
    }
    #endregion

    #region PUBLIC
    public void UpdateInfo(QuestData _questData)
    {
        questData = _questData;
    }
    #endregion

    #region PRIVATE
    private void OnBookSpawn(Vector3 _spawnLoc)
    {
        flag = 1;
        UpdateInfo(questData);
    }

    private void Refresh()
    {
        if (label != null && informationText != null)
        {
            if (questData == null)
            {
                label.SetActive(false);
                informationText.SetActive(false);
            }
            else if (!questData.ObjectiveComplete && flag == 0)
            {
                label.SetActive(true);
                informationText.SetActive(true);
                informationText.GetComponent<Text>().text = waitForSpawnText;
            }
            else if (!questData.ObjectiveComplete && flag == 1)
            {
                label.SetActive(true);
                informationText.SetActive(true);
                informationText.GetComponent<Text>().text = findBookText;
            }
            else if (questData.ObjectiveComplete)
            {
                label.SetActive(true);
                informationText.SetActive(true);
                informationText.GetComponent<Text>().text = afterBookPickedText;
            }
        }
    }
    #endregion
}
