﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using Apotropaic.UI;
using Apotropaic.Data;

namespace Apotropaic.Core
{
    public class SceneLoader : Singleton<SceneLoader>
    {
        [Header("Settings")]
        [SerializeField] private PlaytestSettings playtestSettings = null;

        [Space(20)]
        // Settings
        private float delayTime = 1.0f;

        [HideInInspector] public SceneLoadedEvent onSceneLoadedEvent = new SceneLoadedEvent();
        [SerializeField] private MenuManager menuManager = null;

        [SerializeField] private MenuClassifier loadingMenuClassifier;

        private List<string> loadedScenes = new List<string>();

        private string mainScene = "";

        [System.Serializable]
        public class SceneLoadedEvent : UnityEvent<List<string>> { }

        private void Awake()
        {
            LoadSettings();
        }

        private void LoadSettings()
        {
            delayTime = playtestSettings.LoadingTime;
        }

        // When loading just add a flag for persistence. If true don't add to the loadedScenes
        // Only remove the scenes when you unload

        public void LoadScene(string scene, bool isMainScene = false, bool showLoadingScreen = true)
        {
            StartCoroutine(loadScene(scene, isMainScene, showLoadingScreen, true));
        }

        public void LoadScenes(List<string> scenes, bool showLoadingScreen = true)
        {
            StartCoroutine(loadScenes(scenes, showLoadingScreen));
        }

        IEnumerator loadScenes(List<string> scenes, bool showLoadingScreen)
        {
            if (showLoadingScreen)
            {
                menuManager.ShowMenu(loadingMenuClassifier);
            }

            foreach (string scene in scenes)
            {
                yield return StartCoroutine(loadScene(scene, false, false, false));
            }

            if (showLoadingScreen)
            {
                menuManager.HideMenu(loadingMenuClassifier);
            }

            loadedScenes.Clear();
            loadedScenes.AddRange(scenes);
            onSceneLoadedEvent.Invoke(loadedScenes);
        }

        IEnumerator loadScene(string _scene, bool isMainScene, bool showLoadingScreen, bool raiseEvent)
        {
            Scene scene = SceneManager.GetSceneByPath(_scene);

            if (scene.isLoaded == false)
            {
                if (showLoadingScreen)
                {
                    menuManager.ShowMenu(loadingMenuClassifier);
                }

                yield return new WaitForSeconds(delayTime);

                AsyncOperation sync;

                Application.backgroundLoadingPriority = ThreadPriority.Low;

                sync = SceneManager.LoadSceneAsync(_scene, LoadSceneMode.Additive);

                while (sync.isDone == false) { yield return null; }

                scene = SceneManager.GetSceneByPath(_scene);
                SceneManager.SetActiveScene(scene);

                Application.backgroundLoadingPriority = ThreadPriority.Normal;

                yield return new WaitForSeconds(delayTime);

                if (isMainScene)
                {
                    mainScene = _scene;
                }

                if (showLoadingScreen)
                {
                    menuManager.HideMenu(loadingMenuClassifier);
                }
            }

            if (raiseEvent)
            {
                loadedScenes.Clear();
                loadedScenes.Add(_scene);
                onSceneLoadedEvent.Invoke(loadedScenes);
            }
        }

        // 4 Methods:
        //	- Unload single scene
        //	- Unload list of scenes
        //		- Support to unload multiple (Coroutine)
        //	- Actual Unload of scenes.

        public void UnloadScene(string scene)
        {
            StartCoroutine(unloadScene(scene));
        }

        public void UnloadMainScene()
        {
            if (string.IsNullOrEmpty(mainScene))
            {
                Debug.LogError("No main scene was assigned.");
                return;
            }

            StartCoroutine(unloadScene(mainScene));
            mainScene = "";
        }

        public void UnloadScenes(List<string> scenes)
        {
            StartCoroutine(unloadScenes(scenes));
        }

        IEnumerator unloadScenes(List<string> scenes)
        {
            foreach (string scene in scenes)
            {
                yield return StartCoroutine(unloadScene(scene));
            }
        }

        IEnumerator unloadScene(string scene)
        {
            AsyncOperation sync = null;

            try
            {
                sync = SceneManager.UnloadSceneAsync(scene);
            }
            catch (Exception ex)
            {
                Debug.Log(ex.Message);
            }

            if (sync != null)
            {
                while (sync.isDone == false) { yield return null; }
            }

            sync = Resources.UnloadUnusedAssets();
            while (sync.isDone == false) { yield return null; }
        }

        public T[] GetSceneComponents<T>(string scenePath)
        {
            List<T> components = new List<T>();

            Scene scene = SceneManager.GetSceneByPath(scenePath);

            if (scene == null)
                return null;

            GameObject[] sceneObjets = scene.GetRootGameObjects();

            for (int i = 0; i < sceneObjets.Length; i++)
            {
                T component = sceneObjets[i].GetComponentInChildren<T>(true);

                if (component != null)
                {
                    components.Add(component);
                }
            }

            return components.ToArray();
        }
    }
}