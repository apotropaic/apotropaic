using UnityEngine;
using Apotropaic.UI;

public abstract class SceneInitializer : MonoBehaviour, IMenuReader
{
    [SerializeField] private MenuClassifier menuClassifier;
    [SerializeField] private bool showDefaultMenu = true;

    protected IMenuManager menuManager = null;

    #region UNITY
    #endregion

    #region PUBLIC
    public virtual void Initialize(IMenuManager manager)
    {
        menuManager = manager;

        if (showDefaultMenu)
            menuManager.ShowMenu(menuClassifier);
    }

    public bool Initialized { get; set; }
    public string ObjectScene { get { return gameObject.scene.path; } }
    #endregion

    #region PROTECTED
    protected MenuClassifier MenuClassifier { get { return menuClassifier; } }
    #endregion
}