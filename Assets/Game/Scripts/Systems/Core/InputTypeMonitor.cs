using UnityEngine;
using UnityEngine.InputSystem;

public class InputTypeMonitor : MonoBehaviour
{
    [SerializeField] private InputData inputData = null;

    #region PUBLIC
    public void OnKeyboardMouseAction(InputAction.CallbackContext value)
    {
        inputData.CurrentInputType = InputType.KeyboardMouse;
    }

    public void OnGamepadAction(InputAction.CallbackContext value)
    {
        inputData.CurrentInputType = InputType.Gamepad;
    }
    #endregion
}