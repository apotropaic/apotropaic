using System.Collections.Generic;
using UnityEngine;
using Apotropaic.Core;

namespace Apotropaic.Events
{
    public abstract class EventsInitializerAbstract : MonoBehaviour
    {
        private void Start()
        {
            SceneLoader.Instance.onSceneLoadedEvent.AddListener(OnSceneLoaded);
        }

        private void OnDestroy()
        {
            if (SceneLoader.isValidSingleton())
            {
                SceneLoader.Instance.onSceneLoadedEvent.RemoveListener(OnSceneLoaded);
            }
        }

        private void OnSceneLoaded(List<string> scene)
        {
            for (int i = 0; i < scene.Count; i++)
            {
                InitializeSubscribers(scene[i]);
            }
        }

        protected abstract void InitializeSubscribers(string scene);
    }
}