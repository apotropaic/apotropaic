using UnityEngine;

namespace Apotropaic.Events
{
    public class PauseEventsSubscriber : MonoBehaviour, IPauseEventsSubscriber
    {
        PauseEventsInitializer.PauseDelegate pauseDelegateCache = null;
        PauseEventsInitializer.PauseDelegate resumeDelegateCache = null;

        IPauseEventsSubscriber.PauseDelegate subscriberPauseDelegate = null;
        IPauseEventsSubscriber.PauseDelegate subscriberResumeDelegate = null;

        private void OnDestroy()
        {
            Unsubscribe();
        }

        public void Subscribe(ref PauseEventsInitializer.PauseDelegate _onPause, ref PauseEventsInitializer.PauseDelegate _onResume)
        {
            _onPause += OnPause;
            _onResume += OnResume;

            pauseDelegateCache = _onPause;
            resumeDelegateCache = _onResume;
        }

        public void Unsubscribe()
        {
            pauseDelegateCache -= OnPause;
            resumeDelegateCache -= OnResume;
        }

        private void OnPause()
        {
            if (subscriberPauseDelegate != null)
                subscriberPauseDelegate();
        }

        private void OnResume()
        {
            if (subscriberResumeDelegate != null)
                subscriberResumeDelegate();
        }

        public void Subscribe(IPauseEventsSubscriber.PauseDelegate pauseDelegate, IPauseEventsSubscriber.PauseDelegate resumeDelegate)
        {
            subscriberPauseDelegate = pauseDelegate;
            subscriberResumeDelegate = resumeDelegate;
        }
    }
}