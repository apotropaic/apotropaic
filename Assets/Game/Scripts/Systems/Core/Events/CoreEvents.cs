using System;

namespace Apotropaic.Events
{
    public static class CoreEvents
    {
        public static Action OnNightTutorialEnded = null;
        public static Action OnNightTransition = null;
        public static Action OnOfficeTransition = null;
        public static Action OnSaveGame = null;
        public static Action OnDeleteSavedData = null;
    }
}