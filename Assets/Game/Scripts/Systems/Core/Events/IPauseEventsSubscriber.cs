namespace Apotropaic.Events
{
    public interface IPauseEventsSubscriber
    {
        public delegate void PauseDelegate();
        public void Subscribe(PauseDelegate pauseDelegate, PauseDelegate resumeDelegate);
    }
}