namespace Apotropaic.Events
{
    public interface IPauseEventsInvoker
    {
        public void Pause();
        public void Resume();
    }
}