using UnityEngine;

namespace Apotropaic.Events
{
    public class PauseEventsInvoker : MonoBehaviour, IPauseEventsInvoker
    {
        PauseEventsInitializer.PauseDelegate pauseDelegate = null;
        PauseEventsInitializer.PauseDelegate resumeDelegate = null;

        public void Initialize(PauseEventsInitializer.PauseDelegate _pauseDelegte, PauseEventsInitializer.PauseDelegate _resumeDelegate)
        {
            pauseDelegate = _pauseDelegte;
            resumeDelegate = _resumeDelegate;
        }

        public void Pause()
        {
            if (pauseDelegate != null)
                pauseDelegate();
        }

        public void Resume()
        {
            if (resumeDelegate != null)
                resumeDelegate();
        }
    }
}