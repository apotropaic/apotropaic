using System;
using System.Collections.Generic;
using UnityEngine;

namespace Apotropaic.Events
{
    public static class UIEvents
    {
        public static Action OnOpenedShop = null;
        public static Action OnOpenedBag = null;

        public static Action<List<ConversationData>, Sprite> OnConversationStarted = null;
        public static Action<List<ConversationData>, Sprite> OnConversationSet = null;
        public static Action OnConversationEnded = null;
        public static Action<MenuClassifier> OnSetDialogPreviousMenu = null;
    }
}