using System;
using UnityEngine;

namespace Apotropaic.Events
{
    public static class GameplayEvents
    {
        public static Action<int> OnPlayerTakenDamage = null;
        public static Action OnPlayerDied = null;
        public static Action OnPlacedTrap = null;
        public static Action OnToggleTrap = null;
        public static Action OnNightPhase = null;
        public static Action OnNightTutorial = null;
        public static Action OnMorningPhase = null;
        public static Action OnGameOver = null;

        public static Action OnSpawnTutorialEnemy = null;
        public static Action OnEnemyDied = null;

        public static Action OnEveningEnter = null;
        public static Action<Vector3> OnBookSpawned = null;

        public static Action<bool> OnLockPlayerMovementInput = null;
        public static Action<bool> OnLockTrapPlacement = null;
    }
}