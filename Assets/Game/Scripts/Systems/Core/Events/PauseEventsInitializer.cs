using Apotropaic.Core;

namespace Apotropaic.Events
{
    public class PauseEventsInitializer : EventsInitializerAbstract
    {
        public delegate void PauseDelegate();
        public PauseDelegate onPause;
        public PauseDelegate onResume;

        protected override void InitializeSubscribers(string scene)
        {
            PauseEventsSubscriber[] pauseEventsSubscribers = SceneLoader.Instance.GetSceneComponents<PauseEventsSubscriber>(scene);

            for (int i = 0; i < pauseEventsSubscribers.Length; i++)
            {
                pauseEventsSubscribers[i].Subscribe(ref onPause, ref onResume);
            }

            PauseEventsInvoker[] pauseEventsInvokers = SceneLoader.Instance.GetSceneComponents<PauseEventsInvoker>(scene);

            for (int i = 0; i < pauseEventsInvokers.Length; i++)
            {
                pauseEventsInvokers[i].Initialize(Pause, Resume);
            }
        }

        private void Pause()
        {
            if (null != onPause)
                onPause();
        }

        private void Resume()
        {
            if (null != onResume)
                onResume();
        }
    }
}