using Apotropaic.Data;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Apotropaic.Core
{
    public class Startup : MonoBehaviour
    {
        [SerializeField] private SceneSettings sceneSettings;

        void Start()
        {
            LoadUIScene();
            LoadStartupScene();
        }

        void LoadUIScene()
        {
            LoadScene(sceneSettings.UIScene);
        }

        void LoadStartupScene()
        {
            LoadScene(sceneSettings.StartupScene);
        }

        void LoadScene(SceneReference sceneReference)
        {
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                if (SceneManager.GetSceneAt(i).name == sceneReference)
                {
                    return;
                }
            }

            Scene scene = SceneManager.GetSceneByPath(sceneReference);

            if (scene.isLoaded == false)
                SceneLoader.Instance.LoadScene(sceneReference, true);
        }
    }
}