using Apotropaic.Data;
using UnityEngine;

namespace Apotropaic.Data
{
    public class DataLoader : MonoBehaviour
    {
        [SerializeField] private PlaytestSettings playtestSettings = null;

        [SerializeField] private GameplayData gameplayData = null;
        [SerializeField] private PlayerInventory playerInventory = null;
        [SerializeField] private InventorySettings inventorySettings = null;

        #region UNITY
        #endregion

        #region PUBLIC
        #endregion

        #region PRIVATE
        public void Initialize()
        {
            LoadPlaytestSettings();
            LoadInventorySettings();
        }

        private void LoadPlaytestSettings()
        {
            if (playtestSettings.OverrideData)
            {
                gameplayData.NightsCleared = playtestSettings.NightsCleared;
                gameplayData.FinishedNightTutorial = playtestSettings.FinishedNightTutorial;

                gameplayData.InfiniteTraps = playtestSettings.InfiniteTraps;
                gameplayData.EnableOfficeMurmur = playtestSettings.EnableOfficeMurmur;
                gameplayData.Invincibility = playtestSettings.InvinciblePlayer;

                playerInventory.Currency = playtestSettings.Currency;
            }
        }

        private void LoadInventorySettings()
        {
            for (int i = 0; i < inventorySettings.TrapsUnlockedByDefault.Length; i++)
            {
                inventorySettings.TrapsUnlockedByDefault[i].Unlocked = true;
            }
        }
        #endregion
    }
}