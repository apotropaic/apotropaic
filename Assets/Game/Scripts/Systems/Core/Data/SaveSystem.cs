using CI.QuickSave;
using Project.Build.Commands;
using System.Collections.Generic;
using UnityEngine;
using Apotropaic.Events;

namespace Apotropaic.Data
{
    public class SaveSystem : MonoBehaviour
    {
        [SerializeField] private BuildData buildData = null;
        [SerializeField] private InventorySettings inventorySettings = null;
        [SerializeField] private PlayerInventory playerInventory = null;
        [SerializeField] private PlayerInventory bagInventory = null;
        [SerializeField] private GameplayData gameplayData = null;
        [SerializeField] private TrapSettingsList trapsList = null;
        [SerializeField] private QuestSystemData quests = null;

        [SerializeField] private DataLoader dataLoader = null;

        [SerializeField] private bool enableLogs = false;

        private const string fileTitle = "GameData";

        private string applicationVersion = "";
        private string fileName = "";

        // Keys
        private const string currency = "currency";
        private const string nightsCleared = "nightsCleared";
        private const string finishedNightTutorial = "finishedNightTutorial";
        private const string trapsInventory = "trapsInventory";
        private const string bagTrapsInventory = "bagTrapsInventory";
        private const string conversationsCompleted = "conversationsCompleted";
        private const string trapsUnlocked = "trapsUnlocked";
        private const string questsCompleted = "questsCompleted";

        #region UNITY
        private void Awake()
        {
            Initialize();
        }

        private void OnDestroy()
        {
            UnsubscribeFromEvents();
        }
        #endregion

        #region PUBLIC
        #endregion

        #region PRIVATE
        private void Initialize()
        {
            SubscribeToEvents();
            SetFileName();
            LoadData();
            dataLoader.Initialize();
        }

        private void SubscribeToEvents()
        {
            CoreEvents.OnSaveGame += SaveData;
            CoreEvents.OnDeleteSavedData += DeleteData;
        }

        private void UnsubscribeFromEvents()
        {
            CoreEvents.OnSaveGame -= SaveData;
            CoreEvents.OnDeleteSavedData -= DeleteData;
        }

        private void SetFileName()
        {
            applicationVersion = buildData.buildNumber.ToString();
            fileName = fileTitle + "_" + applicationVersion;
        }

        private void LoadData()
        {
            try
            {
                ReadData();
            }
            catch
            {
                // If data doesn't exist, create a new file and load the startup data.
                ResetData();
                SaveData();
            }
        }

        private void SaveData()
        {
            if (enableLogs)
                Debug.Log("Saving...");

            Dictionary<int, bool> conversationsCompletedData = FetchConversationsProgress();
            Dictionary<int, bool> trapsUnlockedData = FetchTrapsLockData();
            Dictionary<int, bool> questsCompletedData = FetchQuestsCompletedData();

            QuickSaveWriter.Create(fileName)
                .Write(currency, playerInventory.Currency)
                .Write(nightsCleared, gameplayData.NightsCleared)
                .Write(finishedNightTutorial, gameplayData.FinishedNightTutorial)
                .Write(trapsInventory, playerInventory.TrapList)
                .Write(bagTrapsInventory, bagInventory.TrapList)
                .Write(conversationsCompleted, conversationsCompletedData)
                .Write(trapsUnlocked, trapsUnlockedData)
                .Write(questsCompleted, questsCompletedData)
                .Commit();

            if (enableLogs)
                Debug.Log("Data saved.");
        }

        private void DeleteData()
        {
            if (enableLogs)
                Debug.Log("Deleting data...");

            ResetData();
            SaveData();

            if (enableLogs)
                Debug.Log("All data deleted.");
        }

        private void ResetData()
        {
            playerInventory.Currency = inventorySettings.StartCurrency;
            gameplayData.NightsCleared = 0;
            gameplayData.FinishedNightTutorial = false;
            playerInventory.TrapList = new Dictionary<int, int>();
            bagInventory.TrapList = new Dictionary<int, int>();

            ConversationData[] conversations = Resources.LoadAll<ConversationData>("DialogSystem");

            for (int i = 0; i < conversations.Length; i++)
            {
                conversations[i].Talked = false;
            }

            for (int i = 0; i < trapsList.Traps.Length; i++)
            {
                trapsList.Traps[i].Unlocked = false;
            }

            for (int i = 0; i < quests.QuestDatas.Length; i++)
            {
                quests.QuestDatas[i].ObjectiveComplete = false;
            }
        }

        private void ReadData()
        {
            if (enableLogs)
                Debug.Log("Reading...");

            QuickSaveReader reader = QuickSaveReader.Create(fileName);

            if (reader.Exists(currency))
                playerInventory.Currency = reader.Read<int>(currency);

            if (reader.Exists(nightsCleared))
                gameplayData.NightsCleared = reader.Read<int>(nightsCleared);

            if (reader.Exists(finishedNightTutorial))
                gameplayData.FinishedNightTutorial = reader.Read<bool>(finishedNightTutorial);

            if (reader.Exists(trapsInventory))
                playerInventory.TrapList = reader.Read<Dictionary<int, int>>(trapsInventory);

            if (reader.Exists(bagTrapsInventory))
                bagInventory.TrapList = reader.Read<Dictionary<int, int>>(bagTrapsInventory);

            if (reader.Exists(conversationsCompleted))
                LoadConversationsData(reader);

            if (reader.Exists(trapsUnlocked))
                LoadTrapsLockData(reader);

            if (reader.Exists(questsCompleted))
                LoadQuestsCompletedData(reader);

            if (enableLogs)
                Debug.Log("Data read");
        }

        private Dictionary<int, bool> FetchConversationsProgress()
        {
            ConversationData[] conversations = Resources.LoadAll<ConversationData>("DialogSystem");

            Dictionary<int, bool> conversationsData = new Dictionary<int, bool>();

            for (int i = 0; i < conversations.Length; i++)
            {
                int hashCode = conversations[i].OptionPreface.GetHashCode();

                if (conversationsData.ContainsKey(hashCode))
                {
                    Debug.LogWarning("Conversations data already has key for " + conversations[i].OptionPreface + "  " + conversations[i].name);
                    continue;
                }

                conversationsData.Add(hashCode, conversations[i].Talked);
            }

            return conversationsData;
        }

        private void LoadConversationsData(QuickSaveReader reader)
        {
            Dictionary<int, bool> conversationsData = reader.Read<Dictionary<int, bool>>(conversationsCompleted);
            ConversationData[] conversations = Resources.LoadAll<ConversationData>("DialogSystem");

            for (int i = 0; i < conversations.Length; i++)
            {
                int hashCode = conversations[i].OptionPreface.GetHashCode();
                bool talked = false;

                if (!conversationsData.TryGetValue(hashCode, out talked))
                {
                    Debug.LogWarning("Value not found for " + conversations[i].name);
                    continue;
                }

                conversations[i].Talked = talked;
            }
        }

        private Dictionary<int, bool> FetchTrapsLockData()
        {
            Dictionary<int, bool> trapsLockedData = new Dictionary<int, bool>();

            for (int i = 0; i < trapsList.Traps.Length; i++)
            {
                int UID = trapsList.Traps[i].UID;

                if (trapsLockedData.ContainsKey(UID))
                {
                    Debug.LogWarning("Traps locked data already has key for " + trapsList.Traps[i].Name + "  " + trapsList.Traps[i].name);
                    continue;
                }

                trapsLockedData.Add(UID, trapsList.Traps[i].Unlocked);
            }

            return trapsLockedData;
        }

        private void LoadTrapsLockData(QuickSaveReader reader)
        {
            Dictionary<int, bool> trapsUnlockedData = reader.Read<Dictionary<int, bool>>(trapsUnlocked);

            for (int i = 0; i < trapsList.Traps.Length; i++)
            {
                int UID = trapsList.Traps[i].UID;

                bool unlocked = false;

                if (!trapsUnlockedData.TryGetValue(UID, out unlocked))
                {
                    Debug.LogWarning("Value not found for " + trapsList.Traps[i].name);
                    continue;
                }

                trapsList.Traps[i].Unlocked = unlocked;
            }
        }

        private Dictionary<int, bool> FetchQuestsCompletedData()
        {
            Dictionary<int, bool> questsCompletedData = new Dictionary<int, bool>();

            for (int i = 0; i < quests.QuestDatas.Length; i++)
            {
                int hashCode = quests.QuestDatas[i].QuestTitle.GetHashCode();

                if (questsCompletedData.ContainsKey(hashCode))
                {
                    Debug.LogWarning("Quests data already has key for " + quests.QuestDatas[i].QuestTitle + " index: " + i.ToString());
                    continue;
                }

                questsCompletedData.Add(hashCode, quests.QuestDatas[i].ObjectiveComplete);
            }

            return questsCompletedData;
        }

        private void LoadQuestsCompletedData(QuickSaveReader reader)
        {
            Dictionary<int, bool> questsCompletedData = reader.Read<Dictionary<int, bool>>(questsCompleted);

            for (int i = 0; i < quests.QuestDatas.Length; i++)
            {
                int hashCode = quests.QuestDatas[i].QuestTitle.GetHashCode();

                bool completed = false;

                if (!questsCompletedData.TryGetValue(hashCode, out completed))
                {
                    Debug.LogWarning("Value not found for " + quests.QuestDatas[i].QuestTitle);
                    continue;
                }

                quests.QuestDatas[i].ObjectiveComplete = completed;
            }
        }
        #endregion
    }
}