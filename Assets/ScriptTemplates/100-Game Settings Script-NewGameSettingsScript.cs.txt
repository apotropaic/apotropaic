using System;
using UnityEngine;

    #ROOTNAMESPACEBEGIN#
namespace Apotropaic.Data
{
    [CreateAssetMenu(fileName = "#SCRIPTNAME#", menuName = "Scriptable Object/Settings/#SCRIPTNAME#", order = 0)]
    public class #SCRIPTNAME# : GameSettings<#SCRIPTNAME#Preset>
    {
        public int SampleSetting { get { return CurrentPreset.SampleSetting; } }
    }

    [Serializable]
    public class #SCRIPTNAME#Preset : Preset
    {
        [SerializeField] private int sampleSetting = 0;

        public int SampleSetting { get { return sampleSetting; } }
    }
}
#ROOTNAMESPACEEND#